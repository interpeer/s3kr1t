# S3kr1t

[![Build status](https://ci.appveyor.com/api/projects/status/tn1rgijhjpxsnynx?svg=true)](https://ci.appveyor.com/project/jfinkhaeuser/s3kr1t)


A wrapper library for (some) cryptographic functions. Shh!

The TL;DR is this: some crypto libraries offer interchangeable functions. Some
offer very different functions. It's best that not every interpeer project
needs to deal with this, so we're just going to provide small wrapper functions
for everything we're actually going to use.

And that's it.
