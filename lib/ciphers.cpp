/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <s3kr1t/ciphers.h>

#include <build-config.h>

#include <cmath>

#ifdef S3KR1T_HAVE_OPENSSL
#include "openssl/ciphers.h"
#endif // S3KR1T_HAVE_OPENSSL

#include <liberate/logging.h>

namespace s3kr1t {

struct cipher::impl
{
  inline impl(cipher_type _type, cipher_operation cop)
    : type{_type}
    , ctx{type, cop}
  {
  }

  cipher_type     type;

#ifdef S3KR1T_HAVE_OPENSSL
  openssl_context ctx;
#endif
};


cipher::cipher(cipher_type type, cipher_operation cop)
  : m_impl{std::make_shared<impl>(type, cop)}
{
}



cipher::~cipher()
{
}



cipher_type
cipher::type() const
{
  return m_impl->type;
}



std::size_t
cipher::iv_size() const
{
  return m_impl->ctx.iv_size();
}



std::size_t
cipher::key_size() const
{
  return m_impl->ctx.key_size();
}



std::size_t
cipher::block_size() const
{
  return m_impl->ctx.block_size();
}



std::size_t
cipher::tag_size() const
{
  return m_impl->ctx.tag_size();
}



error_t
cipher::initialize(void const * key, void const * iv,
    void const * tag /*= nullptr */)
{
  if (nullptr == key || nullptr == iv) {
    LIBLOG_DEBUG("Need key and IV both.");
    return ERR_INVALID_VALUE;
  }

  return m_impl->ctx.initialize(key, iv, tag);
}



error_t
cipher::encrypt(void * ciphertext,
    std::size_t & ciphersize,
    void const * plaintext,
    std::size_t const & plainsize)
{
  if (nullptr == ciphertext || nullptr == plaintext || !ciphersize || !plainsize) {
    LIBLOG_DEBUG("Cipher and plain text need to be non-zero buffers.");
    return ERR_INVALID_VALUE;
  }

  // The cipher size must be a multiple of the block size, and large enough
  // for the plain text.
  auto bs = block_size();
  std::size_t required = 0;
  if (bs == 1) {
    required = plainsize;
  }
  else {
    std::size_t blocks = static_cast<std::size_t>(std::trunc(static_cast<double>(plainsize) / bs));
    auto remainder = plainsize % bs;
    if (remainder > 0) {
      ++blocks;
    }
    required = blocks * bs;
  }
  if (ciphersize < required) {
    LIBLOG_DEBUG("The ciphertext buffer size is less than the encryption "
        "operation will likely need.");
    ciphersize = required;
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  return m_impl->ctx.encrypt(ciphertext, ciphersize, plaintext, plainsize);
}



error_t
cipher::encrypt_final(void * ciphertext, std::size_t & ciphersize,
    void * tag /* = nullptr */)
{
  if (nullptr == ciphertext) {
    LIBLOG_DEBUG("Cipher text needs to be a non-null buffer.");
    return ERR_INVALID_VALUE;
  }

  return m_impl->ctx.encrypt_final(ciphertext, ciphersize, tag);
}





error_t
cipher::decrypt(void * plaintext, std::size_t & plainsize,
    void const * ciphertext, std::size_t const & ciphersize)
{
  if (nullptr == ciphertext || nullptr == plaintext || !plainsize || !ciphersize) {
    LIBLOG_DEBUG("Cipher and plain text need to be non-zero buffers.");
    return ERR_INVALID_VALUE;
  }

  return m_impl->ctx.decrypt(plaintext, plainsize, ciphertext, ciphersize);
}



error_t
cipher::decrypt_final(void * plaintext, std::size_t & plainsize)
{
  if (nullptr == plaintext) {
    LIBLOG_DEBUG("Plain text needs to be non-null buffer.");
    return ERR_INVALID_VALUE;
  }

  return m_impl->ctx.decrypt_final(plaintext, plainsize);
}



error_t
cipher::add_authenticated(void const * data, std::size_t const & size)
{
  if (nullptr == data || !size) {
    LIBLOG_DEBUG("Cannot authenticate a null/zero-sized buffer.");
    return ERR_INVALID_VALUE;
  }

  return m_impl->ctx.add_authenticated(data, size);
}

} // namespace s3kr1t
