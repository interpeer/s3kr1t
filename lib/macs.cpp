/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <s3kr1t/macs.h>

#include <build-config.h>

#ifdef S3KR1T_HAVE_OPENSSL
#include "openssl/macs.h"
#endif // S3KR1T_HAVE_OPENSSL

#include <liberate/logging.h>

namespace s3kr1t {

error_t
mac(void * macbuf, size_t macbuf_size, size_t & required,
    void const * data, size_t data_size,
    void const * key, size_t key_size,
    void const * extra, size_t extra_size,
    mac_type mtype)
{
  if (!data || !data_size || !macbuf || !macbuf_size || !key || !key_size) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  required = 0;

  switch (mtype) {
    case MT_POLY1305:
    case MT_KMAC_128:
    case MT_KMAC_256:

#ifdef S3KR1T_HAVE_OPENSSL
      return openssl_mac(macbuf, macbuf_size, required,
          data, data_size,
          key, key_size,
          extra, extra_size,
          mtype);
#else
      break;
#endif

      // TODO other types

    default:
      break;
  }

  return ERR_UNSUPPORTED;
}


error_t
check(void const * data, size_t data_size,
    void const * macbuf, size_t macbuf_size,
    void const * key, size_t key_size,
    void const * extra, size_t extra_size,
    mac_type mtype)
{
  if (!data || !data_size || !macbuf || !macbuf_size || !key || !key_size) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  switch (mtype) {
    case MT_POLY1305:
    case MT_KMAC_128:
    case MT_KMAC_256:

#ifdef S3KR1T_HAVE_OPENSSL
      return openssl_check(data, data_size,
          macbuf, macbuf_size,
          key, key_size,
          extra, extra_size,
          mtype);
#else
      break;
#endif

      // TODO other types

    default:
      break;
  }

  return ERR_UNSUPPORTED;
}



} // namespace s3kr1t
