/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <s3kr1t/random.h>

#include <build-config.h>

#if defined(S3KR1T_HAVE_GETENTROPY)
#include <unistd.h>
#endif

#if defined(S3KR1T_HAVE_GETRANDOM)
#include <sys/random.h>
#endif

#if defined(S3KR1T_HAVE_BCRYPT_GENRANDOM)
#include <bcrypt.h>
#endif

#include <cstring>
#include <chrono>

#include <s3kr1t/digests.h>
#include <s3kr1t/ciphers.h>

#include <liberate/logging.h>
#include <liberate/sys/pid.h>
#include <liberate/sys/memory.h>
#include <liberate/string/hexencode.h>

namespace s3kr1t {

S3KR1T_API error_t
get_random_bytes(void * buffer, std::size_t & amount)
{
// BSD has getentropy() and getrandom(), but getentropy() is preferred, so look
// for it first.
#if defined(S3KR1T_HAVE_GETENTROPY)
  int ret = getentropy(buffer, amount);
  if (ret < 0) {
    LIBLOG_ERRNO("Could not generate random bytes.");

    switch (errno) {
      case EFAULT:
        return ERR_INVALID_VALUE;

      case EIO:
        return ERR_AGAIN;

      default:
        return ERR_UNEXPECTED;
    }
  }

  // ret does *not* specify the amount
  return ERR_SUCCESS;
#elif defined(S3KR1T_HAVE_GETRANDOM)
  ssize_t ret = getrandom(buffer, amount, GRND_RANDOM);
  if (ret < 0) {
    LIBLOG_ERRNO("Could not generate random bytes.");

    switch (errno) {
      case EAGAIN:
      case EINTR:
        return ERR_AGAIN;

      case EFAULT:
      case EINVAL:
        return ERR_INVALID_VALUE;

      case ENOSYS:
        return ERR_UNSUPPORTED;

      default:
        return ERR_UNEXPECTED;
    }
  }
  amount = ret;
  return ERR_SUCCESS;
#endif

#if defined(S3KR1T_HAVE_BCRYPT_GENRANDOM)
  auto ret = BCryptGenRandom(NULL, static_cast<UCHAR *>(buffer), amount,
      BCRYPT_USE_SYSTEM_PREFERRED_RNG);
  if (ERROR_SUCCESS != ret) {
    LIBLOG_ERRNO("Could not generate random bytes.");
    return ERR_INVALID_VALUE;
  }

  return ERR_SUCCESS;
#endif

  // TODO other versions?
  return ERR_UNSUPPORTED;
}


namespace {

static constexpr std::size_t        CHACHA20_KEY_IV_SIZE_COMBINED{48};
static constexpr std::size_t        MAX_BYTES_GENERATED{1024*1024};
static constexpr std::chrono::hours MAX_LIFETIME_HOURS{1};


/**
 * The chacha20 generator encapsulates the cipher; it's not particularly
 * deep. The main interesting point is that it takes arbitrarily large
 * seeds and hashes them into a deterministic length that can be used as
 * key and IV for the cipher.
 */
struct chacha20_generator
{
  inline error_t
  reseed(void const * seed, std::size_t const & seed_size)
  {
    // Hash the seed value into a buffer. This extends or compresses values,
    // and mixes the seed buffer if it's a concatenation of various data
    // points.
    char hashed[CHACHA20_KEY_IV_SIZE_COMBINED];
    size_t unused = 0;
    auto err = digest(hashed, sizeof(hashed), unused, seed, seed_size,
        DT_SHA3_384);
    if (ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Could not hash seed value.");
      return err;
    }

    // Initialize the cipher from the combined hash value. It's long enough
    // for key and IV.
    err = m_cipher.initialize(hashed, hashed + m_cipher.key_size());
    if (ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Could not initialize chacha20 generator.");
      return err;
    }

    return ERR_SUCCESS;
  }


  inline error_t
  generate(void * buffer, std::size_t & size)
  {
    // Clear. This is part of making the algorithm deterministic, but also will
    // shut up valgrind, etc.
    liberate::sys::secure_memzero(buffer, size);

    // Use the cipher to encrypt the buffer, and place the result in itself.
    std::size_t cipher_size = size;
    auto err = m_cipher.encrypt(buffer, cipher_size, buffer, size);
    if (ERR_SUCCESS != err) {
      return err;
    }
    size = cipher_size;

    return ERR_SUCCESS;
  }

  cipher  m_cipher{CT_CHACHA20, CO_ENCRYPT};
};


/**
 * The deterministic seeder extends the chacha20 generator slightly by
 * providing some default seed, and fulfilling the excpetation of the
 * PRNGs seed_function.
 */
struct deterministic_seeder
{
  inline deterministic_seeder(void const * seed, std::size_t seed_size)
    : m_generator{}
  {
    char const * use_seed = "This is s3kr1t!";
    std::size_t use_seed_size = std::strlen(use_seed);

    if (!seed || !seed_size) {
      LIBLOG_WARN("No seed given, this is not going to yield safe results.");
    }
    else {
      use_seed = static_cast<char const *>(seed);
      use_seed_size = seed_size;
    }

    auto err = m_generator.reseed(use_seed, use_seed_size);
    if (ERR_SUCCESS != err) {
      throw exception{err, "Could not initialize deterministic seeder"};
    }
  }

  error_t operator()(void * buffer, std::size_t & size)
  {
    return m_generator.generate(buffer, size);
  }

  chacha20_generator  m_generator;
};


} // anonymous namespace


/**
 * The PRNG implementation also uses the chacha20 generator. It mostly is
 * concerned with managing when and how to reseed.
 * - In deterministic mode, it uses the deterministic_reseeder above, and
 *   a reseed counter.
 * - In non-deterministic mode, it additionally uses the PID for fork
 *   protection, and a timestamp.
 *
 * Furthermore, reseeding is automatic, based on whether 
 */
struct prng::impl
{
  inline impl(prng::seed_function seed_func, bool deterministic)
    : m_seed_func{seed_func}
    , m_deterministic{deterministic}
  {
  }

  inline error_t
  generate(void * random_bytes, std::size_t const & size)
  {
    // The only complex thing about this function is that the size may exceed
    // the limits at which we want to reseed. So we have to break the buffer
    // into chunks of that limit.
    char * buf = static_cast<char *>(random_bytes);
    std::size_t remaining = size;
    while (remaining) {
      auto err = reseed_if_needed();
      if (ERR_SUCCESS != err) {
        LIBLOG_DEBUG("Reseeding PRNG failed.");
        return err;
      }

      std::size_t allowed = MAX_BYTES_GENERATED - m_generated;
      std::size_t to_generate = std::min(allowed, remaining);
      err = m_generator.generate(buf, to_generate);
      if (ERR_SUCCESS != err) {
        return err;
      }

      // We got Bytes generated, so now we have to advance the buffer, update
      // state, etc.
      m_generated += to_generate;
      buf += to_generate;
      remaining -= to_generate;
    }

    return ERR_SUCCESS;
  }


  inline error_t
  reseed_if_needed()
  {
    bool do_reseed = false;

    // Always reseed if there was a fork.
    int pid = liberate::sys::getpid();
    if (pid != m_pid) {
      LIBLOG_DEBUG("Reseed needed due to fork.");
      do_reseed = true;
    }

    // Reseed if the last reseed is too long in the past.
    auto now = std::chrono::steady_clock::now();
    auto diff = now - m_last_reseed;
    if (diff > MAX_LIFETIME_HOURS) {
      LIBLOG_DEBUG("Reseed neded due to timeout.");
      do_reseed = true;
    }

    // Reseed if enough data has been generated.
    if (m_generated >= MAX_BYTES_GENERATED) {
      LIBLOG_DEBUG("Reseed needed because too many bytes generated ("
          << m_generated << " out of " << MAX_BYTES_GENERATED << ").");
      do_reseed = true;
    }

    if (!do_reseed) {
      return ERR_SUCCESS;
    }

    // Ok, we have to reseed. Let's do this now!
    std::vector<char> buf;
    buf.resize(CHACHA20_KEY_IV_SIZE_COMBINED
        + sizeof(m_reseed_count)
        + sizeof(int) // Can vary from build to build, because it's only used
                      // in non-deterministic generators.
        + sizeof(std::chrono::steady_clock::time_point));
    liberate::sys::secure_memzero(&buf[0], buf.size());
    char * offset = &buf[0];

    // Add data from the seed function
    std::size_t size = CHACHA20_KEY_IV_SIZE_COMBINED;
    auto err = m_seed_func(offset, size);
    if (ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Failed to generate seed.");
      return err;
    }
    offset += CHACHA20_KEY_IV_SIZE_COMBINED;

    // Add the reseed counter
    std::memcpy(offset, &m_reseed_count, sizeof(m_reseed_count));
    offset += sizeof(m_reseed_count);

    if (!m_deterministic) {
      // Add PID as fork protection.
      std::memcpy(offset, &pid, sizeof(pid));
      offset += sizeof(pid);

      // Add current time
      std::memcpy(offset, &now, sizeof(now));
      offset += sizeof(now);
    }

    // Resize buffer to the bytes actually used. This ensures that variable
    // sized parts like the pid are only considered when a non-deterministic
    // seed function is used.
    buf.resize(offset - &buf[0]);

    // Reseed
    err = m_generator.reseed(&buf[0], buf.size());
    if (ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Failed to (re-)initialize PRNG.");
      return err;
    }
#if 0
    // Extra debug
    auto hd = liberate::string::canonical_hexdump();
    LIBLOG_DEBUG("Reseeded with " << hd(&buf[0], buf.size()));
#endif

    m_pid = pid;
    m_last_reseed = now;
    m_generated = 0;
    ++m_reseed_count;
    return ERR_SUCCESS;
  }


  prng::seed_function                   m_seed_func;
  bool                                  m_deterministic;

  std::chrono::steady_clock::time_point m_last_reseed = {};
  std::size_t                           m_generated = 0;
  int                                   m_pid = -1;

  // Reseed count is *not* size_t because size_t has different sizes on some
  // 32 vs. 64 bit systems (*cough* Windows *cough*).
  uint64_t                              m_reseed_count = 0;

  chacha20_generator                    m_generator = {};
};




prng::prng(void const * seed, std::size_t seed_size)
  : m_impl{std::make_shared<impl>(deterministic_seeder{seed, seed_size}, true)}
{
}


prng::prng(prng::seed_function seed_func)
  : m_impl{std::make_shared<impl>(seed_func, false)}
{
}


prng::~prng()
{
}


error_t
prng::generate(void * random_bytes, std::size_t const & size)
{
  if (nullptr == random_bytes || !size) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }
  return m_impl->generate(random_bytes, size);
}



std::size_t
prng::reseed_count() const
{
  return m_impl->m_reseed_count;
}



bool
prng::is_deterministic() const
{
  return m_impl->m_deterministic;
}



} // namespace s3kr1t
