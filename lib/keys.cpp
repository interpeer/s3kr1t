/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <s3kr1t/keys.h>

#include <build-config.h>

#include <cstring>

#include <liberate/logging.h>
#include <liberate/string/hexencode.h>

#ifdef S3KR1T_HAVE_OPENSSL
#include "openssl/keys.h"
#endif // S3KR1T_HAVE_OPENSSL

#include "key_impl.h"

namespace s3kr1t {


key::key(std::shared_ptr<key::key_impl> impl)
  : m_impl{impl}
{
}



bool
key::is_public() const
{
  return m_impl && m_impl->is_public;
}



bool
key::is_private() const
{
  return m_impl && !m_impl->is_public;
}


key::operator bool() const
{
  return type() != KT_UNKNOWN;
}



key_type
key::type() const
{
  if (!m_impl) {
    return KT_UNKNOWN;
  }
  return m_impl->type();
}


size_t
key::bits() const
{
  if (!m_impl) {
    return 0;
  }
  return m_impl->bits;
}


std::vector<::liberate::types::byte>
key::hash(digest_type dtype /*= DT_SHA3_512 */) const
{
  // Write DER-encoded key into a temporary buffer of sufficient size.
  char buf[8192]; // flawfinder: ignore
  size_t written = 0;
  auto err = write(buf, sizeof(buf), KE_DER, written);
  if (ERR_SUCCESS != err || !written) {
    LIBLOG_ERROR("Could not write DER key.");
    return {};
  }

  // Create a digest of the buffer. Let's put it behind the DER key.
  char * offset = buf + written;
  size_t required = 0;
  err = digest(offset, sizeof(buf) - written, required,
      buf, written,
      dtype);
  if (ERR_SUCCESS != err || !required || required > sizeof(buf)) {
    LIBLOG_ERROR("Could not create key digest.");
    return {};
  }

  // Copy buffer
  std::vector<::liberate::types::byte> ret;
  ret.resize(required);

  std::memcpy(&ret[0], offset, required); // flawfinder: ignore

  return ret;
}


std::string
key::hex_hash(digest_type dtype /* = DT_SHA3_512 */) const
{
  auto h = hash(dtype);
  return liberate::string::hexencode(h.data(), h.size());
}


bool
key::operator==(key const & other) const
{
  if (type() != other.type()) {
    return false;
  }

  switch (type()) {
    case KT_DSA:
    case KT_RSA:
    case KT_EC:
    case KT_ED:
#ifdef S3KR1T_HAVE_OPENSSL
      return m_impl->ossl_key == other.m_impl->ossl_key;
#else
      return false;
#endif // S3KR1T_HAVE_OPENSSL

    case KT_UNKNOWN:
    default:
      return false;
  }
}


bool
key::operator<(key const & other) const
{
  if (type() < other.type()) {
    return true;
  }
  if (type() > other.type()) {
    return false;
  }

  switch (type()) {
    case KT_DSA:
    case KT_RSA:
    case KT_EC:
    case KT_ED:
#ifdef S3KR1T_HAVE_OPENSSL
      return m_impl->ossl_key < other.m_impl->ossl_key;
#else
      return false;
#endif // S3KR1T_HAVE_OPENSSL

    case KT_UNKNOWN:
    default:
      return false;
  }
}





key::pair
key::create_pair() const
{
  // Shallow copy, then extract the public key and return.
  auto privkey_ptr = std::shared_ptr<key>{new key{m_impl}};
  auto pubkey = privkey_ptr->extract_pubkey();
  if (!pubkey) {
    return {};
  }

  pair retval;
  retval.pubkey = std::shared_ptr<key>{new key{pubkey.m_impl}};
  retval.privkey = privkey_ptr;
  return retval;
}



key
key::extract_pubkey() const
{
  if (!is_private()) {
    // TODO Log?
    return {};
  }

  switch (type()) {
    case KT_DSA:
    case KT_RSA:
    case KT_EC:
    case KT_ED:
#ifdef S3KR1T_HAVE_OPENSSL
      {
        auto pubkey = openssl_try_extract_pubkey(m_impl->ossl_key.evp_key);
        if (!pubkey) {
          break;
        }

        return {std::make_shared<key::key_impl>(pubkey, type(), true,
            openssl::extract_bits(pubkey))};
      }
#else
      return {};
#endif // S3KR1T_HAVE_OPENSSL

    case KT_UNKNOWN:
    default:
      break;
  }

  return {};
}



key
key::read_pubkey(
    void const * buffer, size_t bufsize,
    std::string const & password /* = {} */)
{
  if (!buffer || !bufsize) {
    throw exception{ERR_INSUFFICIENT_BUFFER_SIZE, "No input buffer given."};
  }

#ifdef S3KR1T_HAVE_OPENSSL
  auto [pubkey, type, bits] = openssl_read_pubkey(buffer, bufsize, password);
  if (pubkey) {
    auto impl = std::make_shared<key::key_impl>(pubkey, type, true, bits);
    return {impl};
  }
#endif // S3KR1T_HAVE_OPENSSL

  // TODO non-OpenSSL attempts

  // Failure
  return {};
}



key
key::read_privkey(
    void const * buffer, size_t bufsize,
    std::string const & password /* = {} */)
{
  if (!buffer || !bufsize) {
    throw exception{ERR_INSUFFICIENT_BUFFER_SIZE, "No input buffer given."};
  }

#ifdef S3KR1T_HAVE_OPENSSL
  auto [privkey, type, bits] = openssl_read_privkey(buffer, bufsize, password);
  if (privkey) {
    auto impl = std::make_shared<key::key_impl>(privkey, type, false, bits);
    return {impl};
  }
#endif // S3KR1T_HAVE_OPENSSL

  // TODO non-OpenSSL attempts

  // Failure
  return {};
}



key::pair
key::read_keys(
    void const * buffer, size_t bufsize,
    std::string const & password /* = {} */)
{
  if (!buffer || !bufsize) {
    throw exception{ERR_INSUFFICIENT_BUFFER_SIZE, "No input buffer given."};
  }

  auto privkey = key::read_privkey(buffer, bufsize, password);
  if (!privkey) {
    return {};
  }
  return privkey.create_pair();
}



key
key::generate(key_type type, std::string const & parameters)
{
  switch (type) {
    case KT_DSA:
    case KT_RSA:
    case KT_EC:
    case KT_ED:
#ifdef S3KR1T_HAVE_OPENSSL
      {
        // TODO
        auto key = openssl_generate_key(type, parameters);
        if (!key) {
          break;
        }

        return {std::make_shared<key::key_impl>(key, type, false,
            openssl::extract_bits(key))};
      }
#else
      return {};
#endif // S3KR1T_HAVE_OPENSSL

    // TODO other types of key

    case KT_UNKNOWN:
    default:
      break;
  }

  return {};
}



error_t
key::write(void * buffer, size_t bufsize, key_encoding encoding, size_t & written,
    std::string const & password /* = {} */) const
{
  if (!buffer || !bufsize) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  written = 0;

  switch (type()) {
    case KT_DSA:
    case KT_RSA:
    case KT_EC:
    case KT_ED:
#ifdef S3KR1T_HAVE_OPENSSL
      return openssl_write_key(m_impl->ossl_key.evp_key,
          m_impl->is_public,
          buffer, bufsize, written,
          encoding, password);
#else
      return ERR_ENCODE;
#endif

    case KT_UNKNOWN:
    default:
      break;
  }

  return ERR_INVALID_VALUE;
}




} // namespace s3kr1t
