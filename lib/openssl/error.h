/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#ifndef S3KR1T_HAVE_OPENSSL
#error Bad include; OpenSSL not found.
#endif

#include <openssl/err.h>

#include <liberate/logging.h>

namespace s3kr1t {

#define OPENSSL_LOGBASE(method, msg) \
  { \
    char buf[200]; /* flawfinder: ignore */\
    auto err = ERR_get_error(); \
    ERR_error_string_n(err, buf, sizeof(buf)); \
    LIBLOG_ ## method(msg << ": OpenSSL [" << err << "]: " << buf); \
  }

#define OPENSSL_ERRLOG(msg) OPENSSL_LOGBASE(ERROR, msg);
#define OPENSSL_DLOG(msg) OPENSSL_LOGBASE(DEBUG, msg);

} // namespace s3kr1t
