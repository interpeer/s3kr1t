/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_OPENSSL_KDFS_H
#define S3KR1T_OPENSSL_KDFS_H

#include <build-config.h>

#ifndef S3KR1T_HAVE_OPENSSL
#error Bad include; OpenSSL not found.
#endif

#include <cstring>

#include <openssl/evp.h>
#include <openssl/params.h>
#include <openssl/kdf.h>
#include <openssl/core_names.h>

#include <s3kr1t/error.h>

#include "error.h"

namespace s3kr1t {
namespace {

inline constexpr
char const *
get_kdf(kdf_type ktype)
{
  switch (ktype) {
    case KDFT_SCRYPT:
      return "SCRYPT";
    case KDFT_ARGON2ID:
      return "ARGON2ID";

    default:
      break;
  }
  return nullptr;
}

} // anonymous namespace


inline error_t
openssl_kdf(void * outbuf, size_t outbuf_size, size_t & required,
    void const * password, size_t password_size,
    void const * salt, size_t salt_size,
    kdf_type ktype)
{
  // Set parameters. The OSSL_PARAM_ functions take addreses, because they
  // expect us to manage the storage, which means the primitive values need
  // to live outside of the blocks where we create the params.

#if defined(OSSL_KDF_PARAM_SCRYPT_N)
  // Parameters for SCRYPT
  uint64_t n = 1024;
  uint32_t r = 8;
  uint32_t p = 16;
#endif

#if defined(OSSL_KDF_PARAM_ARGON2_LANES)
  // Parameters for Argon2
  // For most parameters chosen, see RFC9106 Section 4; this is the
  // second recommended option to preserve some memory.
  uint32_t lanes = 4;
  uint32_t memcost = 65536;
  uint32_t iter = 3;
#endif

  std::unique_ptr<OSSL_PARAM[]> params;
  if (KDFT_SCRYPT == ktype) {
#if defined(OSSL_KDF_PARAM_SCRYPT_N)
    params = std::make_unique<OSSL_PARAM[]>(6);

    params[2] = OSSL_PARAM_construct_uint64(OSSL_KDF_PARAM_SCRYPT_N, &n);
    params[3] = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_SCRYPT_R, &r);
    params[4] = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_SCRYPT_P, &p);
    params[5] = OSSL_PARAM_construct_end();
#else
    LIBLOG_ERROR("This version of OpenSSL does not support Argon2");
    return ERR_UNSUPPORTED;
#endif
  }
  else if (KDFT_ARGON2ID == ktype) {
#if defined(OSSL_KDF_PARAM_ARGON2_LANES)
    // For most parameters chosen, see RFC9106 Section 4; this is the
    // second recommended option to preserve some memory.
    params = std::make_unique<OSSL_PARAM[]>(6);

    params[2] = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_ARGON2_LANES, &lanes);
    params[3] = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_ARGON2_MEMCOST, &memcost);
    params[4] = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_ITER, &iter);
    params[5] = OSSL_PARAM_construct_end();
#else
    LIBLOG_ERROR("This version of OpenSSL does not support Argon2");
    return ERR_UNSUPPORTED;
#endif
  }

  // Params 0 and 1 are password and salt respectively
  params[0] = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_PASSWORD,
      const_cast<void *>(password), password_size);
  params[1] = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_SALT,
      const_cast<void *>(salt), salt_size);

  // Create KDF algo
  auto algo = std::unique_ptr<EVP_KDF, decltype(&EVP_KDF_free)>{
    EVP_KDF_fetch(nullptr, get_kdf(ktype), nullptr),
    EVP_KDF_free
  };
  if (!algo) {
    LIBLOG_ERROR("Unsupported KDF type: " << ktype);
    return ERR_UNSUPPORTED;
  }

  // Create context
  auto ctx = std::unique_ptr<EVP_KDF_CTX, decltype(&EVP_KDF_CTX_free)>{
    EVP_KDF_CTX_new(algo.get()),
    EVP_KDF_CTX_free
  };
  if (!ctx) {
    OPENSSL_ERRLOG("Could not create KDF context");
    return ERR_KDF;
  }

  size_t kdf_size = EVP_KDF_CTX_get_kdf_size(ctx.get());
  if (kdf_size == SIZE_MAX) {
    required = outbuf_size;
  }
  else {
    required = kdf_size;
    if (required != outbuf_size) {
      LIBLOG_ERROR("Need a specific output size for this KDF!");
      return ERR_KDF;
    }
  }

  int _err = EVP_KDF_derive(ctx.get(),
      static_cast<unsigned char *>(outbuf), required,
      params.get());
  if (_err <= 0) {
    OPENSSL_ERRLOG("Unable to derive key");
    return ERR_KDF;
  }

  // All good
  return ERR_SUCCESS;
}



} // namespace s3kr1t

#endif // guard
