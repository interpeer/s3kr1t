/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_OPENSSL_CIPHERS_H
#define S3KR1T_OPENSSL_CIPHERS_H

#include <build-config.h>

#ifndef S3KR1T_HAVE_OPENSSL
#error Bad include; OpenSSL not found.
#endif

#include <cstring>
#include <optional>
#include <atomic>

#include <openssl/evp.h>

#include <s3kr1t/error.h>

#include "error.h"

namespace s3kr1t {
namespace {

struct openssl_cipher
{
  cipher_type type = CT_NONE;
  EVP_CIPHER const * (*cipher_func)() = nullptr;
  // Tag size isn't necessarily the size we *need* to use here, but the
  // size we *want* to use - as such, we're limiting the user options a
  // little here, but that isn't much of a downside.
  std::size_t tag_size = 0;
  bool        set_tag_before = false;
} OPENSSL_CIPHERS[] = {
  { CT_AES_128_ECB,         EVP_aes_128_ecb, 0, false },
  { CT_AES_128_CBC,         EVP_aes_128_cbc, 0, false },
  { CT_AES_128_CFB1,        EVP_aes_128_cfb1, 0, false },
  { CT_AES_128_CFB1,        EVP_aes_128_cfb8, 0, false },
  { CT_AES_128_CFB128,      EVP_aes_128_cfb128, 0, false },
  { CT_AES_128_OFB,         EVP_aes_128_ofb, 0, false },
  { CT_AES_128_CTR,         EVP_aes_128_ctr, 0, false },
  { CT_AES_128_GCM,         EVP_aes_128_gcm, 12, false },
  { CT_AES_128_OCB,         EVP_aes_128_ocb, 16, true },

  { CT_AES_192_ECB,         EVP_aes_192_ecb, 0, false },
  { CT_AES_192_CBC,         EVP_aes_192_cbc, 0, false },
  { CT_AES_192_CFB1,        EVP_aes_192_cfb1, 0, false },
  { CT_AES_192_CFB1,        EVP_aes_192_cfb8, 0, false },
  { CT_AES_192_CFB128,      EVP_aes_192_cfb128, 0, false },
  { CT_AES_192_OFB,         EVP_aes_192_ofb, 0, false },
  { CT_AES_192_CTR,         EVP_aes_192_ctr, 0, false },
  { CT_AES_192_GCM,         EVP_aes_192_gcm, 12, false },
  { CT_AES_192_OCB,         EVP_aes_192_ocb, 16, true },

  { CT_AES_256_ECB,         EVP_aes_256_ecb, 0, false },
  { CT_AES_256_CBC,         EVP_aes_256_cbc, 0, false },
  { CT_AES_256_CFB1,        EVP_aes_256_cfb1, 0, false },
  { CT_AES_256_CFB1,        EVP_aes_256_cfb8, 0, false },
  { CT_AES_256_CFB128,      EVP_aes_256_cfb128, 0, false },
  { CT_AES_256_OFB,         EVP_aes_256_ofb, 0, false },
  { CT_AES_256_CTR,         EVP_aes_256_ctr, 0, false },
  { CT_AES_256_GCM,         EVP_aes_256_gcm, 12, false },
  { CT_AES_256_OCB,         EVP_aes_256_ocb, 16, true },

  { CT_CHACHA20,            EVP_chacha20, 0, false },
  { CT_CHACHA20_POLY1305,   EVP_chacha20_poly1305, 16, true },
};

struct openssl_context
{
  inline explicit openssl_context(cipher_type type, cipher_operation cop)
    : m_evp_ctx{EVP_CIPHER_CTX_new(), EVP_CIPHER_CTX_free}
    , m_cop{cop}
  {
    if (CT_NONE == type) {
      throw exception{ERR_INVALID_VALUE, "Need to specify a cipher!"};
    }

    switch (m_cop) {
      case CO_ENCRYPT:
      case CO_DECRYPT:
        break;

      default:
        throw exception{ERR_INVALID_VALUE, "Invalid cipher operation specified!"};
    }

    for (size_t i = 0 ; i < sizeof(OPENSSL_CIPHERS) / sizeof(openssl_cipher) ; ++i) {
      if (type == OPENSSL_CIPHERS[i].type) {
        m_cipher = OPENSSL_CIPHERS[i];
        break;
      }
    }

    if (CT_NONE == m_cipher.type) {
      throw exception{ERR_INVALID_VALUE, "Specified cipher not found!"};
    }
  }


  inline std::size_t iv_size() const
  {
    if (CT_NONE == m_cipher.type) {
      return 0;
    }
    return EVP_CIPHER_iv_length(m_cipher.cipher_func());
  }


  inline std::size_t key_size() const
  {
    if (CT_NONE == m_cipher.type) {
      return 0;
    }
    return EVP_CIPHER_key_length(m_cipher.cipher_func());
  }


  inline std::size_t block_size() const
  {
    if (CT_NONE == m_cipher.type) {
      return 0;
    }
    return EVP_CIPHER_block_size(m_cipher.cipher_func());
  }



  inline std::size_t tag_size() const
  {
    if (CT_NONE == m_cipher.type) {
      return 0;
    }
    return m_cipher.tag_size;
  }


  inline error_t initialize(void const * key, void const * iv,
      void const * tag /* = nullptr */)
  {
    auto ks = key_size();
    if (ks > 0) {
      m_key.resize(ks);
      std::memcpy(&m_key[0], key, ks);
    }

    auto is = iv_size();
    if (is > 0) {
      m_iv.resize(is);
      std::memcpy(&m_iv[0], iv, is);
    }

    if (tag) {
      auto ts = tag_size();
      if (ts > 0) {
        m_tag.resize(ts);
        std::memcpy(&m_tag[0], tag, ts);
      }
    }

    if (m_initialized) {
      // Need to re-initialize.
      auto err = init_cipher();
      if (ERR_SUCCESS != err) {
        return err;
      }
    }

    return ERR_SUCCESS;
  }


  inline error_t encrypt(void * ciphertext,
      std::size_t & ciphersize,
      void const * plaintext,
      std::size_t const & plainsize)
  {
    int outlen = ciphersize;
    ciphersize = 0;

    if (CT_NONE == m_cipher.type) {
      LIBLOG_ERROR("No cipher selected.");
      return ERR_ENCRYPT;
    }

    if (!m_initialized) {
      auto err = init_cipher();
      if (ERR_SUCCESS != err) {
        return err;
      }
    }
    if (m_cop != CO_ENCRYPT) {
      LIBLOG_ERROR("Trying to encrypt with a decryption context.");
      return ERR_UNSUPPORTED;
    }

    int ret = EVP_EncryptUpdate(m_evp_ctx.get(),
        static_cast<unsigned char *>(ciphertext), &outlen,
        static_cast<unsigned char const *>(plaintext), plainsize);
    if (1 != ret) {
      OPENSSL_ERRLOG("Error during encryption.");
      return ERR_ENCRYPT;
    }

    ciphersize = outlen;
    return ERR_SUCCESS;
  }


  inline error_t encrypt_final(void * ciphertext, std::size_t & ciphersize,
      void * tag /* = nullptr */)
  {
    int outlen = ciphersize;
    ciphersize = 0;

    if (CT_NONE == m_cipher.type) {
      LIBLOG_ERROR("No cipher selected.");
      return ERR_ENCRYPT;
    }

    if (!m_initialized) {
      auto err = init_cipher();
      if (ERR_SUCCESS != err) {
        return err;
      }
    }
    if (m_cop != CO_ENCRYPT) {
      LIBLOG_ERROR("Trying to encrypt with a decryption context.");
      return ERR_UNSUPPORTED;
    }

    // Configure the tag size if we have one.
    if (m_cipher.set_tag_before && m_cipher.tag_size > 0) {
      int ret = EVP_CIPHER_CTX_ctrl(m_evp_ctx.get(),
          EVP_CTRL_AEAD_SET_TAG, m_cipher.tag_size, nullptr);
      if (1 != ret) {
        OPENSSL_ERRLOG("Could not set AEAD tag size.");
        return ERR_INITIALIZATION;
      }
    }

    // Final encryption step
    int ret = EVP_EncryptFinal_ex(m_evp_ctx.get(),
        static_cast<unsigned char *>(ciphertext), &outlen);
    if (1 != ret) {
      OPENSSL_ERRLOG("Error finalizing encryption.");
      return ERR_ENCRYPT;
    }

    // If we have a tag size *and* a tag pointer, try to get the tag.
    if (m_cipher.tag_size && tag) {
      ret = EVP_CIPHER_CTX_ctrl(m_evp_ctx.get(), EVP_CTRL_AEAD_GET_TAG, m_cipher.tag_size, tag);
      if (1 != ret) {
        OPENSSL_ERRLOG("Could not retrieve AEAD tag.");
        return ERR_ENCRYPT;
      }
    }

    ciphersize = outlen;
    m_initialized = false; // Open for re-use
    return ERR_SUCCESS;
  }


  inline error_t decrypt(void * plaintext,
      std::size_t & plainsize,
      void const * ciphertext,
      std::size_t const & ciphersize)
  {
    int outlen = plainsize;
    plainsize = 0;

    if (CT_NONE == m_cipher.type) {
      LIBLOG_ERROR("No cipher selected.");
      return ERR_DECRYPT;
    }

    if (!m_initialized) {
      auto err = init_cipher();
      if (ERR_SUCCESS != err) {
        return err;
      }
    }
    if (m_cop != CO_DECRYPT) {
      LIBLOG_ERROR("Trying to decrypt with an encryption context.");
      return ERR_UNSUPPORTED;
    }

    int ret = EVP_DecryptUpdate(m_evp_ctx.get(),
        static_cast<unsigned char *>(plaintext), &outlen,
        static_cast<unsigned char const *>(ciphertext), ciphersize);
    if (1 != ret) {
      OPENSSL_ERRLOG("Error during decryption.");
      return ERR_DECRYPT;
    }

    plainsize = outlen;
    return ERR_SUCCESS;
  }


  inline error_t decrypt_final(void * plaintext, std::size_t & plainsize)
  {
    int outlen = plainsize;
    plainsize = 0;

    if (CT_NONE == m_cipher.type) {
      LIBLOG_ERROR("No cipher selected.");
      return ERR_DECRYPT;
    }

    if (!m_initialized) {
      auto err = init_cipher();
      if (ERR_SUCCESS != err) {
        return err;
      }
    }
    if (m_cop != CO_DECRYPT) {
      LIBLOG_ERROR("Trying to decrypt with an encryption context.");
      return ERR_UNSUPPORTED;
    }

    // Set the AEAD tag, if we should have one.
    if (m_cipher.tag_size) {
      if (!m_tag.size()) {
        LIBLOG_ERROR("Need a tag for decrypting, but none was set.");
        return ERR_INITIALIZATION;
      }

      int ret = EVP_CIPHER_CTX_ctrl(m_evp_ctx.get(),
          EVP_CTRL_AEAD_SET_TAG, m_tag.size(), &m_tag[0]);
      if (1 != ret) {
        OPENSSL_ERRLOG("Could not set AEAD tag.");
        return ERR_INITIALIZATION;
      }
    }

    int ret = EVP_DecryptFinal_ex(m_evp_ctx.get(),
        static_cast<unsigned char *>(plaintext), &outlen);
    if (1 != ret) {
      OPENSSL_ERRLOG("Error finalizing decryption.");
      return ERR_DECRYPT;
    }

    plainsize = outlen;
    m_initialized = false; // Open for re-use
    return ERR_SUCCESS;
  }


  inline error_t add_authenticated(void const * data, std::size_t const & size)
  {
    if (CT_NONE == m_cipher.type) {
      LIBLOG_ERROR("No cipher selected.");
      return ERR_DECRYPT;
    }

    if (!m_initialized) {
      auto err = init_cipher();
      if (ERR_SUCCESS != err) {
        return err;
      }
    }

    int unused = 0;
    if (m_cop == CO_ENCRYPT) {
      int ret = EVP_EncryptUpdate(m_evp_ctx.get(),
          nullptr, &unused,
          static_cast<unsigned char const *>(data), size);
      if (1 != ret) {
        OPENSSL_ERRLOG("Error adding authenticated data.");
        return ERR_ENCRYPT;
      }
    }
    else {
      int ret = EVP_DecryptUpdate(m_evp_ctx.get(),
          nullptr, &unused,
          static_cast<unsigned char const *>(data), size);
      if (1 != ret) {
        OPENSSL_ERRLOG("Error adding authenticated data.");
        return ERR_ENCRYPT;
      }
    }

    return ERR_SUCCESS;
  }


  inline error_t init_cipher()
  {
    if (m_key.size() != key_size() || m_iv.size() != iv_size()) {
      LIBLOG_ERROR("Cipher is not initialized, cannot continue.");
      return ERR_INITIALIZATION;
    }

    int ret = 0;

    if (m_cop == CO_ENCRYPT) {
      ret = EVP_EncryptInit_ex(m_evp_ctx.get(),
          m_cipher.cipher_func(), nullptr,
          reinterpret_cast<unsigned char *>(m_key.data()),
          reinterpret_cast<unsigned char *>(m_iv.data())
      );
    }
    else {
      ret = EVP_DecryptInit_ex(m_evp_ctx.get(),
          m_cipher.cipher_func(), nullptr,
          reinterpret_cast<unsigned char *>(m_key.data()),
          reinterpret_cast<unsigned char *>(m_iv.data())
      );
    }
    if (1 != ret) {
      OPENSSL_ERRLOG("Cipher context could not be initialized.");
      return ERR_INITIALIZATION;
    }

    m_initialized = true;
    return ERR_SUCCESS;
  }


  using evp_ctx_ptr = std::unique_ptr<EVP_CIPHER_CTX, decltype(&EVP_CIPHER_CTX_free)>;
  evp_ctx_ptr                     m_evp_ctx;

  openssl_cipher                  m_cipher = {};

  std::vector<char>               m_key {};
  std::vector<char>               m_iv = {};
  std::vector<char>               m_tag = {};

  std::atomic<bool>               m_initialized = false;
  cipher_operation                m_cop = static_cast<cipher_operation>(0xff);
};

} // anonymous namespace
} // namespace s3kr1t

#endif // guard
