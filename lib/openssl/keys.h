/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_OPENSSL_KEYS_H
#define S3KR1T_OPENSSL_KEYS_H

#include <build-config.h>

#ifndef S3KR1T_HAVE_OPENSSL
#error Bad include; OpenSSL not found.
#endif

#include <s3kr1t/error.h>

#include "error.h"

#include <memory>
#include <cstring>
#include <functional>

#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/objects.h>
#include <openssl/rsa.h>
#include <openssl/ec.h>
#include <openssl/dsa.h>

#include <liberate/string/util.h>

namespace s3kr1t {


/**
 * The main interface is an openssl_key struct to which operations are
 * deferred.
 */
using evp_key_ptr = std::shared_ptr<EVP_PKEY>;


namespace {
namespace openssl {

struct key_generator
{
  using ctx_ptr = std::shared_ptr<EVP_PKEY_CTX>;

  int         ossl_type;
  ctx_ptr     ctx;
  ctx_ptr     params;

  inline key_generator(int ossl_key_type)
    : ossl_type{ossl_key_type}
  {
    ERR_clear_error();

    // Create context
    ctx = ctx_ptr{
      EVP_PKEY_CTX_new_id(ossl_type, nullptr),
      EVP_PKEY_CTX_free
    };
    if (!ctx) {
      OPENSSL_ERRLOG("Unable to create key generation context.");
      return;
    }

    int success = EVP_PKEY_keygen_init(ctx.get());
    if (!success) {
      OPENSSL_ERRLOG("Unable to initalize key generation context.");
    }
  }


  inline bool
  ensure_params()
  {
    params = ctx_ptr{
      EVP_PKEY_CTX_new_id(ossl_type, nullptr),
      EVP_PKEY_CTX_free
    };
    if (!params) {
      OPENSSL_ERRLOG("Unable to create key parameters.");
      return false;
    }

    int success = EVP_PKEY_paramgen_init(params.get());
    if (!success) {
      OPENSSL_ERRLOG("Unable to initialize key parameters.");
      return false;
    }

    return true;
  }


  inline bool
  set_ec_curve_nid(int nid)
  {
    int success = EVP_PKEY_CTX_set_ec_paramgen_curve_nid(ctx.get(), nid);
    if (!success) {
      OPENSSL_ERRLOG("Unable to set curve NID parameters");
      return false;
    }
    return true;
  }


  inline bool
  set_rsa_bits(size_t bits)
  {
    if (!bits) {
      LIBLOG_ERROR("Invalid key size: " << bits);
      return false;
    }

    int success = EVP_PKEY_CTX_set_rsa_keygen_bits(ctx.get(), bits);
    if (!success) {
      OPENSSL_ERRLOG("Unable to set RSA key bits.");
      return false;
    }
    return true;
  }


  inline bool
  set_dsa_bits(size_t bits)
  {
    if (!bits) {
      LIBLOG_ERROR("Invalid key size: " << bits);
      return false;
    }

    if (!ensure_params()) {
      return false;
    }

    int success = EVP_PKEY_CTX_set_dsa_paramgen_bits(params.get(), bits);
    if (!success) {
      OPENSSL_ERRLOG("Unable to set DSA key bits.");
      return false;
    }
    return true;
  }


  inline evp_key_ptr
  generate()
  {
    // If we had to go via a parameter set, we need to apply it now.
    if (params) {
      EVP_PKEY * _cparams = nullptr;
      int success = EVP_PKEY_paramgen(params.get(), &_cparams);
      if (!success) {
        OPENSSL_ERRLOG("Unable to generate parameters");
        return {};
      }
      auto cparams = evp_key_ptr{_cparams, EVP_PKEY_free};
      _cparams = nullptr;

      // Replace context with one created from the params
      ctx = ctx_ptr{
        EVP_PKEY_CTX_new(cparams.get(), nullptr),
        EVP_PKEY_CTX_free
      };
      if (!ctx) {
        OPENSSL_ERRLOG("Unable to re-initialize key generation context.");
        return {};
      }

      success = EVP_PKEY_keygen_init(ctx.get());
      if (!success) {
        OPENSSL_ERRLOG("Unable to initalize key generation context.");
        return {};
      }
    }

    // Generate key
    EVP_PKEY * _key = nullptr;
    int success = EVP_PKEY_keygen(ctx.get(), &_key);
    if (!success || !_key) {
      OPENSSL_ERRLOG("Unable to generete key.");
      return {};
    }
    return evp_key_ptr{_key, EVP_PKEY_free};
  }
};

} // namespace openssl
} // anonymous namespace


struct openssl_key
{
  evp_key_ptr                 evp_key = {};
  key_type                    type;


  inline bool operator==(openssl_key const & other)
  {
#if OPENSSL_VERSION_MAJOR >= 3
    int res = EVP_PKEY_eq(evp_key.get(), other.evp_key.get());
#else
    int res = EVP_PKEY_cmp(evp_key.get(), other.evp_key.get());
#endif
    return 1 == res; // Match
  }

  inline bool operator<(openssl_key const & other)
  {
#if OPENSSL_VERSION_MAJOR >= 3
    int res = EVP_PKEY_eq(evp_key.get(), other.evp_key.get());
#else
    int res = EVP_PKEY_cmp(evp_key.get(), other.evp_key.get());
#endif
    return 1 != res; // Any non-matching is less-than
  }
};


namespace {
namespace openssl {

using bio_ptr = std::unique_ptr<BIO, decltype(&BIO_free_all)>;
using x509_ptr = std::unique_ptr<X509, decltype(&X509_free)>;

inline x509_ptr
try_extract_pem_cert(bio_ptr & certbio, std::string const & password)
{
  BIO_reset(certbio.get());
  ERR_clear_error();

  auto res = x509_ptr{
    PEM_read_bio_X509(
        certbio.get(), nullptr, nullptr,
        password.empty() ? nullptr : const_cast<char *>(password.c_str())
    ),
    X509_free};
  if (!res) {
    OPENSSL_DLOG("Could not read X509 certificate from PEM input");
  }
  return res;
}



inline x509_ptr
try_extract_der_cert(bio_ptr & certbio, std::string const & password [[maybe_unused]])
{
  // TODO password-protected DER is a bit kludgy, see e.g.
  // https://security.stackexchange.com/questions/244668/openssl-encrypt-der-format-private-key
  // TL;DR is, let's leave it for now.
  BIO_reset(certbio.get());
  ERR_clear_error();

  auto res = x509_ptr{
    d2i_X509_bio(certbio.get(), nullptr),
    X509_free};
  if (!res) {
    OPENSSL_DLOG("Could not read X509 certificate from DER input");
  }
  return res;
}


inline evp_key_ptr
try_extract_evp_pubkey(x509_ptr & cert)
{
  ERR_clear_error();

  evp_key_ptr res;
  res.reset(X509_get_pubkey(cert.get()), EVP_PKEY_free);
  if (!res) {
    OPENSSL_DLOG("Could not read public key from X509 certificate");
  }

  return res;
}


inline evp_key_ptr
try_extract_evp_pubkey(bio_ptr & keybio, std::string const & password,
    bool reset)
{
  if (reset) {
    BIO_reset(keybio.get());
  }
  else {
    BIO_seek(keybio.get(), 0);
  }

  ERR_clear_error();

  evp_key_ptr res;
  res.reset(
    PEM_read_bio_PUBKEY(keybio.get(), nullptr, nullptr,
        const_cast<char *>(password.c_str())),
    EVP_PKEY_free
  );
  if (!res) {
    OPENSSL_DLOG("Could not read public key from PEM input");

    BIO_reset(keybio.get());
    ERR_clear_error();

    res.reset(
      d2i_PUBKEY_bio(keybio.get(), nullptr),
      EVP_PKEY_free
    );

    if (!res) {
      OPENSSL_DLOG("Could not read public key from DER input");
    }
  }

  return res;
}


inline evp_key_ptr
try_extract_evp_privkey(bio_ptr & keybio, std::string const & password)
{
  BIO_reset(keybio.get());
  ERR_clear_error();

  evp_key_ptr res;
  res.reset(
    PEM_read_bio_PrivateKey(keybio.get(),
      nullptr, nullptr,
      const_cast<char *>(password.c_str())),
    EVP_PKEY_free
  );
  if (!res) {
    OPENSSL_DLOG("Could not read private key from PEM input");

    BIO_reset(keybio.get());
    ERR_clear_error();

    res.reset(
      d2i_PrivateKey_bio(keybio.get(), nullptr),
      EVP_PKEY_free
    );
    if (!res) {
      OPENSSL_DLOG("Could not read private key from DER input");
    }
  }

  return res;
}



inline evp_key_ptr
generate_key_dsa(size_t bits)
{
  key_generator keygen{EVP_PKEY_DSA};

  if (!keygen.set_dsa_bits(bits)) {
    return {};
  }

  return keygen.generate();
}



inline evp_key_ptr
generate_key_rsa(size_t bits)
{
  key_generator keygen{EVP_PKEY_RSA};

  if (!keygen.set_rsa_bits(bits)) {
    return {};
  }

  return keygen.generate();
}



inline evp_key_ptr
generate_key_ecdsa(std::string const & curve)
{
  // First, see if we can get an appropriate NID. If not, then we know the
  // parameter was bad.
  int nid = OBJ_sn2nid(curve.c_str());
  if (NID_undef == nid) {
    OPENSSL_ERRLOG("Bad curve selected: " << curve);
    return {};
  }

  key_generator keygen{EVP_PKEY_EC};

  if (!keygen.set_ec_curve_nid(nid)) {
    return {};
  }

  return keygen.generate();
}



inline evp_key_ptr
generate_key_edwards(std::string const & parameter)
{
  auto curve = liberate::string::to_lower(parameter);
  int kt = 0;
  if (curve == "ed25519") {
    kt = EVP_PKEY_ED25519;
  }
  else if (curve == "ed448") {
    kt = EVP_PKEY_ED448;
  }
  else {
    LIBLOG_ERROR("Bad curve selected: " << curve);
    return {};
  }

  key_generator keygen{kt};

  return keygen.generate();
}


inline error_t
write_pubkey_bio(evp_key_ptr evp_key, bio_ptr & keybio, key_encoding encoding)
{
  if (KE_PEM == encoding) {
    int success = PEM_write_bio_PUBKEY(keybio.get(), evp_key.get());
    if (!success) {
      OPENSSL_ERRLOG("Failed to write public key in PEM encoding");
      return ERR_ENCODE;
    }
    return ERR_SUCCESS;
  }

  if (KE_DER == encoding) {
    int success = i2d_PUBKEY_bio(keybio.get(), evp_key.get());
    if (!success) {
      OPENSSL_ERRLOG("Failed to write public key in DER encoding");
      return ERR_ENCODE;
    }
    return ERR_SUCCESS;
  }

  // TODO
  return ERR_UNEXPECTED;
}



inline error_t
write_privkey_bio(evp_key_ptr evp_key, bio_ptr & keybio, key_encoding encoding)
{
  if (KE_PEM == encoding) {
    int success = PEM_write_bio_PKCS8PrivateKey(keybio.get(), evp_key.get(),
        nullptr, nullptr, 0, nullptr, nullptr);
    if (!success) {
      OPENSSL_ERRLOG("Failed to write private key in PEM encoding");
      return ERR_ENCODE;
    }
    return ERR_SUCCESS;
  }

  if (KE_DER == encoding) {
    int success = i2d_PrivateKey_bio(keybio.get(), evp_key.get());
    if (!success) {
      OPENSSL_ERRLOG("Failed to write private key in DER encoding");
      return ERR_ENCODE;
    }
    return ERR_SUCCESS;
  }

  // TODO
  return ERR_UNEXPECTED;
}


inline error_t
write_privkey_bio_encrypted(evp_key_ptr evp_key, bio_ptr & keybio, key_encoding encoding,
    std::string const & password)
{
  if (KE_PEM == encoding) {
    int success = PEM_write_bio_PKCS8PrivateKey(keybio.get(), evp_key.get(),
        EVP_des_ede3_cbc(),
        nullptr, 0, nullptr,
        const_cast<void *>(static_cast<void const *>(password.c_str())));
    if (!success) {
      OPENSSL_ERRLOG("Failed to write encrypted private key in PEM encoding");
      return ERR_ENCODE;
    }
    return ERR_SUCCESS;
  }

  if (KE_DER == encoding) {
    LIBLOG_ERROR("Not supported by OpenSSL; see https://gist.github.com/bertjwregeer/5300110#file-d2i_pkcs8privatekey-c");
    return ERR_UNSUPPORTED;
#if 0
    int success = i2d_PKCS8PrivateKey_bio(keybio.get(), evp_key.get(),
        EVP_des_ede3_cbc(),
        nullptr, 0, nullptr,
        const_cast<void *>(static_cast<void const *>(password.c_str())));
    if (!success) {
      OPENSSL_ERRLOG("Failed to write encrypted private key in DER encoding");
      return ERR_ENCODE;
    }
    return ERR_SUCCESS;
#endif
  }

  // TODO
  return ERR_UNEXPECTED;
}


inline int
extract_bits(evp_key_ptr key)
{
  if (!key) {
    return -1;
  }

  int type = EVP_PKEY_base_id(key.get());
  switch (type) {
    case EVP_PKEY_RSA:
    case EVP_PKEY_DSA:
    case EVP_PKEY_EC:
      // XXX: The function is EVP_PKEY_get_bits, but for compatibility with 1.x
      //      use the alias/macro.
      return EVP_PKEY_bits(key.get());

    case EVP_PKEY_ED25519:
      return 25519;

    case EVP_PKEY_ED448:
      return 448;

    case EVP_PKEY_NONE:
    default:
      return -1;
  }
}


inline key_type
extract_key_type(evp_key_ptr key)
{
  if (!key) {
    return KT_UNKNOWN;
  }

  int type = EVP_PKEY_base_id(key.get());
  switch (type) {
    case EVP_PKEY_RSA:
      return KT_RSA;

    case EVP_PKEY_DSA:
      return KT_DSA;

    case EVP_PKEY_EC:
      return KT_EC;

    case EVP_PKEY_ED25519:
    case EVP_PKEY_ED448:
      return KT_ED;

    case EVP_PKEY_NONE:
    default:
      return KT_UNKNOWN;
  }
}

} // namespace openssl


using raw_func = std::function<EVP_PKEY * (int, ENGINE *,
    unsigned char const *, size_t)>;


inline evp_key_ptr
try_extract_raw_key(raw_func func, void const * buffer, size_t bufsize)
{
  // Ed25519
  // - 32 in OpenSSL
  // - 64 in libsodium
  // See https://crypto.stackexchange.com/questions/54353/why-are-nacl-secret-keys-64-bytes-for-signing-but-32-bytes-for-box/
  //
  // Ed448: 57

  if (bufsize < 32) {
    LIBLOG_DEBUG("Buffer definitely too small, can't read raw key.");
    return {};
  }

  if (bufsize >= 57) {
    // Try ed448 first; if that fails, try ed22519.
    evp_key_ptr ptr{
      func(
          EVP_PKEY_ED448, NULL,
          static_cast<unsigned char const *>(buffer), bufsize),
      EVP_PKEY_free
    };
    if (ptr) {
      return ptr;
    }
  }

  // Try ed25519
  evp_key_ptr ptr{
    func(
        EVP_PKEY_ED25519, NULL,
        static_cast<unsigned char const *>(buffer), bufsize),
    EVP_PKEY_free
  };
  return ptr;
}

inline evp_key_ptr
try_extract_raw_privkey(void const * buffer, size_t bufsize)
{
  return try_extract_raw_key(EVP_PKEY_new_raw_private_key, buffer, bufsize);

}

inline evp_key_ptr
try_extract_raw_pubkey(void const * buffer, size_t bufsize)
{
  return try_extract_raw_key(EVP_PKEY_new_raw_public_key, buffer, bufsize);
}





inline std::tuple<evp_key_ptr, key_type, int>
openssl_read_pubkey(
    void const * buffer, size_t bufsize,
    std::string const & password)
{
  using namespace openssl;

  ERR_clear_error();

  // Create BIO from input.
  bio_ptr bio{
    BIO_new_mem_buf(buffer, bufsize),
    BIO_free_all};

  // Try PEM first
  auto cert = try_extract_pem_cert(bio, password);
  if (!cert) {
    // Try DER
    cert = try_extract_der_cert(bio, password);
  }

  // With the cert in hand, try to extract a pubkey. If there is no cert,
  // try to read the pubkey directly.
  evp_key_ptr pubkey;
  if (cert) {
    pubkey = try_extract_evp_pubkey(cert);
  }
  else {
    pubkey = try_extract_evp_pubkey(bio, password, true);
  }

  if (!pubkey) {
    // This might be a raw key
    pubkey = try_extract_raw_pubkey(buffer, bufsize);
    if (!pubkey) {
      throw exception{ERR_DECODE, "No public key in input!"};
    }
  }

  return {pubkey, extract_key_type(pubkey), extract_bits(pubkey)};
}




inline std::tuple<evp_key_ptr, key_type, int>
openssl_read_privkey(
    void const * buffer, size_t bufsize,
    std::string const & password)
{
  using namespace openssl;

  ERR_clear_error();

  // Create BIO from input.
  bio_ptr bio{
    BIO_new_mem_buf(buffer, bufsize),
    BIO_free_all};

  // With the bio in hand, try to extract a privkey.
  evp_key_ptr privkey = try_extract_evp_privkey(bio, password);
  if (!privkey) {
    privkey = try_extract_raw_privkey(buffer, bufsize);
    if (!privkey) {
      throw exception{ERR_DECODE, "No private key in input!"};
    }
  }

  return {privkey, extract_key_type(privkey), extract_bits(privkey)};
}



inline evp_key_ptr
openssl_try_extract_pubkey(evp_key_ptr privkey)
{
  using namespace openssl;
  ERR_clear_error();

  // There is only one way OpenSSL has for converting a private key to a public
  // key, and that is to write the key to PEM and load the public key from that.
  // We can use an in-memory BIO for this.
  bio_ptr keybio(BIO_new(BIO_s_mem()),
      BIO_free_all);

  // Try to write the public key
  if (!PEM_write_bio_PUBKEY(keybio.get(), privkey.get())) {
    OPENSSL_DLOG("Could not serialize private key");
  }

  // Now try to extract a pubkey from the BIO
  return try_extract_evp_pubkey(keybio, {}, false);
}



inline evp_key_ptr
openssl_generate_key(key_type type, std::string const & parameters)
{
  using namespace openssl;

  // TODO probably initialize random number generator here

  switch (type) {
    case KT_DSA:
      return generate_key_dsa(std::stoi(parameters));

    case KT_RSA:
      return generate_key_rsa(std::stoi(parameters));

    case KT_EC:
      return generate_key_ecdsa(parameters);

    case KT_ED:
      return generate_key_edwards(parameters);

    default:
      break;
  }

  return {};
}



inline error_t
openssl_write_key_bio(evp_key_ptr evp_key,
    bool is_public,
    void * buffer, size_t bufsize, size_t & written,
    key_encoding encoding, std::string const & password)
{
  // Create a BIO structure; unfortunately, OpenSSL does not know how to write
  // to a BIO_new_mem_buf() directly. So the writing of the key will always
  // succeed, but copying the result to the buffer may not (below).
  openssl::bio_ptr keybio{
    BIO_new(BIO_s_mem()),
    BIO_free_all
  };

  // Write public or private key?
  error_t err = ERR_UNEXPECTED;
  if (is_public) {
    err = openssl::write_pubkey_bio(evp_key, keybio, encoding);
  }
  else {
    if (password.empty()) {
      err = openssl::write_privkey_bio(evp_key, keybio, encoding);
    }
    else {
      err = openssl::write_privkey_bio_encrypted(evp_key, keybio, encoding,
          password);
    }
  }

  // Adjust the written length on success
  if (ERR_SUCCESS != err) {
    written = 0;
    return err;
  }

  // Now we have to copy the BIO buffer to the output buffer. First we need
  // to check the BIO length.
  char * data = nullptr;
  auto amount = BIO_get_mem_data(keybio.get(), &data);
  if (amount < 0 || static_cast<size_t>(amount) > bufsize) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  // Copy the buffer
  std::memcpy(buffer, data, amount); // flawfinder: ignore
  written = amount;

  return ERR_SUCCESS;
}


inline error_t
openssl_write_key_raw(evp_key_ptr evp_key,
    bool is_public,
    void * buffer, size_t bufsize, size_t & written)
{
  size_t len = bufsize;

  int success = 0;
  if (is_public) {
    success = EVP_PKEY_get_raw_public_key(evp_key.get(),
        static_cast<unsigned char *>(buffer), &len);
  }
  else {
    success = EVP_PKEY_get_raw_private_key(evp_key.get(),
        static_cast<unsigned char *>(buffer), &len);
  }

  if (!success) {
    OPENSSL_ERRLOG("Failed to write raw key");
    written = 0;
    return ERR_ENCODE;
  }

  written = len;
  return ERR_SUCCESS;
}


inline error_t
openssl_write_key(evp_key_ptr evp_key,
    bool is_public,
    void * buffer, size_t bufsize, size_t & written,
    key_encoding encoding, std::string const & password)
{
  if (KE_RAW == encoding) {
    if (!password.empty()) {
      LIBLOG_DEBUG("Passwords not supported with raw encoding.");
      return ERR_UNSUPPORTED;
    }

    return openssl_write_key_raw(evp_key, is_public, buffer, bufsize,
        written);
  }

  return openssl_write_key_bio(evp_key, is_public, buffer, bufsize, written,
      encoding, password);
}


} // anonymous namespace
} // namespace s3kr1t

#endif // guard
