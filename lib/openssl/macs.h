/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_OPENSSL_MACS_H
#define S3KR1T_OPENSSL_MACS_H

#include <build-config.h>

#ifndef S3KR1T_HAVE_OPENSSL
#error Bad include; OpenSSL not found.
#endif

#include <cstring>

#include <openssl/evp.h>

#include <s3kr1t/error.h>

#include "error.h"

namespace s3kr1t {
namespace {

inline constexpr
char const *
get_mac(mac_type mtype)
{
  switch (mtype) {
    case MT_POLY1305:
      return "POLY1305";
    case MT_KMAC_128:
      return "KMAC-128";
    case MT_KMAC_256:
      return "KMAC-256";

    default:
      break;
  }
  return nullptr;
}



inline error_t
openssl_mac_internal(unsigned char * macbuf, size_t macbuf_size, size_t & required,
    void const * data, size_t data_size,
    void const * key, size_t key_size,
    void const * extra, size_t extra_size,
    mac_type mtype, error_t macerr)
{
  // Additional checks
  if (MT_POLY1305 == mtype) {
    if (key_size < 32) {
      LIBLOG_ERROR("Poly1305 needs a key of 32 Bytes.");
      return ERR_INVALID_VALUE;
    }
  }

  // Set extra parameters.
  std::unique_ptr<OSSL_PARAM[]> params;
  uint32_t size = 0;
  if (MT_POLY1305 == mtype) {
    if (!extra || 12 != extra_size) {
      // Be precise with the 12 Bytes here due to
      // https://nvd.nist.gov/vuln/detail/CVE-2019-1543
      LIBLOG_ERROR("Poly1305 needs an IV of 12 Bytes.");
      return ERR_INVALID_VALUE;
    }

    params = std::make_unique<OSSL_PARAM[]>(2);
    params[0] = OSSL_PARAM_construct_octet_string("iv",
        const_cast<void *>(extra), extra_size);
    params[1] = OSSL_PARAM_construct_end();
  }
  else if (MT_KMAC_128 == mtype || MT_KMAC_256 == mtype) {
    if (extra && extra_size) {
      params = std::make_unique<OSSL_PARAM[]>(3);
      params[0] = OSSL_PARAM_construct_octet_string("custom",
          const_cast<void *>(extra), extra_size);
      // NIST.SP.800-185 recommends > 64 *bits* / 8 bytes
      size = (MT_KMAC_128 == mtype) ? 16 : 32;
      params[1] = OSSL_PARAM_construct_uint32("size", &size);
      params[2] = OSSL_PARAM_construct_end();
    }
  }

  // Create MAC algo
  auto algo = std::unique_ptr<EVP_MAC, decltype(&EVP_MAC_free)>{
    EVP_MAC_fetch(nullptr, get_mac(mtype), nullptr),
    EVP_MAC_free
  };
  if (!algo) {
    LIBLOG_ERROR("Unsupported MAC type: " << mtype);
    return ERR_UNSUPPORTED;
  }

  // Create context
  auto ctx = std::unique_ptr<EVP_MAC_CTX, decltype(&EVP_MAC_CTX_free)>{
    EVP_MAC_CTX_new(algo.get()),
    EVP_MAC_CTX_free
  };
  if (!ctx) {
    OPENSSL_ERRLOG("Could not create MAC context");
    return macerr;
  }


  // Initialize MAC context
  int success = EVP_MAC_init(ctx.get(),
      static_cast<unsigned char const *>(key), key_size,
      params.get());
  if (!success) {
    OPENSSL_ERRLOG("Unable to initialze MAC");
    return macerr;
  }

  // Update digest with message
  success = EVP_MAC_update(ctx.get(),
      static_cast<unsigned char const *>(data), data_size);
  if (!success) {
    OPENSSL_ERRLOG("Unable to update MAC");
    return macerr;
  }

  // Put MAC in temporary buffer
  size_t maclen = 0;
  success = EVP_MAC_final(ctx.get(), macbuf, &maclen, macbuf_size);
  if (!success) {
    OPENSSL_ERRLOG("Unable to calculate final MAC");
    return macerr;
  }

  // All good
  required = maclen;
  return ERR_SUCCESS;
}



inline error_t
openssl_mac(void * macbuf, size_t macbuf_size, size_t & required,
    void const * data, size_t data_size,
    void const * key, size_t key_size,
    void const * extra, size_t extra_size,
    mac_type mtype)
{
  // Calculate mac with a temporary buffer that is of guaranteed good
  // size.
  unsigned char tmp[EVP_MAX_MD_SIZE]; // flawfinder: ignore
  auto err = openssl_mac_internal(tmp, sizeof(tmp), required,
      data, data_size, key, key_size, extra, extra_size,
      mtype, ERR_MAC);
  if (ERR_SUCCESS != err) {
    return err;
  }

  // Check the output buffer has the correct size
  if (required > macbuf_size || required > sizeof(tmp)) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  // If this was ok, then copy the buffer.
  std::memcpy(macbuf, tmp, required); // flawfinder: ignore
  return ERR_SUCCESS;
}



inline error_t
openssl_check(void const * data, size_t data_size,
    void const * macbuf, size_t macbuf_size,
    void const * key, size_t key_size,
    void const * extra, size_t extra_size,
    mac_type mtype)
{
  // Calculate mac with a temporary buffer that is of guaranteed good
  // size.
  unsigned char tmp[EVP_MAX_MD_SIZE]; // flawfinder: ignore
  size_t required = 0;
  auto err = openssl_mac_internal(tmp, sizeof(tmp), required,
      data, data_size, key, key_size, extra, extra_size,
      mtype, ERR_MAC);
  if (ERR_SUCCESS != err) {
    return err;
  }

  // Check the mac buffer has the correct size
  if (required > macbuf_size || required > sizeof(tmp)) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  // Do a memory comparison
  int cmp = std::memcmp(tmp, macbuf, required); // flawfinder: ignore
  if (0 == cmp) {
    return ERR_SUCCESS;
  }
  return ERR_VERIFY;
}

} // anonymous namespace
} // namespace s3kr1t

#endif // guard
