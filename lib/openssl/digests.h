/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_OPENSSL_DIGESTS_H
#define S3KR1T_OPENSSL_DIGESTS_H

#include <build-config.h>

#ifndef S3KR1T_HAVE_OPENSSL
#error Bad include; OpenSSL not found.
#endif

#include <cstring>

#include <openssl/evp.h>

#include <s3kr1t/error.h>

#include "error.h"

namespace s3kr1t {
namespace {
inline EVP_MD const *
get_md(digest_type dtype)
{
  // See https://www.openssl.org/docs/manmaster/man3/EVP_DigestSignInit.html
  switch (dtype) {
    case DT_SHA2_224:
      return EVP_sha224();
    case DT_SHA2_256:
      return EVP_sha256();
    case DT_SHA2_384:
      return EVP_sha384();
    case DT_SHA2_512:
      return EVP_sha512();
#if defined(S3KR1T_HAVE_OPENSSL_SHA3)
    case DT_SHA3_224:
      return EVP_sha3_224();
    case DT_SHA3_256:
      return EVP_sha3_256();
    case DT_SHA3_384:
      return EVP_sha3_384();
    case DT_SHA3_512:
      return EVP_sha3_512();
#endif
    default:
      break;
  }
  return nullptr;
}



inline error_t
openssl_digest_internal(unsigned char * digbuf, size_t digbuf_size, size_t & required,
    void const * data, size_t data_size,
    digest_type dtype, error_t digerr)
{
  // Input are verified; fetch md
  auto md = get_md(dtype);
  if (!md) {
    LIBLOG_ERROR("Unsupported digest type: " << dtype);
    return ERR_UNSUPPORTED;
  }

  // Create context
  auto ctx = std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_free)>{
    EVP_MD_CTX_create(),
    EVP_MD_CTX_free
  };
  if (!ctx) {
    OPENSSL_ERRLOG("Could not create digest context");
    return digerr;
  }

  // Initialize digest context
  int success = EVP_DigestInit_ex(ctx.get(), md, nullptr);
  if (!success) {
    OPENSSL_ERRLOG("Unable to initialze digest");
    return digerr;
  }

  // Update digest with message
  success = EVP_DigestUpdate(ctx.get(), data, data_size);
  if (!success) {
    OPENSSL_ERRLOG("Unable to update digest");
    return digerr;
  }

  // Put digest in temporary buffer
  unsigned int diglen = digbuf_size;
  success = EVP_DigestFinal_ex(ctx.get(), digbuf, &diglen);
  if (!success) {
    OPENSSL_ERRLOG("Unable to calculate final digest");
    return digerr;
  }

  // All good
  required = diglen;
  return ERR_SUCCESS;
}



inline error_t
openssl_digest(void * digbuf, size_t digbuf_size, size_t & required,
    void const * data, size_t data_size,
    digest_type dtype)
{
  // Calculate digest with a temporary buffer that is of guaranteed good
  // size.
  unsigned char tmp[EVP_MAX_MD_SIZE]; // flawfinder: ignore
  auto err = openssl_digest_internal(tmp, sizeof(tmp), required,
      data, data_size, dtype, ERR_DIGEST);
  if (ERR_SUCCESS != err) {
    return err;
  }

  // Check the output buffer has the correct size
  if (required > digbuf_size || required > sizeof(tmp)) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  // If this was ok, then copy the buffer.
  std::memcpy(digbuf, tmp, required); // flawfinder: ignore
  return ERR_SUCCESS;
}



inline error_t
openssl_check(void const * data, size_t data_size,
    void const * digbuf, size_t digbuf_size,
    digest_type dtype)
{
  // Calculate digest with a temporary buffer that is of guaranteed good
  // size.
  unsigned char tmp[EVP_MAX_MD_SIZE]; // flawfinder: ignore
  size_t required = 0;
  auto err = openssl_digest_internal(tmp, sizeof(tmp), required,
      data, data_size, dtype, ERR_DIGEST);
  if (ERR_SUCCESS != err) {
    return err;
  }

  // Check the digest buffer has the correct size
  if (required > digbuf_size || required > sizeof(tmp)) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  // Do a memory comparison
  int cmp = std::memcmp(tmp, digbuf, required); // flawfinder: ignore
  if (0 == cmp) {
    return ERR_SUCCESS;
  }
  return ERR_VERIFY;
}

} // anonymous namespace
} // namespace s3kr1t

#endif // guard
