/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_OPENSSL_SIGNATURES_H
#define S3KR1T_OPENSSL_SIGNATURES_H

#include <build-config.h>

#ifndef S3KR1T_HAVE_OPENSSL
#error Bad include; OpenSSL not found.
#endif

#include <openssl/evp.h>

#include <s3kr1t/error.h>

#include "error.h"
#include "keys.h"

namespace s3kr1t {
namespace {

inline std::tuple<bool, EVP_MD const *>
get_md(key_type ktype, digest_type dtype)
{
  if (KT_ED == ktype) {
    // See https://www.openssl.org/docs/manmaster/man7/Ed25519.html
    // The digest algorithm is selected by RFC 8032; we cannot choose
    // it.
    return {true, nullptr};
  }

  // See https://www.openssl.org/docs/manmaster/man3/EVP_DigestSignInit.html
  if (KT_DSA == ktype) {
    switch (dtype) {
        case DT_SHA2_224:
        case DT_SHA2_256:
        case DT_SHA2_384:
        case DT_SHA2_512:
        // Supported
        break;

      default:
        // Unsupported
        return {false, nullptr};
    }
  }

  if (KT_RSA == ktype) {
    switch (dtype) {
        case DT_SHA2_224:
        case DT_SHA2_256:
        case DT_SHA2_384:
        case DT_SHA2_512:

        case DT_SHA3_224:
        case DT_SHA3_256:
        case DT_SHA3_384:
        case DT_SHA3_512:
        // Supported
        break;

      default:
        // Unsupported
        return {false, nullptr};
    }
  }

  // TODO ecdsa

  // Finally if the checks are done, call the appropriate digest function.
  switch (dtype) {
    case DT_SHA2_224:
      return {true, EVP_sha224()};
    case DT_SHA2_256:
      return {true, EVP_sha256()};
    case DT_SHA2_384:
      return {true, EVP_sha384()};
    case DT_SHA2_512:
      return {true, EVP_sha512()};
    case DT_SHA3_224:
      return {true, EVP_sha3_224()};
    case DT_SHA3_256:
      return {true, EVP_sha3_256()};
    case DT_SHA3_384:
      return {true, EVP_sha3_384()};
    case DT_SHA3_512:
      return {true, EVP_sha3_512()};
    default:
      break;
  }
  return {false, nullptr};
}


inline error_t
openssl_sign(void * sigbuf, size_t sigbuf_size, size_t & required,
    void const * data, size_t data_size,
    evp_key_ptr key, key_type ktype, digest_type dtype)
{
  // Input are verified; fetch md
  auto [matched, md] = get_md(ktype, dtype);
  if (!matched) {
    LIBLOG_ERROR("Unsupported digest type: " << static_cast<int>(dtype));
    return ERR_UNSUPPORTED;
  }

  // Create context
  auto ctx = std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_free)>{
    EVP_MD_CTX_create(),
    EVP_MD_CTX_free
  };
  if (!ctx) {
    OPENSSL_ERRLOG("Could not create signature context");
    return ERR_SIGN;
  }

  // Initialize with digest function
  int success = EVP_DigestSignInit(ctx.get(), nullptr, md, nullptr, key.get());
  if (!success) {
    OPENSSL_ERRLOG("Could not initialize signature context");
    return ERR_SIGN;
  }

  // Calculate digest. With Edwards keys, we have to use the one-shot
  // EVP_DigestSign() function. Since that's how the API is constructed
  // anyway, we're just going to do this for all keys. The stream API
  // usage stays in case we'll want to support it later on.
#if 0
  success = EVP_DigestSignUpdate(ctx.get(), data, data_size);
  if (!success) {
    OPENSSL_ERRLOG("Could not calculate digest");
    return ERR_SIGN;
  }

  // Obtain signature length
  size_t siglen = 0;
  success = EVP_DigestSignFinal(ctx.get(), nullptr, &siglen);
  if (!success) {
    OPENSSL_ERRLOG("Unable to determine signature length");
    return ERR_SIGN;
  }

  // Bail out if the signature size is too large for the buffer
  if (siglen > sigbuf_size) {
    required = siglen;
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  // Copy signature
  siglen = sigbuf_size;
  success = EVP_DigestSignFinal(ctx.get(),
      static_cast<unsigned char *>(sigbuf), &siglen);
  if (!success) {
    OPENSSL_ERRLOG("Unable to copy signature");
    return ERR_SIGN;
  }
#else
  // Obtain signature length
  size_t siglen = 0;
  success = EVP_DigestSign(ctx.get(), nullptr, &siglen, nullptr, 0);
  if (!success) {
    OPENSSL_ERRLOG("Unable to determine signature length");
    return ERR_SIGN;
  }

  // Bail out if the signature size is too large for the buffer
  if (siglen > sigbuf_size) {
    required = siglen;
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  // Create signature
  siglen = sigbuf_size;
  success = EVP_DigestSign(ctx.get(),
      static_cast<unsigned char *>(sigbuf), &siglen,
      static_cast<unsigned char const *>(data), data_size);
  if (!success) {
    OPENSSL_ERRLOG("Unable to create signature");
    return ERR_SIGN;
  }
#endif

  required = siglen;
  return ERR_SUCCESS;
}


inline error_t
openssl_verify(void const * data, size_t data_size,
    void const * sigbuf, size_t sigbuf_size,
    evp_key_ptr key, key_type ktype, digest_type dtype)
{
  // Input are verified; fetch md
  auto [matched, md] = get_md(ktype, dtype);
  if (!matched) {
    LIBLOG_ERROR("Unsupported digest type: " << dtype);
    return ERR_UNSUPPORTED;
  }

  // Create context
  auto ctx = std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_free)>{
    EVP_MD_CTX_create(),
    EVP_MD_CTX_free
  };
  if (!ctx) {
    OPENSSL_ERRLOG("Could not create signature context");
    return ERR_VERIFY;
  }

  // Initialize with digest function
  int success = EVP_DigestVerifyInit(ctx.get(), nullptr, md, nullptr, key.get());
  if (!success) {
    OPENSSL_ERRLOG("Could not initialize signature context");
    return ERR_VERIFY;
  }

  // Calculate digest - see sign() above for rationale on one-shot and commented
  // out code.
#if 0
  success = EVP_DigestVerifyUpdate(ctx.get(), data, data_size);
  if (!success) {
    OPENSSL_ERRLOG("Could not calculate digest");
    return ERR_VERIFY;
  }

  // Verify signature
  success = EVP_DigestVerifyFinal(ctx.get(),
      static_cast<unsigned char const *>(sigbuf), sigbuf_size);
  if (!success) {
    OPENSSL_ERRLOG("Unable to verify signature");
    return ERR_VERIFY;
  }
#else
  success = EVP_DigestVerify(ctx.get(),
      static_cast<unsigned char const *>(sigbuf), sigbuf_size,
      static_cast<unsigned char const *>(data), data_size);
  if (!success) {
    OPENSSL_ERRLOG("Unable to verify signature");
    return ERR_VERIFY;
  }
#endif

  return ERR_SUCCESS;
}




} // anonymous namespace
} // namespace s3kr1t

#endif // guard
