/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <s3kr1t/error.h>

/**
 * Stringify the symbol passed to S3KR1T_SPRINGIFY()
 **/
#define S3KR1T_STRINGIFY(n) S3KR1T_STRINGIFY_HELPER(n)
#define S3KR1T_STRINGIFY_HELPER(n) #n


namespace s3kr1t {

/**
 * (Un-)define macros to ensure error defintions are being generated.
 **/
#define S3KR1T_START_ERRORS namespace { \
  static const struct { \
    char const * const name; \
    uint32_t code; \
    char const * const message; \
  } error_table[] = {
#define S3KR1T_ERRDEF(name, code, desc) \
  { S3KR1T_STRINGIFY(name), code, desc },
#define S3KR1T_END_ERRORS { NULL, 0, NULL } }; }

#undef S3KR1T_ERROR_H
#include <s3kr1t/error.h>



/*****************************************************************************
 * Functions
 **/


char const *
error_message(error_t code)
{
  if (code >= S3KR1T_ERROR_LAST) {
    return "unidentified error";
  }

  for (uint32_t i = 0
      ; i < static_cast<error_t>(sizeof(error_table) / sizeof(error_table[0]))
      ; ++i)
  {
    if (static_cast<uint32_t>(code) == error_table[i].code) {
      return error_table[i].message;
    }
  }

  return "unidentified error";
}



char const *
error_name(error_t code)
{
  if (code >= S3KR1T_ERROR_LAST) {
    return "unidentified error";
  }

  for (uint32_t i = 0
      ; i < static_cast<error_t>(sizeof(error_table) / sizeof(error_table[0]))
      ; ++i)
  {
    if (static_cast<uint32_t>(code) == error_table[i].code) {
      return error_table[i].name;
    }
  }

  return "unidentified error";
}


/*****************************************************************************
 * Exception
 **/
namespace {

// Helper function for trying to create a verbose error message. This may fail
// if there can't be any more allocations, for example, so be extra careful.
void
combine_error(std::string & result, error_t code,
    std::string const & details)
{
  try {
    result = "[" + std::string{error_name(code)} + "] ";
    result += std::string{error_message(code)};
    if (!details.empty()) {
      result += " // ";
      result += details;
    }
  } catch (...) {
    result = "Error copying error message.";
  }
}

} // anonymous namespace


exception::exception(error_t code,
    std::string const & details /* = "" */) throw()
  : std::runtime_error("")
  , m_code(code)
{
  combine_error(m_message, m_code, details);
}



exception::~exception() throw()
{
}



char const *
exception::what() const throw()
{
  if (m_message.empty()) {
    return error_message(m_code);
  }
  return m_message.c_str();
}



char const *
exception::name() const throw()
{
  return error_name(m_code);
}



error_t
exception::code() const throw()
{
  return m_code;
}


} // namespace s3kr1t
