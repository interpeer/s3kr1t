/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <s3kr1t/signatures.h>

#include <build-config.h>

#ifdef S3KR1T_HAVE_OPENSSL
#include "openssl/signatures.h"
#endif // S3KR1T_HAVE_OPENSSL

#include "key_impl.h"

#include <liberate/logging.h>

namespace s3kr1t {


S3KR1T_API error_t
sign(void * sigbuf, size_t sigbuf_size, size_t & required,
    void const * data, size_t data_size,
    key const & key, digest_type dtype)
{
  if (!sigbuf || !sigbuf_size || !data || !data_size) {
    LIBLOG_ERROR("Some inputs were missing!");
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  if (!key || !key.is_private()) {
    LIBLOG_ERROR("Need a private key for signing.");
    return ERR_SIGN;
  }

  switch (key.type()) {
    case KT_DSA:
    case KT_RSA:
    case KT_EC:
    case KT_ED:
#ifdef S3KR1T_HAVE_OPENSSL
      return openssl_sign(sigbuf, sigbuf_size, required,
          data, data_size, key.m_impl->ossl_key.evp_key,
          key.type(), dtype);
#else
      break;
#endif // S3KR1T_HAVE_OPENSSL

    // TODO other types of key

    case KT_UNKNOWN:
    default:
      break;
  }

  return ERR_UNSUPPORTED;
}


S3KR1T_API error_t
verify(void const * data, size_t data_size,
    void const * sigbuf, size_t sigbuf_size,
    key const & key, digest_type dtype)
{
  if (!sigbuf || !sigbuf_size || !data || !data_size) {
    LIBLOG_ERROR("Some inputs were missing!");
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  if (!key.is_public()) {
    LIBLOG_ERROR("Need a public key for verifying.");
    return ERR_VERIFY;
  }

  switch (key.type()) {
    case KT_DSA:
    case KT_RSA:
    case KT_EC:
    case KT_ED:
#ifdef S3KR1T_HAVE_OPENSSL
      return openssl_verify(data, data_size, sigbuf, sigbuf_size,
          key.m_impl->ossl_key.evp_key,
          key.type(), dtype);
#else
      break;
#endif // S3KR1T_HAVE_OPENSSL

    // TODO other types of key

    case KT_UNKNOWN:
    default:
      break;
  }

  return ERR_UNSUPPORTED;

}



} // namespace s3kr1t
