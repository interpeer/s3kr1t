/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <s3kr1t/digests.h>

#include <build-config.h>

#ifdef S3KR1T_HAVE_OPENSSL
#include "openssl/digests.h"
#endif // S3KR1T_HAVE_OPENSSL

#include <liberate/logging.h>

namespace s3kr1t {

error_t
digest(void * digbuf, size_t digbuf_size, size_t & required,
    void const * data, size_t data_size,
    digest_type dtype)
{
  if (!data || !data_size || !digbuf || !digbuf_size) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  required = 0;

  switch (dtype) {
    case DT_SHA2_224:
    case DT_SHA2_256:
    case DT_SHA2_384:
    case DT_SHA2_512:
    case DT_SHA3_224:
    case DT_SHA3_256:
    case DT_SHA3_384:
    case DT_SHA3_512:

#ifdef S3KR1T_HAVE_OPENSSL
      return openssl_digest(digbuf, digbuf_size, required,
          data, data_size, dtype);
#else
      break;
#endif

      // TODO other types

    default:
      break;
  }

  return ERR_UNSUPPORTED;
}


error_t
check(void const * data, size_t data_size,
    void const * digbuf, size_t digbuf_size,
    digest_type dtype)
{
  if (!data || !data_size || !digbuf || !digbuf_size) {
    return ERR_INSUFFICIENT_BUFFER_SIZE;
  }

  switch (dtype) {
    case DT_SHA2_224:
    case DT_SHA2_256:
    case DT_SHA2_384:
    case DT_SHA2_512:
    case DT_SHA3_224:
    case DT_SHA3_256:
    case DT_SHA3_384:
    case DT_SHA3_512:

#ifdef S3KR1T_HAVE_OPENSSL
      return openssl_check(data, data_size,
          digbuf, digbuf_size,
          dtype);
#else
      break;
#endif

      // TODO other types

    default:
      break;
  }

  return ERR_UNSUPPORTED;
}



} // namespace s3kr1t
