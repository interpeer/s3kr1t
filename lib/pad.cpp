/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <s3kr1t/pad.h>

#include <build-config.h>

#include <string.h>

#include <liberate/logging.h>

namespace s3kr1t {

S3KR1T_API error_t
pkcs7_pad_flexible(void * buffer, std::size_t const & size)
{
  if (!buffer) {
    LIBLOG_DEBUG("Null buffer cannot be padded.");
    return ERR_INVALID_VALUE;
  }

  if (!size) {
    // All good, silent success
    return ERR_SUCCESS;
  }

  char value = size % 256;

#if defined(S3KR1T_MEMSET_S)
  memset_s(buffer, size, value, size)
#elif defined(S3KR1T_MEMSET_EXPLICIT)
  memset_explicit(buffer, value, size);
#elif defined(S3KR1T_HAVE_EXPLICIT_MEMSET)
  explicit_memset(buffer, value, size);
#else
  memset(buffer, value, size);
#endif

  return ERR_SUCCESS;
}



S3KR1T_API error_t
pkcs7_pad_easy(void const * buffer, std::size_t const & buffer_size,
    void * pad_offset, std::size_t & pad_size)
{
  if (!buffer || !buffer_size) {
    LIBLOG_DEBUG("Null/empty buffer cannot be padded.");
    return ERR_INVALID_VALUE;
  }
  if (!pad_offset) {
    LIBLOG_DEBUG("Null pad offset cannot be used.");
    return ERR_INVALID_VALUE;
  }

  auto buf = static_cast<char const *>(buffer);
  auto offset = static_cast<char *>(pad_offset);
  if (offset < buf || offset > buf + buffer_size) {
    LIBLOG_DEBUG("Offset is not within bufer, won't pad.");
    return ERR_INVALID_VALUE;
  }

  if (offset == buf + buffer_size) {
    // Special case, this is something we can expect to happen in
    // code that takes buffer management a bit easier.
    return ERR_SUCCESS;
  }

  // Looks like valid parameters, now calculate the padding size.
  pad_size = buf + buffer_size - offset;

  return pkcs7_pad_flexible(offset, pad_size);
}



} // namespace s3kr1t
