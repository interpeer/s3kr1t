/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_KEY_IMPL_H
#define S3KR1T_KEY_IMPL_H

#include <s3kr1t/keys.h>

#include <build-config.h>

#ifdef S3KR1T_HAVE_OPENSSL
#include "openssl/keys.h"
#endif // S3KR1T_HAVE_OPENSSL

namespace s3kr1t {

struct key::key_impl
{
#ifdef S3KR1T_HAVE_OPENSSL
  openssl_key                 ossl_key = {};
#endif // S3KR1T_HAVE_OPENSSL

  bool                        is_public;
  size_t                      bits;

#ifdef S3KR1T_HAVE_OPENSSL
  key_impl(evp_key_ptr key, key_type _type, bool _is_public, size_t _bits)
    : ossl_key{key, _type}
    , is_public(_is_public)
    , bits{_bits}
  {
  }

  inline key_type
  type()
  {
    return ossl_key.type;
  }
#endif // S3KR1T_HAVE_OPENSSL
};


} // namespace s3kr1t

#endif // guard
