/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <s3kr1t/pad.h>


namespace {

inline void check_value(char const * buffer, std::size_t size, char value)
{
  auto buf = buffer;
  for (std::size_t i = 0 ; i < size ; ++buf, ++i) {
    ASSERT_EQ(value, *buf);
  }
}


} // anonymous namespace

TEST(Pad, flexible_bad_inputs)
{
  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, s3kr1t::pkcs7_pad_flexible(nullptr, 42));
}


TEST(Pad, flexible_zero_length)
{
  char buf[] = "foo";
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, s3kr1t::pkcs7_pad_flexible(buf, 0));
}


TEST(Pad, flexible_length_short)
{
  char buf[5] = "foo";
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, s3kr1t::pkcs7_pad_flexible(buf + 3, sizeof(buf) - 3));
  check_value(buf + 3, sizeof(buf) - 3, 2);
}


TEST(Pad, flexible_length_long)
{
  {
    char buf[259] = "foo";
    ASSERT_EQ(s3kr1t::ERR_SUCCESS, s3kr1t::pkcs7_pad_flexible(buf + 3, sizeof(buf) - 3));
    check_value(buf + 3, sizeof(buf) - 3, 0);
  }

  {
    char buf[260] = "foo";
    ASSERT_EQ(s3kr1t::ERR_SUCCESS, s3kr1t::pkcs7_pad_flexible(buf + 3, sizeof(buf) - 3));
    check_value(buf + 3, sizeof(buf) - 3, 1);
  }
}


TEST(Pad, easy_bad_inputs)
{
  char buf[300] = "foo";
  std::size_t done = 0;

  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, s3kr1t::pkcs7_pad_easy(nullptr, 42, buf, done));
  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, s3kr1t::pkcs7_pad_easy(buf, 0, buf, done));
  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, s3kr1t::pkcs7_pad_easy(buf, sizeof(buf), nullptr, done));
  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, s3kr1t::pkcs7_pad_easy(buf, sizeof(buf), nullptr, done));
  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, s3kr1t::pkcs7_pad_easy(buf, sizeof(buf), buf - 1, done));
  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, s3kr1t::pkcs7_pad_easy(buf, sizeof(buf), buf + sizeof(buf) + 1, done));
}


TEST(Pad, zero_buffer)
{
  char buf[3] = { 'f', 'o', 'o' };
  std::size_t done = 0;
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, s3kr1t::pkcs7_pad_easy(buf, sizeof(buf), buf + 3, done));
  ASSERT_EQ(0, done);
}


TEST(Pad, small_buffer)
{
  char buf[10] = "foo";
  std::size_t done = 0;
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, s3kr1t::pkcs7_pad_easy(buf, sizeof(buf), buf + 3, done));
  ASSERT_EQ(7, done);
  check_value(buf + 3, sizeof(buf) - 3, 7);
}


TEST(Pad, large_buffer)
{
  char buf[300] = "foo";
  std::size_t done = 0;
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, s3kr1t::pkcs7_pad_easy(buf, sizeof(buf), buf + 3, done));
  ASSERT_EQ(297, done);
  check_value(buf + 3, sizeof(buf) - 3, 41);
}
