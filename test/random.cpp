/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <memory>

#include <s3kr1t/random.h>

TEST(Random, get_random_bytes)
{
  char buf[10] = { 0 };
  std::size_t size = sizeof(buf);
  auto err = s3kr1t::get_random_bytes(buf, size);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  ASSERT_EQ(size, sizeof(buf));

  // As a simple, if not *entirely* correct test, check that not all bytes are
  // zero.
  bool non_zero_found = false;
  for (std::size_t i = 0 ; i < sizeof(buf) ; ++i) {
    if (buf[i]) {
      non_zero_found = true;
      break;
    }
  }
  ASSERT_TRUE(non_zero_found);
}



TEST(PRNG, bad_inputs)
{
  s3kr1t::prng prng{nullptr, 0};

  char buf[] = "foo";
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE, prng.generate(nullptr, 42));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE, prng.generate(buf, 0));
}


TEST(PRNG, no_seed_makes_predictable_output)
{
  s3kr1t::prng prng{nullptr, 0};
  ASSERT_TRUE(prng.is_deterministic());
  ASSERT_EQ(0, prng.reseed_count());

  uint32_t expected[] = {
    0x987ef676,
    0x5de988d6,
    0xc4c96e6e,
    0x644ace37,
  };

  for (size_t i = 0 ; i < sizeof(expected) / sizeof(expected[0]) ; ++i) {
    uint32_t value = 0;
    auto err = prng.generate(&value, sizeof(value));
    ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
    // std::cerr << "i: " << i << std::hex << expected[i] << " - " << value << std::dec << std::endl;
    ASSERT_EQ(value, expected[i]);
  }

  ASSERT_EQ(1, prng.reseed_count());
}


TEST(PRNG, static_seed_makes_predictable_output)
{
  uint32_t seed = 0xf007b411;
  s3kr1t::prng prng{&seed, sizeof(seed)};
  ASSERT_TRUE(prng.is_deterministic());
  ASSERT_EQ(0, prng.reseed_count());

  uint32_t expected[] = {
    0x66bee082,
    0xf0ae2345,
    0xa9acf9f9,
    0x5a37a1b3,
  };

  for (size_t i = 0 ; i < sizeof(expected) / sizeof(expected[0]) ; ++i) {
    uint32_t value = 0;
    auto err = prng.generate(&value, sizeof(value));
    ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
    // std::cerr << "i: " << i << std::hex << expected[i] << " - " << value << std::dec << std::endl;
    ASSERT_EQ(value, expected[i]);
  }

  ASSERT_EQ(1, prng.reseed_count());
}


TEST(PRNG, with_dynamic_seed)
{
  s3kr1t::prng prng{s3kr1t::get_random_bytes};
  ASSERT_FALSE(prng.is_deterministic());
  ASSERT_EQ(0, prng.reseed_count());

  uint32_t last = 0;
  for (size_t i = 0 ; i < 100 ; ++i) {
    uint32_t value = 0;
    auto err = prng.generate(&value, sizeof(value));
    ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
    ASSERT_NE(value, last);
    last = value;
  }

  ASSERT_EQ(1, prng.reseed_count());
}


TEST(PRNG, large_amount_of_data)
{
  s3kr1t::prng prng{s3kr1t::get_random_bytes};
  ASSERT_FALSE(prng.is_deterministic());
  ASSERT_EQ(0, prng.reseed_count());

  // Over 4 MiB > should reseed > 2 times, whatever the internals
  char buf[1024];
  for (size_t i = 0 ; i < 4097 ; ++i) {
    auto err = prng.generate(buf, sizeof(buf));
    ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  }

  ASSERT_GT(prng.reseed_count(), 2);
}


TEST(PRNG, single_large_amount_of_data)
{
  s3kr1t::prng prng{s3kr1t::get_random_bytes};
  ASSERT_FALSE(prng.is_deterministic());
  ASSERT_EQ(0, prng.reseed_count());

  // Over 4 MiB > should reseed > 2 times, whatever the internals
  constexpr std::size_t SIZE = 1024 * 4097;
  auto buf = std::unique_ptr<char[]>(new char[SIZE]);
  auto err = prng.generate(buf.get(), SIZE);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);

  ASSERT_GT(prng.reseed_count(), 2);
}
