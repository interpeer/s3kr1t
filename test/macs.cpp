/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <cstring>

#include <s3kr1t/macs.h>

namespace {

static constexpr char const NONCE[] = "0123456789AB";

struct test_data {
  s3kr1t::mac_type  type;
  size_t            expected_size;
} test_macs[] = {
  { s3kr1t::MT_POLY1305, 16 },
  { s3kr1t::MT_KMAC_128, 16 },
  { s3kr1t::MT_KMAC_256, 32 },
};


std::string generate_name_value(testing::TestParamInfo<test_data> const & data)
{
  using namespace s3kr1t;

  switch (data.param.type) {
    case s3kr1t::MT_POLY1305:
      return "MT_POLY1305";
    case s3kr1t::MT_KMAC_128:
      return "MT_KMAC_128";
    case s3kr1t::MT_KMAC_256:
      return "MT_KMAC_256";

    default:
      break;
  }
  return "";
}



} // anonymous namespace


class MACs
  : public testing::TestWithParam<test_data>
{
};


TEST_P(MACs, bad_inputs)
{
  auto mtype = GetParam().type;

  std::string message = "Hello, world!";
  std::string key = "s3kr1t_012345678901234";
  char mac[512] = {};
  size_t required = 0;

  // Bad inputs for mac
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::mac(
        nullptr, sizeof(mac),
        required,
        message.c_str(), message.size(),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::mac(
        mac, 0,
        required,
        message.c_str(), message.size(),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        nullptr, message.size(),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), 0,
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), message.size(),
        nullptr, key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), message.size(),
        key.c_str(), 0,
        NONCE, std::strlen(NONCE),
        mtype));

  // Bad inputs for verify
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        nullptr, message.size(),
        mac, sizeof(mac),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), 0,
        mac, sizeof(mac),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), message.size(),
        nullptr, sizeof(mac),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), message.size(),
        mac, 0,
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), message.size(),
        mac, sizeof(mac),
        nullptr, key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), message.size(),
        mac, sizeof(mac),
        key.c_str(), 0,
        NONCE, std::strlen(NONCE),
        mtype));
}


TEST_P(MACs, small_buffer)
{
  auto mtype = GetParam().type;

  std::string message = "Hello, world!";
  std::string key = "s3kr1t_0123456789012345678901234";
  char mac[3] = {};
  size_t required = 0;

  // Create mac
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), message.size(),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_GT(required, 0);

  // Verify mac
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), message.size(),
        mac, sizeof(mac),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
}



TEST_P(MACs, match)
{
  auto mtype = GetParam().type;

  std::string message = "Hello, world!";
  std::string key = "s3kr1t_0123456789012345678901234";
  char mac[512] = {};
  size_t required = 0;

  // Create mac
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), message.size(),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_GT(required, 0);
  ASSERT_EQ(required, GetParam().expected_size);

  // Verify mac
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::check(
        message.c_str(), message.size(),
        mac, required,
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
}


TEST_P(MACs, no_match)
{
  auto mtype = GetParam().type;

  std::string message = "Hello, world!";
  std::string key = "s3kr1t_0123456789012345678901234";
  char mac[512] = {};
  size_t required = 0;

  // Create mac
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), message.size(),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_GT(required, 0);

  // Verify mac - but over part of the message only!
  ASSERT_EQ(s3kr1t::ERR_VERIFY,
      s3kr1t::check(
        message.c_str(), message.size() - 3,
        mac, required,
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
}


TEST_P(MACs, no_match_password)
{
  auto mtype = GetParam().type;

  std::string message = "Hello, world!";
  std::string key0 = "s3kr1t_0123456789012345678901234";
  std::string key1 = "sw0rdf1sh_0123456789012345678901";
  char mac[512] = {};
  size_t required = 0;

  // Create mac
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), message.size(),
        key0.c_str(), key0.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_GT(required, 0);

  // Verify mac - but over part of the message only!
  ASSERT_EQ(s3kr1t::ERR_VERIFY,
      s3kr1t::check(
        message.c_str(), message.size() - 3,
        mac, required,
        key1.c_str(), key1.size(),
        NONCE, std::strlen(NONCE),
        mtype));
}


TEST_P(MACs, no_match_nonce)
{
  auto mtype = GetParam().type;

  std::string message = "Hello, world!";
  std::string key = "s3kr1t_0123456789012345678901234";
  std::string bad_nonce = "AB0123456789";
  char mac[512] = {};
  size_t required = 0;

  // Create mac
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), message.size(),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_GT(required, 0);

  // Verify mac - but over part of the message only!
  ASSERT_EQ(s3kr1t::ERR_VERIFY,
      s3kr1t::check(
        message.c_str(), message.size() - 3,
        mac, required,
        key.c_str(), key.size(),
        bad_nonce.c_str(), bad_nonce.size(),
        mtype));
}



INSTANTIATE_TEST_SUITE_P(s3kr1t, MACs,
    testing::ValuesIn(test_macs),
    generate_name_value);



namespace {

inline void
test_kmac_no_nonce(s3kr1t::mac_type mtype)
{
  std::string message = "Hello, world!";
  std::string key = "s3kr1t_0123456789012345678901234";
  char mac[512] = {};
  size_t required = 0;

  // Create mac
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), message.size(),
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
  ASSERT_GT(required, 0);

  // Verify mac
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::check(
        message.c_str(), message.size(),
        mac, required,
        key.c_str(), key.size(),
        NONCE, std::strlen(NONCE),
        mtype));
}


} // anonymous namespace

TEST(MACs, KMAC_128_no_nonce)
{
  test_kmac_no_nonce(s3kr1t::MT_KMAC_128);
}

TEST(MACs, KMAC_256_no_nonce)
{
  test_kmac_no_nonce(s3kr1t::MT_KMAC_256);
}


namespace {

inline void
test_poly_nonce(char const * nonce, size_t nonce_size)
{
  std::string message = "Hello, world!";
  std::string key = "s3kr1t_0123456789012345678901234";
  char mac[512] = {};
  size_t required = 0;

  // Create mac fails
  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE,
      s3kr1t::mac(
        mac, sizeof(mac),
        required,
        message.c_str(), message.size(),
        key.c_str(), key.size(),
        nonce, nonce_size,
        s3kr1t::MT_POLY1305));
}


}

TEST(MACs, Poly1305_bad_nonce)
{
  // Too short
  test_poly_nonce("foo", 123);
}


TEST(MACs, Poly1305_no_nonce)
{
  // Absent
  test_poly_nonce(nullptr, 12);
  test_poly_nonce(NONCE, 0);
}
