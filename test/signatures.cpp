/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <s3kr1t/signatures.h>

#include <liberate/string/hexencode.h>

#include "test_keys.h"

namespace {

inline void
test_sign_verify(s3kr1t::key const & privkey, s3kr1t::key const & pubkey,
    bool expect_match, s3kr1t::digest_type dtype)
{
  std::string data{"Hello, world!"};
  size_t required = 0;
  char sigbuf[1024] = {};

  // Test without inputs
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::sign(nullptr, sizeof(sigbuf), required,
        data.c_str(), data.size(), privkey, dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::sign(sigbuf, 0, required,
        data.c_str(), data.size(), privkey, dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::sign(sigbuf, sizeof(sigbuf), required,
        nullptr, data.size(), privkey, dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::sign(sigbuf, sizeof(sigbuf), required,
        data.c_str(), 0, privkey, dtype));

  // Attempting to sign with a public key should yield an error.
  ASSERT_EQ(s3kr1t::ERR_SIGN,
      s3kr1t::sign(sigbuf, sizeof(sigbuf), required,
        data.c_str(), data.size(), pubkey, dtype));

  // Sign well, but with a too-small signature buffer (single byte)
  required = 0;
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::sign(sigbuf, 1, required,
        data.c_str(), data.size(), privkey, dtype));
  ASSERT_GT(required, 0);

  // Sign well
  required = 0;
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::sign(sigbuf, sizeof(sigbuf), required,
        data.c_str(), data.size(), privkey, dtype));
  ASSERT_GT(required, 0);

  // Verify without inputs
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::verify(nullptr, data.size(),
        sigbuf, required, pubkey, dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::verify(data.c_str(), 0,
        sigbuf, required, pubkey, dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::verify(data.c_str(), data.size(),
        nullptr, required, pubkey, dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::verify(data.c_str(), data.size(),
        sigbuf, 0, pubkey, dtype));

  // Verify with bad key
  ASSERT_EQ(s3kr1t::ERR_VERIFY,
      s3kr1t::verify(data.c_str(), data.size(),
        sigbuf, sizeof(sigbuf), privkey, dtype));

  // Verify well - here it depends on whether we expect a match or do not.
  auto expected_code = expect_match ? s3kr1t::ERR_SUCCESS : s3kr1t::ERR_VERIFY;
  ASSERT_EQ(expected_code,
      s3kr1t::verify(data.c_str(), data.size(),
        sigbuf, required, pubkey, dtype));
}

inline void
test_sign_verify_none(s3kr1t::key const & privkey, s3kr1t::key const & pubkey,
    bool expect_match)
{
  test_sign_verify(privkey, pubkey, expect_match, s3kr1t::DT_NONE);
}

inline void
test_sign_verify_sha2(s3kr1t::key const & privkey, s3kr1t::key const & pubkey,
    bool expect_match)
{
  test_sign_verify(privkey, pubkey, expect_match, s3kr1t::DT_SHA2_224);
  test_sign_verify(privkey, pubkey, expect_match, s3kr1t::DT_SHA2_256);
  test_sign_verify(privkey, pubkey, expect_match, s3kr1t::DT_SHA2_384);
  test_sign_verify(privkey, pubkey, expect_match, s3kr1t::DT_SHA2_512);
}

inline void
test_sign_verify_sha3(s3kr1t::key const & privkey, s3kr1t::key const & pubkey,
    bool expect_match)
{
  test_sign_verify(privkey, pubkey, expect_match, s3kr1t::DT_SHA3_224);
  test_sign_verify(privkey, pubkey, expect_match, s3kr1t::DT_SHA3_256);
  test_sign_verify(privkey, pubkey, expect_match, s3kr1t::DT_SHA3_384);
  test_sign_verify(privkey, pubkey, expect_match, s3kr1t::DT_SHA3_512);
}

} // anonymous namespace

TEST(Signatures, ecdsa_pair)
{
  {
    auto pair = s3kr1t::key::read_keys(test_keys::ecdsa_private_key_p256);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *pair.pubkey, true);
  }

  {
    auto pair = s3kr1t::key::read_keys(test_keys::ecdsa_private_key_p384);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *pair.pubkey, true);
  }

  {
    auto pair = s3kr1t::key::read_keys(test_keys::ecdsa_private_key_p521);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *pair.pubkey, true);
  }
}


TEST(Signatures, ecdsa_unmatched)
{
  auto other_privkey = s3kr1t::key::generate(s3kr1t::KT_EC, "secp256k1");
  auto other_pair = other_privkey.create_pair();
  ASSERT_TRUE(other_pair.pubkey);
  ASSERT_TRUE(other_pair.privkey);

  {
    auto pair = s3kr1t::key::read_keys(test_keys::ecdsa_private_key_p256);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *other_pair.pubkey, false);
    test_sign_verify_sha2(*other_pair.privkey, *pair.pubkey, false);
  }

  {
    auto pair = s3kr1t::key::read_keys(test_keys::ecdsa_private_key_p384);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *other_pair.pubkey, false);
    test_sign_verify_sha2(*other_pair.privkey, *pair.pubkey, false);
  }

  {
    auto pair = s3kr1t::key::read_keys(test_keys::ecdsa_private_key_p521);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *other_pair.pubkey, false);
    test_sign_verify_sha2(*other_pair.privkey, *pair.pubkey, false);
  }
}


TEST(Signatures, rsa_pair)
{
  {
    auto pair = s3kr1t::key::read_keys(test_keys::rsa_encrypted_private_key,
        test_keys::rsa_encrypted_key_password);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *pair.pubkey, true);
    test_sign_verify_sha3(*pair.privkey, *pair.pubkey, true);
  }

  {
    auto pair = s3kr1t::key::read_keys(test_keys::rsa_other_private_key);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *pair.pubkey, true);
    test_sign_verify_sha3(*pair.privkey, *pair.pubkey, true);
  }
}


TEST(Signatures, rsa_unmatched)
{
  auto other_privkey = s3kr1t::key::generate(s3kr1t::KT_RSA, "1024");
  auto other_pair = other_privkey.create_pair();
  ASSERT_TRUE(other_pair.pubkey);
  ASSERT_TRUE(other_pair.privkey);

  {
    auto pair = s3kr1t::key::read_keys(test_keys::rsa_encrypted_private_key,
        test_keys::rsa_encrypted_key_password);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *other_pair.pubkey, false);
    test_sign_verify_sha2(*other_pair.privkey, *pair.pubkey, false);

    test_sign_verify_sha3(*pair.privkey, *other_pair.pubkey, false);
    test_sign_verify_sha3(*other_pair.privkey, *pair.pubkey, false);
  }

  {
    auto pair = s3kr1t::key::read_keys(test_keys::rsa_other_private_key);
    ASSERT_TRUE(pair.pubkey);
    ASSERT_TRUE(pair.privkey);

    test_sign_verify_sha2(*pair.privkey, *other_pair.pubkey, false);
    test_sign_verify_sha2(*other_pair.privkey, *pair.pubkey, false);

    test_sign_verify_sha3(*pair.privkey, *other_pair.pubkey, false);
    test_sign_verify_sha3(*other_pair.privkey, *pair.pubkey, false);
  }
}


TEST(Signatures, dsa_pair)
{
  auto pair = s3kr1t::key::read_keys(test_keys::dsa_private_key);
  ASSERT_TRUE(pair.pubkey);
  ASSERT_TRUE(pair.privkey);

  test_sign_verify_sha2(*pair.privkey, *pair.pubkey, true);
}


TEST(Signatures, dsa_unmatched)
{
  auto other_privkey = s3kr1t::key::generate(s3kr1t::KT_DSA, "1024");
  auto other_pair = other_privkey.create_pair();
  ASSERT_TRUE(other_pair.pubkey);
  ASSERT_TRUE(other_pair.privkey);

  auto pair = s3kr1t::key::read_keys(test_keys::dsa_private_key);
  ASSERT_TRUE(pair.pubkey);
  ASSERT_TRUE(pair.privkey);

  test_sign_verify_sha2(*pair.privkey, *other_pair.pubkey, false);
  test_sign_verify_sha2(*other_pair.privkey, *pair.pubkey, false);
}


TEST(Signatures, ed_pair)
{
  auto pair = s3kr1t::key::read_keys(test_keys::ed25519_private_key);
  ASSERT_TRUE(pair.pubkey);
  ASSERT_TRUE(pair.privkey);

  test_sign_verify_none(*pair.privkey, *pair.pubkey, true);
}


TEST(Signatures, ed_unmatched)
{
  auto other_privkey = s3kr1t::key::generate(s3kr1t::KT_ED, "ed25519");
  auto other_pair = other_privkey.create_pair();
  ASSERT_TRUE(other_pair.pubkey);
  ASSERT_TRUE(other_pair.privkey);

  auto pair = s3kr1t::key::read_keys(test_keys::ed25519_private_key);
  ASSERT_TRUE(pair.pubkey);
  ASSERT_TRUE(pair.privkey);

  test_sign_verify_none(*pair.privkey, *other_pair.pubkey, false);
  test_sign_verify_none(*other_pair.privkey, *pair.pubkey, false);
}
