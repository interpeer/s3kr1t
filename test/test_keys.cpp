/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "test_keys.h"

namespace test_keys
{
  std::string google_cert_root2 = R"(-----BEGIN CERTIFICATE-----
MIIFVzCCAz+gAwIBAgINAgPlrsWNBCUaqxElqjANBgkqhkiG9w0BAQwFADBHMQsw
CQYDVQQGEwJVUzEiMCAGA1UEChMZR29vZ2xlIFRydXN0IFNlcnZpY2VzIExMQzEU
MBIGA1UEAxMLR1RTIFJvb3QgUjIwHhcNMTYwNjIyMDAwMDAwWhcNMzYwNjIyMDAw
MDAwWjBHMQswCQYDVQQGEwJVUzEiMCAGA1UEChMZR29vZ2xlIFRydXN0IFNlcnZp
Y2VzIExMQzEUMBIGA1UEAxMLR1RTIFJvb3QgUjIwggIiMA0GCSqGSIb3DQEBAQUA
A4ICDwAwggIKAoICAQDO3v2m++zsFDQ8BwZabFn3GTXd98GdVarTzTukk3LvCvpt
nfbwhYBboUhSnznFt+4orO/LdmgUud+tAWyZH8QiHZ/+cnfgLFuv5AS/T3KgGjSY
6Dlo7JUle3ah5mm5hRm9iYz+re026nO8/4Piy33B0s5Ks40FnotJk9/BW9BuXvAu
MC6C/Pq8tBcKSOWIm8Wba96wyrQD8Nr0kLhlZPdcTK3ofmZemde4wj7I0BOdre7k
RXuJVfeKH2JShBKzwkCX44ofR5GmdFrS+LFjKBC4swm4VndAoiaYecb+3yXuPuWg
f9RhD1FLPD+M2uFwdNjCaKH5wQzpoeJ/u1U8dgbuak7MkogwTZq9TwtImoS1mKPV
+3PBV2HdKFZ1E66HjucMUQkQdYhMvI35ezzUIkgfKtzra7tEscszcTJGr61K8Yzo
dDqs5xoic4DSMPclQsciOzsSrZYuxsN2B6ogtzVJV+mSSeh2FnIxZyuWfoqjx5RW
Ir9qS34BIbIjMt/kmkRtWVtd9QCgHJvGeJeNkP+byKq0rxFROV7Z+2et1VsRnTKa
G73VululycslaVNVJ1zgyjbLiGH7HrfQy+4W+9OmTN6SpdTi3/UGVN4unUu0kzCq
gc7dGtxRcw1PcOnlthYhGXmy5okLdWTK1au8CcEYof/UVKGFPP0UJAOyh9OktwID
AQABo0IwQDAOBgNVHQ8BAf8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4E
FgQUu//KjiOfT5nK2+JopqUVJxce2Q4wDQYJKoZIhvcNAQEMBQADggIBAB/Kzt3H
vqGf2SdMC9wXmBFqiN495nFWcrKeGk6c1SuYJF2ba3uwM4IJvd8lRuqYnrYb/oM8
0mJhwQTtzuDFycgTE1XnqGOtjHsB/ncw4c5omwX4Eu55MaBBRTUoCnGkJE+M3DyC
B19m3H0Q/gxhswWV7uGugQ+o+MePTagjAiZrHYNSVc61LwDKgEDg4XSsYPWHgJ2u
NmSRXbBoGOqKYcl3qJfEycel/FVL8/B/uWU9J2jQzGv6U53hkRrJXRqWbTKH7QMg
yALOWr7Z6v2yTcQvG99fevX4i8buMTolUVVnjWQye+mew4K6Ki3pHrTgSAai/Gev
HyICc/sgCq+dVEuhzf9gR7A/Xe8bVr2XIZYtCtFenTgCR2y59PYjJbigapordwj6
xLEokCZYCDzifqrXPW+6MYgKBesntaFJ7qBFVHvmJ2WZICGoo7z7GJa7Um8M7YNR
TOlZ4iBgxcJlkoKM8xAfDoqXvneCbT+PHV28SSe9zE8P4c52hgQjxcCMElv924Sg
JPFI/2R80L5cFtHvma3AH/vLrrw4IgYmZNralw4/KBVEqE8AyvCazM90arQ+POuV
7LXTWtiBmelDGDfrs7vRWGJB82bSj6p4lVQgw1oudCvV0b4YacCs1aTPObpRhANl
6WLAYv7YTVWW4tAR+kg0Eeye7QUd5MjWHYbL
-----END CERTIFICATE-----
)";

  std::string google_cert_root4 = {
  "\x30\x82\x02\x09\x30\x82\x01\x8e\xa0\x03\x02\x01"
  "\x02\x02\x0d\x02\x03\xe5\xc0\x68\xef\x63\x1a\x9c"
  "\x72\x90\x50\x52\x30\x0a\x06\x08\x2a\x86\x48\xce"
  "\x3d\x04\x03\x03\x30\x47\x31\x0b\x30\x09\x06\x03"
  "\x55\x04\x06\x13\x02\x55\x53\x31\x22\x30\x20\x06"
  "\x03\x55\x04\x0a\x13\x19\x47\x6f\x6f\x67\x6c\x65"
  "\x20\x54\x72\x75\x73\x74\x20\x53\x65\x72\x76\x69"
  "\x63\x65\x73\x20\x4c\x4c\x43\x31\x14\x30\x12\x06"
  "\x03\x55\x04\x03\x13\x0b\x47\x54\x53\x20\x52\x6f"
  "\x6f\x74\x20\x52\x34\x30\x1e\x17\x0d\x31\x36\x30"
  "\x36\x32\x32\x30\x30\x30\x30\x30\x30\x5a\x17\x0d"
  "\x33\x36\x30\x36\x32\x32\x30\x30\x30\x30\x30\x30"
  "\x5a\x30\x47\x31\x0b\x30\x09\x06\x03\x55\x04\x06"
  "\x13\x02\x55\x53\x31\x22\x30\x20\x06\x03\x55\x04"
  "\x0a\x13\x19\x47\x6f\x6f\x67\x6c\x65\x20\x54\x72"
  "\x75\x73\x74\x20\x53\x65\x72\x76\x69\x63\x65\x73"
  "\x20\x4c\x4c\x43\x31\x14\x30\x12\x06\x03\x55\x04"
  "\x03\x13\x0b\x47\x54\x53\x20\x52\x6f\x6f\x74\x20"
  "\x52\x34\x30\x76\x30\x10\x06\x07\x2a\x86\x48\xce"
  "\x3d\x02\x01\x06\x05\x2b\x81\x04\x00\x22\x03\x62"
  "\x00\x04\xf3\x74\x73\xa7\x68\x8b\x60\xae\x43\xb8"
  "\x35\xc5\x81\x30\x7b\x4b\x49\x9d\xfb\xc1\x61\xce"
  "\xe6\xde\x46\xbd\x6b\xd5\x61\x18\x35\xae\x40\xdd"
  "\x73\xf7\x89\x91\x30\x5a\xeb\x3c\xee\x85\x7c\xa2"
  "\x40\x76\x3b\xa9\xc6\xb8\x47\xd8\x2a\xe7\x92\x91"
  "\x6a\x73\xe9\xb1\x72\x39\x9f\x29\x9f\xa2\x98\xd3"
  "\x5f\x5e\x58\x86\x65\x0f\xa1\x84\x65\x06\xd1\xdc"
  "\x8b\xc9\xc7\x73\xc8\x8c\x6a\x2f\xe5\xc4\xab\xd1"
  "\x1d\x8a\xa3\x42\x30\x40\x30\x0e\x06\x03\x55\x1d"
  "\x0f\x01\x01\xff\x04\x04\x03\x02\x01\x86\x30\x0f"
  "\x06\x03\x55\x1d\x13\x01\x01\xff\x04\x05\x30\x03"
  "\x01\x01\xff\x30\x1d\x06\x03\x55\x1d\x0e\x04\x16"
  "\x04\x14\x80\x4c\xd6\xeb\x74\xff\x49\x36\xa3\xd5"
  "\xd8\xfc\xb5\x3e\xc5\x6a\xf0\x94\x1d\x8c\x30\x0a"
  "\x06\x08\x2a\x86\x48\xce\x3d\x04\x03\x03\x03\x69"
  "\x00\x30\x66\x02\x31\x00\xe8\x40\xff\x83\xde\x03"
  "\xf4\x9f\xae\x1d\x7a\xa7\x2e\xb9\xaf\x4f\xf6\x83"
  "\x1d\x0e\x2d\x85\x01\x1d\xd1\xd9\x6a\xec\x0f\xc2"
  "\xaf\xc7\x5e\x56\x5e\x5c\xd5\x1c\x58\x22\x28\x0b"
  "\xf7\x30\xb6\x2f\xb1\x7c\x02\x31\x00\xf0\x61\x3c"
  "\xa7\xf4\xa0\x82\xe3\x21\xd5\x84\x1d\x73\x86\x9c"
  "\x2d\xaf\xca\x34\x9b\xf1\x9f\xb9\x23\x36\xe2\xbc"
  "\x60\x03\x9d\x80\xb3\x9a\x56\xc8\xe1\xe2\xbb\x14"
  "\x79\xca\xcd\x21\xd4\x94\xb5\x49\x43", 525};
  std::string google_cert_key = R"()";

  std::string ecdsa_private_key_p256 = R"(-----BEGIN EC PARAMETERS-----
BgUrgQQACg==
-----END EC PARAMETERS-----
-----BEGIN EC PRIVATE KEY-----
MHQCAQEEIBEtwYnInMNkb16DD3Zcn7KK7XUt8UHOdnhIEPesIt3poAcGBSuBBAAK
oUQDQgAEPyNi/CAq2pABlfd32fwBR0nrisvBLNUTLrIunrD4ad1s4kHesPAUPqSX
T+tkFc1Ar+T4s8dJbtt5yXQhHT20Uw==
-----END EC PRIVATE KEY-----
  )";
  std::string ecdsa_public_key_p256 = R"(-----BEGIN PUBLIC KEY-----
MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEPyNi/CAq2pABlfd32fwBR0nrisvBLNUT
LrIunrD4ad1s4kHesPAUPqSXT+tkFc1Ar+T4s8dJbtt5yXQhHT20Uw==
-----END PUBLIC KEY-----
  )";

  std::string ecdsa_private_key_p384 = R"(-----BEGIN EC PARAMETERS-----
BgUrgQQAIg==
-----END EC PARAMETERS-----
-----BEGIN EC PRIVATE KEY-----
MIGkAgEBBDC/aTd6/5/zO6F7UvNcXrwUELrRywp2HLRPlmNRWRcvcpwMqjv4eBoN
47GeC1AcLyKgBwYFK4EEACKhZANiAATjp9jhOJef0qJ6i4gOAlBav1F3REaH3q6i
h/Tqzw5E1zrPIEuADr+pdItGd5BxqP20g4pII0XcJ9iar3NH1fwDzSbAiH9ZHWHu
NTe0ekJAuHV1Ug85Ut4R0tc4lAzLcO4=
-----END EC PRIVATE KEY-----
  )";
  std::string ecdsa_public_key_p384 = R"(-----BEGIN PUBLIC KEY-----
MHYwEAYHKoZIzj0CAQYFK4EEACIDYgAE46fY4TiXn9KieouIDgJQWr9Rd0RGh96u
oof06s8ORNc6zyBLgA6/qXSLRneQcaj9tIOKSCNF3CfYmq9zR9X8A80mwIh/WR1h
7jU3tHpCQLh1dVIPOVLeEdLXOJQMy3Du
-----END PUBLIC KEY-----
  )";

  std::string ecdsa_private_key_p521 = R"(-----BEGIN EC PARAMETERS-----
BgUrgQQAIw==
-----END EC PARAMETERS-----
-----BEGIN EC PRIVATE KEY-----
MIHcAgEBBEIBHkq74lzJUtamawmNHjbKmxAvZ1VEjDawgDlrbmXkfXbrBCzhvAH0
I7WWxLDFc1zy0u5LjQ79OeTlSqLaaHWZfuWgBwYFK4EEACOhgYkDgYYABABg252K
f4VOXzOfq/KFYWh22+PurDvZUsCVIVMYT0WYVNXhRwTNrYswOYRvRUhFsJda6YFg
xpbYH9ROXMxxkdNMtgHrFnxoawj7mfaKmkyJafwaRlfcFeaBNW/fRBFZpQY3yfW+
1zhjh1YAM/NfuAyP4jAoAViLEJtuzouxdPvpKONRpg==
-----END EC PRIVATE KEY-----
  )";
  std::string ecdsa_public_key_p521 = R"(-----BEGIN PUBLIC KEY-----
MIGbMBAGByqGSM49AgEGBSuBBAAjA4GGAAQAYNudin+FTl8zn6vyhWFodtvj7qw7
2VLAlSFTGE9FmFTV4UcEza2LMDmEb0VIRbCXWumBYMaW2B/UTlzMcZHTTLYB6xZ8
aGsI+5n2ippMiWn8GkZX3BXmgTVv30QRWaUGN8n1vtc4Y4dWADPzX7gMj+IwKAFY
ixCbbs6LsXT76SjjUaY=
-----END PUBLIC KEY-----
  )";

  // RSA

  std::string rsa_encrypted_private_key = R"(-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-256-CBC,1955605DDF454914DB35B93F2F1167B2

n5fpbiA8+JJLiFKVyo6eSzrlV1Jcd+ZUET09Q0Gig2jWX1xar7+H+prZRKHV0Hc4
O6Me+wIpsegQUyjXlZV1hXFL+TEYn5aN160TwQF1QrMV6zMGSnB468B313VvOz4E
Tqcv7qM1z/D/FR19pXvWwJZsQvFaM0CkhbidrWtEN5fgQJaAk9RZPbzwf34N3Kcp
k/ghU5v2x8UQiml5BLMMdSYgO31EhiCPQZdeFSUbjMIXBAS0CLdRqRFgJWJQMuoE
JD0Or1O5RmS8NVaUGDjg5MK61EkDBaq4guhnLUnzvPbdNrZ5RJ/MC1VBekcj2Xh3
Xtwk6g39OKVmQuCti6lqN28Rj5Zqzp8rJ3NbSBgE8k1/BuFAzPpFY4VpxduFrm7q
LgE+w+wkwz9RtT6UdgOu19EGfefSfJmqQP3t7m7jKajx/+uf94RVikDzJvu19Mz9
NqwdOlReu5zyfwy/QhiytDMLh34e+ji777dEXZcA4yI+/B/Fk72coYikbwtLXuSI
rcejVz9yu2yQw2EZQd/dtnv6v6luLjF/kQ1cbaA2OYPQNrvQUvJ+Ka/9bXOjRXLC
tsr3w4e3TYIF7HrpcO+9tpBmP1a3qwli0GL1zBeL2xxoD5NxgIR/vEdmRdw1tAVI
fL8sEOHOdHfApu/LHVh7BPZJo8B8i5AjNWctLlPDt+Jz8bCXPlCoZkbOYAkZaz5H
SAjV0GH8jUfDSKeveUXYrQxl8ppWQ3uAXpMfTCq+5/qKSi4vEe7KgrpwsI4Wnj/7
3sKHVm+37tKEO8TmZxgDOhNYW7Mq9oNWOGAwrxlMSrmpvl0IEOrHs+TLIi2FRc1d
LK+MQxfBILEOV9u8puZHdQK8V2nRufirz4nuUzyrWiKolylDdi7edPDBeC95E9mC
Xn1zaPOkp5n52t77vi6CwdbgzlUQcWaSyBl0FIKORlsl0MZOAAU2DKRYd3ixTxmp
d0kDVwVMWavzDnpZU3Qy06izBPulc6ogLyuTkUn4MNzQLvRPXclFPhRzeQg8eP00
Pmey+3XBoLQa0gbzdCF7Jtp6wS+90oBl6NImCpJ/4X16tm/iYOOejY95RRO0UJrw
V9yqjXQrqngq7Im69+N5iR/N4q+k6xB0PTIb8p8mE/2APmiJgUolQhrXZGsw8hCV
Wt0IGAtacR8j3mHxu6Lhi71Lq4SQNwYm3hS2lvqfM75zAwBmaS9pcjO45E/u5KTu
TkVpu/wkBJ9IMI4q3/D93LxqZ0/QDx19YMTEbl+ap5hOHeRAMfZyR8xUghsxiogT
mNwjxkrSu6N4tR1e0911BJqjFY3x7oVFrgKzuGIn6lV/c11UXyvrnBv3kUkcTn6Q
hSTPudDDfY0TRub/oDcv8n8HKjNBGk65beAJPRttTYGrb2JNqDGHdPoz/2zaDjId
yg0QM7a0CwfopbyuDLLJ3OfLFzfXfMllwB0AMg6d8xTUOyoEL/+X+aZtfPUtgAA9
NEPXeno5TppmoMXf/cF8hHagdasgHzoLhUZDnyBy1SJbHjCM+pn91oBA0v4UinVj
9netamQDdqUQkEu0/Y4AiSrj+ajFdEJMitZwyOD1Ame/Pg9lVXmayhNN7iTU6lEA
-----END RSA PRIVATE KEY-----
  )";
  std::string rsa_encrypted_key_password = "s3kr1t";

  std::string rsa_public_key = R"(-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0UFT/t2s3rm1XzvCftCw
C1GHmz7kKpEggplFlIn1vis8kOQWM9KAYLuphJlQ8JCffEy9pjsSCeGasmgOiXwx
Ter6LY755bx3MbTMBmj6wMKoMl7JRoeILsMSG5E4pnNHPb8WFkfKwCaPt9ghXgcF
/aC3KRtMG+Ix1L2JQbAZp/aOY9EtAvE+Wb5Y4wuJOfFPSkAZCwxLi8gytFNpej6Y
XkFNXxmcxVpOUTyHIPQy95uHBsv0nU+UKLE66dx7uJj6a8V7ewPsS2H9PPdZ+Y5y
nc08nPapOSOCQC/c0ee3q+4cNUv02Q6UFmKWKLaD8uPGA9ZXOTsqXfOs3iOe/cqC
UwIDAQAB
-----END PUBLIC KEY-----
  )";

  std::string rsa_other_private_key = R"(-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQDAWil7GFIdATA8Z1sUfq8GuW22Z729Me002/KhToyvmSSzkrwj
puOboXBZqO82874t/NVNZLqdnv5W/JIPv6N0Js1kfwJA7cvKlT1gvwVlQHOmMGke
hxjCP5+MJVTRwIqohEbxmwaq51BfCBz0mGgbOU+ZkFWes4nLdcTDmdb+9wIDAQAB
AoGBAIhraZ/zz8jiXuN4KhCPCHMsb7M+QEM+dY1XsOzWWNDxPCZfmy9t3V8zUAtG
1OGUXP0go+cP2YqQiLIIz058C9/Ja+rIrQyPMmsKuxYVmLxH1yeIxJWVlwJ57HW3
iiSGC7rcuAMlzk0hxn1FA4EQrUaDnqvmu4HRikNeuFcUEpWBAkEA7VJdl42Kh6Kc
QPjWWB6NcIO2scNejr4ojleX0nDmo8Ntri0LB5DXM2vESrjMnMSxJNlGRmF5TZ3Y
6DIdsmB77QJBAM99vFNz4/rU4Mu64vZ2TGWkEHQYDkGaiK3wL1hEQnEzFqizluyL
ql9GKbOa74+E+NX0K0A4IXLlwHL7qOuNMfMCQEEZG9UrnTlRcJ0d5OXtE40uhBnE
5pYvMT7CZkHyeEGppNdnQaREpdNUF1cL1AyYo6Lg+rbpm/Wfe9VNkfnRH40CQDYZ
0sDvtsHwP6zfTc51ifX2kVOz8gOpB3pS5gVdxvU9HOtf6DpFMGcnJDhwbYY+ZaNj
xuM3v4D15vgUJjdE2YECQQC8aXNMGV/lFKWqq1MEaqqnEP2g/7SwelgJay+1VGkr
j9qaEUyQRgyefMJssCuL5w4Vf1tJYwHb02vDCgtXSZmS
-----END RSA PRIVATE KEY-----
  )";

  std::string rsa_other_public_key = R"(-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDAWil7GFIdATA8Z1sUfq8GuW22
Z729Me002/KhToyvmSSzkrwjpuOboXBZqO82874t/NVNZLqdnv5W/JIPv6N0Js1k
fwJA7cvKlT1gvwVlQHOmMGkehxjCP5+MJVTRwIqohEbxmwaq51BfCBz0mGgbOU+Z
kFWes4nLdcTDmdb+9wIDAQAB
-----END PUBLIC KEY-----
  )";

  // DSA

  std::string dsa_private_key = R"(-----BEGIN DSA PRIVATE KEY-----
MIIDVQIBAAKCAQEAkUcN6zJXqDnbsimDKeab/Fq3oKDy45PaPn9RBoMrb1q8/ZVW
4pzFa/5utIc/53lH8f1VmmmInxy44DIcWkaTpyuZgDxGwGMFSGXshQjY1sRqvXsy
n5jW4o9hbddcaYI//tVmQN2V8V5dBfhWF/pRavj6LMvKjsbiks9yPhoR0QhPVx/l
lwbjOnGZdxRgxRywa/Zx7Vr5UMixi8e6VPFXl3UDZ2JaMYOl+2jjBTYoyRSfdXdz
a+qMi9/MVDQymYXCjWl8vkWDLDqfTPkzGs0Apm4kbLb/E4UjTouncgcQdK8HlMu0
yp2NPb8Y2VkBc6l7vsFHu3lkQHJfblWbnQKGdwIhANUonK1CH7DQfmu/lnUjSWWE
+ei2bn8QoRzF0/Og1AzvAoIBAGcOi1/Lsu6KDPTswP0/7mqehbalIL6V61pINs22
2ia3RrrZ1CI8yy/Uzu6zGkrw8sKHLiNl0G47E4o1fxtCKUizwbA+InukOCXi+eIm
akYqznLWhA+ZR+6WFTZ5U8sOMVrMoKPc2hbWHYsTV8D1zmqp3JJpJO35y29HW2me
iiCooigklZ4sSrficXfR9yN4t4ibGrh3Dixm6BkA0q6w7XDYRcqQYwV+M88QM91x
wPC/tZMnUipCl+K89yuOmTjzrW0xkK0TOeLF3G6rmyyaRRXKxoOOW5eUuq5TkuJA
GN7dm/Ike1BBDMuZyzRNw04vkPs6gSgOtqgeEpdNxD7CatQCggEAaAEFJy61qcWV
se1z4XT0Z0TLKWf0XBi1g1V7Y8PBOumZQesDcQSygbHfPzUeb2nKLpk7SoFoD6VX
vpz0AeM6FRhdv7uXQyw2J5bX6uASuLYG9cxm0A+7xXOMYFeZVrr42ACntaRqkbXD
+Mqky4KeyjF2kBQrqlMkYWfAz00W6TMH2jdDeswZIbwb70mLdNl/4+EFDGcl7QYp
jjslD5aA9EJo6KX7r6pCxkbtRoODWvLTTAttkYvoAZ5SuppAZ5T+DcYtg4MKA5xU
Xlqr60p6qMxxzd/E5L6eHbsNuV2KIqHU6bykxk0mVaikFjiJ6dj9D0I0ZsxQGdAi
l2wJ7Hb/yQIgKIReF6iWEwo2sntPcAgWh3OuEfzewRAqvC5jdCFhqFI=
-----END DSA PRIVATE KEY-----
  )";
  std::string dsa_public_key = R"(-----BEGIN PUBLIC KEY-----
MIIDRjCCAjkGByqGSM44BAEwggIsAoIBAQCRRw3rMleoOduyKYMp5pv8WregoPLj
k9o+f1EGgytvWrz9lVbinMVr/m60hz/neUfx/VWaaYifHLjgMhxaRpOnK5mAPEbA
YwVIZeyFCNjWxGq9ezKfmNbij2Ft11xpgj/+1WZA3ZXxXl0F+FYX+lFq+Posy8qO
xuKSz3I+GhHRCE9XH+WXBuM6cZl3FGDFHLBr9nHtWvlQyLGLx7pU8VeXdQNnYlox
g6X7aOMFNijJFJ91d3Nr6oyL38xUNDKZhcKNaXy+RYMsOp9M+TMazQCmbiRstv8T
hSNOi6dyBxB0rweUy7TKnY09vxjZWQFzqXu+wUe7eWRAcl9uVZudAoZ3AiEA1Sic
rUIfsNB+a7+WdSNJZYT56LZufxChHMXT86DUDO8CggEAZw6LX8uy7ooM9OzA/T/u
ap6FtqUgvpXrWkg2zbbaJrdGutnUIjzLL9TO7rMaSvDywocuI2XQbjsTijV/G0Ip
SLPBsD4ie6Q4JeL54iZqRirOctaED5lH7pYVNnlTyw4xWsygo9zaFtYdixNXwPXO
aqnckmkk7fnLb0dbaZ6KIKiiKCSVnixKt+Jxd9H3I3i3iJsauHcOLGboGQDSrrDt
cNhFypBjBX4zzxAz3XHA8L+1kydSKkKX4rz3K46ZOPOtbTGQrRM54sXcbqubLJpF
FcrGg45bl5S6rlOS4kAY3t2b8iR7UEEMy5nLNE3DTi+Q+zqBKA62qB4Sl03EPsJq
1AOCAQUAAoIBAGgBBScutanFlbHtc+F09GdEyyln9FwYtYNVe2PDwTrpmUHrA3EE
soGx3z81Hm9pyi6ZO0qBaA+lV76c9AHjOhUYXb+7l0MsNieW1+rgEri2BvXMZtAP
u8VzjGBXmVa6+NgAp7WkapG1w/jKpMuCnsoxdpAUK6pTJGFnwM9NFukzB9o3Q3rM
GSG8G+9Ji3TZf+PhBQxnJe0GKY47JQ+WgPRCaOil+6+qQsZG7UaDg1ry00wLbZGL
6AGeUrqaQGeU/g3GLYODCgOcVF5aq+tKeqjMcc3fxOS+nh27DbldiiKh1Om8pMZN
JlWopBY4ienY/Q9CNGbMUBnQIpdsCex2/8k=
-----END PUBLIC KEY-----
  )";

  // Edwards

  std::string ed25519_private_key = R"(-----BEGIN PRIVATE KEY-----
MC4CAQAwBQYDK2VwBCIEIE6V3BprgqVmv18GD75hBkxAUMLZM8KTy7JntEA1abN6
-----END PRIVATE KEY-----
  )";

  std::string ed25519_public_key = R"(-----BEGIN PUBLIC KEY-----
MCowBQYDK2VwAyEA/LHBym63DYPoVAUWl1QWJWprDP7QWPDZEdUPVsb2ynw=
-----END PUBLIC KEY-----
  )";

  std::string ed448_private_key = R"(-----BEGIN PRIVATE KEY-----
MEcCAQAwBQYDK2VxBDsEOVw5ou+rZPP6x4h4cp04/e3qqvaV2rnUJPXYP7+yGxZg
kP4ec3/VYyxIxTbHE/gtIckB1RzocXIMNA==
-----END PRIVATE KEY-----
)";

  std::string ed448_public_key = R"(-----BEGIN PUBLIC KEY-----
MEMwBQYDK2VxAzoApBizNlW0mmJyHwJhHLeQ7VtaDb5aLUHC3cnjyKFHOglzV0dh
+kj3bnCQhYbs//ihu9lfAs3SlIiA
-----END PUBLIC KEY-----
)";

} // namespace test_keys

