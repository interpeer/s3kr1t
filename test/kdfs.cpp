/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <cstring>

#include <s3kr1t/kdfs.h>

#include <liberate/string/hexencode.h>

#define DEBUG_OUTBUFS \
  { \
    liberate::string::canonical_hexdump hd; \
    std::cerr << __LINE__ << " - out0:\n" << hd(out0, sizeof(out0)) << std::endl; \
    std::cerr << __LINE__ << " - out1:\n" << hd(out1, sizeof(out1)) << std::endl; \
  }


namespace {

s3kr1t::kdf_type test_kdfs[] = {
  s3kr1t::KDFT_SCRYPT,
  s3kr1t::KDFT_ARGON2ID,
};


std::string generate_name_value(testing::TestParamInfo<s3kr1t::kdf_type> const & ktype)
{
  using namespace s3kr1t;

  switch (ktype.param) {
    case s3kr1t::KDFT_SCRYPT:
      return "KDFT_SCRYPT";
    case s3kr1t::KDFT_ARGON2ID:
      return "KDFT_ARGON2ID";

    default:
      break;
  }
  return "";
}



} // anonymous namespace


class KDFs
  : public testing::TestWithParam<s3kr1t::kdf_type>
{
};


TEST_P(KDFs, bad_inputs)
{
  auto ktype = GetParam();

  std::string password = "super secret stuff";
  std::string salt = "sugar";
  char out[128] = {};
  size_t required = 0;


  // Bad inputs for kdf
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::kdf(
        nullptr, sizeof(out),
        required,
        password.c_str(), password.size(),
        salt.c_str(), salt.size(),
        ktype));

  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::kdf(
        out, 0,
        required,
        password.c_str(), password.size(),
        salt.c_str(), salt.size(),
        ktype));

  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::kdf(
        out, sizeof(out),
        required,
        nullptr, password.size(),
        salt.c_str(), salt.size(),
        ktype));

  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::kdf(
        out, sizeof(out),
        required,
        password.c_str(), 0,
        salt.c_str(), salt.size(),
        ktype));

  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::kdf(
        out, sizeof(out),
        required,
        password.c_str(), password.size(),
        nullptr, salt.size(),
        ktype));

  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::kdf(
        out, sizeof(out),
        required,
        password.c_str(), password.size(),
        salt.c_str(), 0,
        ktype));
}


TEST_P(KDFs, success)
{
  auto ktype = GetParam();

  std::string password = "super secret stuff";
  std::string salt = "sugar";
  char out[128] = {};
  size_t required = 0;

  // Derive key
  auto err = s3kr1t::kdf(
        out, sizeof(out),
        required,
        password.c_str(), password.size(),
        salt.c_str(), salt.size(),
        ktype);
  if (s3kr1t::ERR_UNSUPPORTED == err) {
    GTEST_SKIP() << "This algorithm is not supported with the used cryptographic backends.";
    return;
  }

  ASSERT_EQ(err, s3kr1t::ERR_SUCCESS);
  ASSERT_GT(required, 0);
  ASSERT_GE(sizeof(out), required);
}


TEST_P(KDFs, no_match_passwords)
{
  auto ktype = GetParam();

  std::string password0 = "super secret stuff";
  std::string password1 = "moar secret!!!11eleven";
  std::string salt = "sugar";
  char out0[128] = {0};
  char out1[128] = {0};
  size_t required0 = 0;
  size_t required1 = 0;

  // Derive first key
  auto err = s3kr1t::kdf(
        out0, sizeof(out0),
        required0,
        password0.c_str(), password0.size(),
        salt.c_str(), salt.size(),
        ktype);
  if (s3kr1t::ERR_UNSUPPORTED == err) {
    GTEST_SKIP() << "This algorithm is not supported with the used cryptographic backends.";
    return;
  }

  ASSERT_EQ(err, s3kr1t::ERR_SUCCESS);
  ASSERT_GT(required0, 0);
  ASSERT_GE(sizeof(out0), required0);

  // Derive second key
  err = s3kr1t::kdf(
        out1, sizeof(out1),
        required1,
        password1.c_str(), password1.size(),
        salt.c_str(), salt.size(),
        ktype);

  ASSERT_EQ(err, s3kr1t::ERR_SUCCESS);
  ASSERT_GT(required1, 0);
  ASSERT_GE(sizeof(out1), required1);

  // Compare results
  ASSERT_EQ(required0, required1);

  ASSERT_NE(0, std::memcmp(out0, out1, required0));
}



TEST_P(KDFs, no_match_salts)
{
  auto ktype = GetParam();

  std::string password = "super secret stuff";
  std::string salt0 = "sugar";
  std::string salt1 = "spice";
  char out0[128] = {};
  char out1[128] = {};
  // DEBUG_OUTBUFS;

  size_t required0 = 0;
  size_t required1 = 0;

  // Derive first key
  auto err = s3kr1t::kdf(
        out0, sizeof(out0),
        required0,
        password.c_str(), password.size(),
        salt0.c_str(), salt0.size(),
        ktype);
  if (s3kr1t::ERR_UNSUPPORTED == err) {
    GTEST_SKIP() << "This algorithm is not supported with the used cryptographic backends.";
    return;
  }

  ASSERT_EQ(err, s3kr1t::ERR_SUCCESS);
  ASSERT_GT(required0, 0);
  ASSERT_GE(sizeof(out0), required0);

  // Derive second key
  err = s3kr1t::kdf(
        out1, sizeof(out1),
        required1,
        password.c_str(), password.size(),
        salt1.c_str(), salt1.size(),
        ktype);

  ASSERT_EQ(err, s3kr1t::ERR_SUCCESS);
  ASSERT_GT(required1, 0);
  ASSERT_GE(sizeof(out1), required1);

  // Compare results
  ASSERT_EQ(required0, required1);

  // DEBUG_OUTBUFS;

  ASSERT_NE(0, std::memcmp(out0, out1, required0));
}


INSTANTIATE_TEST_SUITE_P(s3kr1t, KDFs,
    testing::ValuesIn(test_kdfs),
    generate_name_value);
