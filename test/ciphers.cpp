/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <cmath>

#include <s3kr1t/ciphers.h>

namespace {

s3kr1t::cipher_type test_ciphers[] = {
  s3kr1t::CT_AES_128_ECB,
  s3kr1t::CT_AES_128_CBC,
  s3kr1t::CT_AES_128_CFB1,
  s3kr1t::CT_AES_128_CFB128,
  s3kr1t::CT_AES_128_OFB,
  s3kr1t::CT_AES_128_CTR,
  s3kr1t::CT_AES_128_GCM,
  s3kr1t::CT_AES_128_OCB,
  s3kr1t::CT_AES_192_ECB,
  s3kr1t::CT_AES_192_CBC,
  s3kr1t::CT_AES_192_CFB1,
  s3kr1t::CT_AES_192_CFB128,
  s3kr1t::CT_AES_192_OFB,
  s3kr1t::CT_AES_192_CTR,
  s3kr1t::CT_AES_192_GCM,
  s3kr1t::CT_AES_192_OCB,
  s3kr1t::CT_AES_256_ECB,
  s3kr1t::CT_AES_256_CBC,
  s3kr1t::CT_AES_256_CFB1,
  s3kr1t::CT_AES_256_CFB128,
  s3kr1t::CT_AES_256_OFB,
  s3kr1t::CT_AES_256_CTR,
  s3kr1t::CT_AES_256_GCM,
  s3kr1t::CT_AES_256_OCB,
  s3kr1t::CT_CHACHA20,
  s3kr1t::CT_CHACHA20_POLY1305,
};


s3kr1t::cipher_type test_ciphers_aead[] = {
  s3kr1t::CT_AES_128_GCM,
  s3kr1t::CT_AES_128_OCB,
  s3kr1t::CT_AES_192_GCM,
  s3kr1t::CT_AES_192_OCB,
  s3kr1t::CT_AES_256_GCM,
  s3kr1t::CT_AES_256_OCB,
  s3kr1t::CT_CHACHA20_POLY1305,
};



std::string generate_name_value(testing::TestParamInfo<s3kr1t::cipher_type> const & ctype)
{
  using namespace s3kr1t;

  switch (ctype.param) {
    case s3kr1t::CT_AES_128_ECB:
      return "CT_AES_128_ECB";

    case s3kr1t::CT_AES_128_CBC:
      return "CT_AES_128_CBC";

    case s3kr1t::CT_AES_128_CFB1:
      return "CT_AES_128_CFB1";

    case s3kr1t::CT_AES_128_CFB128:
      return "CT_AES_128_CFB128";

    case s3kr1t::CT_AES_128_OFB:
      return "CT_AES_128_OFB";

    case s3kr1t::CT_AES_128_CTR:
      return "CT_AES_128_CTR";

    case s3kr1t::CT_AES_128_GCM:
      return "CT_AES_128_GCM";

    case s3kr1t::CT_AES_128_OCB:
      return "CT_AES_128_OCB";

    case s3kr1t::CT_AES_192_ECB:
      return "CT_AES_192_ECB";

    case s3kr1t::CT_AES_192_CBC:
      return "CT_AES_192_CBC";

    case s3kr1t::CT_AES_192_CFB1:
      return "CT_AES_192_CFB1";

    case s3kr1t::CT_AES_192_CFB128:
      return "CT_AES_192_CFB128";

    case s3kr1t::CT_AES_192_OFB:
      return "CT_AES_192_OFB";

    case s3kr1t::CT_AES_192_CTR:
      return "CT_AES_192_CTR";

    case s3kr1t::CT_AES_192_GCM:
      return "CT_AES_192_GCM";

    case s3kr1t::CT_AES_192_OCB:
      return "CT_AES_192_OCB";

    case s3kr1t::CT_AES_256_ECB:
      return "CT_AES_256_ECB";

    case s3kr1t::CT_AES_256_CBC:
      return "CT_AES_256_CBC";

    case s3kr1t::CT_AES_256_CFB1:
      return "CT_AES_256_CFB1";

    case s3kr1t::CT_AES_256_CFB128:
      return "CT_AES_256_CFB128";

    case s3kr1t::CT_AES_256_OFB:
      return "CT_AES_256_OFB";

    case s3kr1t::CT_AES_256_CTR:
      return "CT_AES_256_CTR";

    case s3kr1t::CT_AES_256_GCM:
      return "CT_AES_256_GCM";

    case s3kr1t::CT_AES_256_OCB:
      return "CT_AES_256_OCB";

    case s3kr1t::CT_CHACHA20:
      return "CT_CHACHA20";

    case s3kr1t::CT_CHACHA20_POLY1305:
      return "CT_CHACHA20_POLY1305";

    default:
      break;
  }
  return "";
}


inline std::string
extend_to(std::size_t size, std::string const & val)
{
  std::string ret = val;
  if (size > 0) {
    std::size_t remain_size = ret.size() % size;
    if (remain_size > 0) {
      auto pad_size = size - remain_size;
      for (size_t i = 0 ; i < pad_size ; ++i) {
        ret += static_cast<char>(i + 65);
      }
    }
  }
  return ret;
}


inline std::tuple<std::string, std::string>
create_key_and_iv(s3kr1t::cipher const & ciph)
{
  return {
    extend_to(ciph.key_size(), "Hello"),
    extend_to(ciph.iv_size(), "World")
  };
}


} // anonymous namespace



TEST(Ciphers, construction_no_cipher)
{
  ASSERT_THROW((s3kr1t::cipher{s3kr1t::CT_NONE, s3kr1t::CO_ENCRYPT}), s3kr1t::exception);
}


TEST(Ciphers, construction_bad_cipher)
{
  ASSERT_THROW((s3kr1t::cipher{static_cast<s3kr1t::cipher_type>(255), s3kr1t::CO_ENCRYPT}), s3kr1t::exception);
}



class Ciphers
  : public testing::TestWithParam<s3kr1t::cipher_type>
{
};



TEST_P(Ciphers, construction)
{
  auto ctype = GetParam();

  s3kr1t::cipher ciph{ctype, s3kr1t::CO_ENCRYPT};

  // ECB modes are fine with a zero-sized IV
  // ASSERT_NE(0, ciph.iv_size());
  ASSERT_EQ(ciph.iv_size(), ciph.nonce_size());
  ASSERT_NE(0, ciph.key_size());
  ASSERT_NE(0, ciph.block_size());
}



TEST_P(Ciphers, initialization)
{
  auto ctype = GetParam();

  s3kr1t::cipher ciph{ctype, s3kr1t::CO_ENCRYPT};

  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, ciph.initialize(nullptr, nullptr));
  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, ciph.initialize(nullptr, "foo"));
  ASSERT_EQ(s3kr1t::ERR_INVALID_VALUE, ciph.initialize("foo", nullptr));

  auto [key, iv] = create_key_and_iv(ciph);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph.initialize(key.c_str(), iv.c_str()));
}


TEST_P(Ciphers, crypt)
{
  auto ctype = GetParam();

  s3kr1t::cipher ciph{ctype, s3kr1t::CO_ENCRYPT};

  auto [key, iv] = create_key_and_iv(ciph);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph.initialize(key.c_str(), iv.c_str()));

  std::string data = "Hello, world!";

  // *** Encrypt
  std::size_t outsize = (std::trunc(static_cast<double>(data.size()) / ciph.block_size()) + 1) * ciph.block_size();

  std::vector<char> out;
  out.resize(outsize);

  // Fail with a too-small buffer
  std::size_t size = 3;
  auto err = ciph.encrypt(&out[0], size, data.c_str(), data.size());
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE, err);

  // Succeed encrypting
  size = out.size();
  err = ciph.encrypt(&out[0], size, data.c_str(), data.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  std::size_t written = size;

  auto offset = &out[size];
  size = out.size() - size;

  std::vector<char> tag;
  tag.resize(ciph.tag_size());
  char * tag_ptr = nullptr;
  if (tag.size()) {
    tag_ptr = &tag[0];
  }

  err = ciph.encrypt_final(offset, size, tag_ptr);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written += size;

  ASSERT_GE(written, data.size());

  // *** Decrypt
  std::vector<char> done;
  done.resize(out.size());

  // Must fail with the same cipher instance.
  size = done.size();
  err = ciph.decrypt(&done[0], size, out.data(), written);
  ASSERT_EQ(s3kr1t::ERR_UNSUPPORTED, err);
  ASSERT_EQ(size, 0);

  // Create new instance
  s3kr1t::cipher ciph2{ciph.type(), s3kr1t::CO_DECRYPT};
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph2.initialize(key.c_str(), iv.c_str(), tag_ptr));

  size = done.size();
  err = ciph2.decrypt(&done[0], size, out.data(), written);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written = size;

  offset = &done[size];
  size = done.size() - size;

  err = ciph2.decrypt_final(offset, size);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written += size;

  ASSERT_GE(written, data.size());
  done.resize(written);

  std::string recovered{done.data(), done.data() + done.size()};
  ASSERT_EQ(recovered, data);
}



TEST_P(Ciphers, succeed_on_reuse)
{
  auto ctype = GetParam();

  s3kr1t::cipher ciph{ctype, s3kr1t::CO_ENCRYPT};

  auto [key, iv] = create_key_and_iv(ciph);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph.initialize(key.c_str(), iv.c_str()));

  std::string data = "Hello, world!";

  // *** Encrypt
  std::size_t outsize = (std::trunc(static_cast<double>(data.size()) / ciph.block_size()) + 1) * ciph.block_size();

  std::vector<char> out;
  out.resize(outsize);

  // Succeed encrypting
  auto size = out.size();
  auto err = ciph.encrypt(&out[0], size, data.c_str(), data.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  std::size_t written = size;

  auto offset = &out[size];
  size = out.size() - size;

  std::vector<char> tag;
  tag.resize(ciph.tag_size());
  char * tag_ptr = nullptr;
  if (tag.size()) {
    tag_ptr = &tag[0];
  }

  err = ciph.encrypt_final(offset, size, tag_ptr);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written += size;

  ASSERT_GE(written, data.size());

  // *** Encrypt again.
  size = out.size();
  err = ciph.encrypt(&out[0], size, data.c_str(), data.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);

  // No need to verify anything
}



INSTANTIATE_TEST_SUITE_P(s3kr1t, Ciphers,
    testing::ValuesIn(test_ciphers),
    generate_name_value);



class CiphersWithAuthentication
  : public testing::TestWithParam<s3kr1t::cipher_type>
{
};


TEST_P(CiphersWithAuthentication, crypt_with_aad)
{
  auto ctype = GetParam();

  s3kr1t::cipher ciph{ctype, s3kr1t::CO_ENCRYPT};

  auto [key, iv] = create_key_and_iv(ciph);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph.initialize(key.c_str(), iv.c_str()));

  std::string data = "Hello, world!";
  std::string auth1 = "foo";
  std::string auth2 = "bar";

  // *** Encrypt
  std::size_t outsize = (std::trunc(static_cast<double>(data.size()) / ciph.block_size()) + 1) * ciph.block_size();

  std::vector<char> out;
  out.resize(outsize);

   // Succeed encrypting
  auto err = ciph.add_authenticated(auth1.c_str(), auth1.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  err = ciph.add_authenticated(auth2.c_str(), auth2.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);

  auto size = out.size();
  err = ciph.encrypt(&out[0], size, data.c_str(), data.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  std::size_t written = size;

  auto offset = &out[size];
  size = out.size() - size;

  std::vector<char> tag;
  tag.resize(ciph.tag_size());
  char * tag_ptr = nullptr;
  if (tag.size()) {
    tag_ptr = &tag[0];
  }

  err = ciph.encrypt_final(offset, size, tag_ptr);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written += size;

  ASSERT_GE(written, data.size());

  // *** Decrypt
  std::vector<char> done;
  done.resize(out.size());

  // Must fail with the same cipher instance.
  size = done.size();
  err = ciph.decrypt(&done[0], size, out.data(), written);
  ASSERT_EQ(s3kr1t::ERR_UNSUPPORTED, err);
  ASSERT_EQ(size, 0);

  // Create new instance
  s3kr1t::cipher ciph2{ciph.type(), s3kr1t::CO_DECRYPT};
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph2.initialize(key.c_str(), iv.c_str(), tag_ptr));

  err = ciph2.add_authenticated(auth1.c_str(), auth1.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  err = ciph2.add_authenticated(auth2.c_str(), auth2.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);

  size = done.size();
  err = ciph2.decrypt(&done[0], size, out.data(), written);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written = size;

  offset = &done[size];
  size = done.size() - size;

  err = ciph2.decrypt_final(offset, size);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written += size;

  ASSERT_GE(written, data.size());
  done.resize(written);

  std::string recovered{done.data(), done.data() + done.size()};
  ASSERT_EQ(recovered, data);
}



TEST_P(CiphersWithAuthentication, decrypt_fail_without_aad)
{
  auto ctype = GetParam();

  s3kr1t::cipher ciph{ctype, s3kr1t::CO_ENCRYPT};

  auto [key, iv] = create_key_and_iv(ciph);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph.initialize(key.c_str(), iv.c_str()));

  std::string data = "Hello, world!";
  std::string auth1 = "foo";
  std::string auth2 = "bar";

  // *** Encrypt
  std::size_t outsize = (std::trunc(static_cast<double>(data.size()) / ciph.block_size()) + 1) * ciph.block_size();

  std::vector<char> out;
  out.resize(outsize);

  // Succeed encrypting
  auto err = ciph.add_authenticated(auth1.c_str(), auth1.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  err = ciph.add_authenticated(auth2.c_str(), auth2.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);

  auto size = out.size();
  err = ciph.encrypt(&out[0], size, data.c_str(), data.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  std::size_t written = size;

  auto offset = &out[size];
  size = out.size() - size;

  std::vector<char> tag;
  tag.resize(ciph.tag_size());
  char * tag_ptr = nullptr;
  if (tag.size()) {
    tag_ptr = &tag[0];
  }

  err = ciph.encrypt_final(offset, size, tag_ptr);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written += size;

  ASSERT_GE(written, data.size());

  // *** Decrypt
  std::vector<char> done;
  done.resize(out.size());

  // Must fail with the same cipher instance.
  size = done.size();
  err = ciph.decrypt(&done[0], size, out.data(), written);
  ASSERT_EQ(s3kr1t::ERR_UNSUPPORTED, err);
  ASSERT_EQ(size, 0);

  // Create new instance
  s3kr1t::cipher ciph2{ciph.type(), s3kr1t::CO_DECRYPT};
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph2.initialize(key.c_str(), iv.c_str(), tag_ptr));

  // Skip adding AAD during decryption, and the decrypt operation must fail
  // err = ciph2.add_authenticated(auth1.c_str(), auth1.size());
  // ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  // err = ciph2.add_authenticated(auth2.c_str(), auth2.size());
  // ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);

  size = done.size();
  err = ciph2.decrypt(&done[0], size, out.data(), written);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written = size;

  offset = &done[size];
  size = done.size() - size;

  err = ciph2.decrypt_final(offset, size);
  ASSERT_EQ(s3kr1t::ERR_DECRYPT, err);
}



TEST_P(CiphersWithAuthentication, decrypt_fail_without_tag)
{
  auto ctype = GetParam();

  s3kr1t::cipher ciph{ctype, s3kr1t::CO_ENCRYPT};

  auto [key, iv] = create_key_and_iv(ciph);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph.initialize(key.c_str(), iv.c_str()));

  std::string data = "Hello, world!";
  std::string auth1 = "foo";
  std::string auth2 = "bar";

  // *** Encrypt
  std::size_t outsize = (std::trunc(static_cast<double>(data.size()) / ciph.block_size()) + 1) * ciph.block_size();

  std::vector<char> out;
  out.resize(outsize);

  // Succeed encrypting
  auto err = ciph.add_authenticated(auth1.c_str(), auth1.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  err = ciph.add_authenticated(auth2.c_str(), auth2.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);

  auto size = out.size();
  err = ciph.encrypt(&out[0], size, data.c_str(), data.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  std::size_t written = size;

  auto offset = &out[size];
  size = out.size() - size;

  std::vector<char> tag;
  tag.resize(ciph.tag_size());
  char * tag_ptr = nullptr;
  if (tag.size()) {
    tag_ptr = &tag[0];
  }

  err = ciph.encrypt_final(offset, size, tag_ptr);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written += size;

  ASSERT_GE(written, data.size());

  // *** Decrypt
  std::vector<char> done;
  done.resize(out.size());

  // Must fail with the same cipher instance.
  size = done.size();
  err = ciph.decrypt(&done[0], size, out.data(), written);
  ASSERT_EQ(s3kr1t::ERR_UNSUPPORTED, err);
  ASSERT_EQ(size, 0);

  // Create new instance - but fail to add tag!
  s3kr1t::cipher ciph2{ciph.type(), s3kr1t::CO_DECRYPT};
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, ciph2.initialize(key.c_str(), iv.c_str(), nullptr));

  err = ciph2.add_authenticated(auth1.c_str(), auth1.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  err = ciph2.add_authenticated(auth2.c_str(), auth2.size());
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);

  size = done.size();
  err = ciph2.decrypt(&done[0], size, out.data(), written);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  written = size;

  offset = &done[size];
  size = done.size() - size;

  err = ciph2.decrypt_final(offset, size);
  ASSERT_EQ(s3kr1t::ERR_INITIALIZATION, err);
}



INSTANTIATE_TEST_SUITE_P(s3kr1t, CiphersWithAuthentication,
    testing::ValuesIn(test_ciphers_aead),
    generate_name_value);
