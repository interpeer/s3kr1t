/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <s3kr1t/keys.h>

#include <liberate/string/hexencode.h>

#include "test_keys.h"

namespace {

inline void
test_pubkey(std::string const & input, s3kr1t::key_type expected,
    size_t expected_bits = 0)
{
  auto pubkey = s3kr1t::key::read_pubkey(input);
  ASSERT_TRUE(pubkey);
  ASSERT_TRUE(pubkey.is_public());
  ASSERT_FALSE(pubkey.is_private());
  ASSERT_EQ(expected, pubkey.type());
  if (expected_bits) {
    ASSERT_EQ(expected_bits, pubkey.bits());
  }

  auto hash = pubkey.hash();
  ASSERT_EQ(hash.size(), 64); // Default SHA3-512

  auto hexhash = pubkey.hex_hash();
  ASSERT_EQ(hash.size() * 2, hexhash.size());
}


inline s3kr1t::key::pair
test_privkey_ops(s3kr1t::key const & privkey, s3kr1t::key_type expected)
{
  EXPECT_TRUE(privkey);
  EXPECT_TRUE(privkey.is_private());
  EXPECT_FALSE(privkey.is_public());
  EXPECT_EQ(expected, privkey.type());

  auto hash = privkey.hash();
  EXPECT_EQ(hash.size(), 64); // Default SHA3-512

  auto hexhash = privkey.hex_hash();
  EXPECT_EQ(hash.size() * 2, hexhash.size());

  auto pair = privkey.create_pair();
  EXPECT_TRUE(pair.privkey);
  EXPECT_TRUE(pair.pubkey);
  if (!pair.privkey || !pair.pubkey) {
    return pair;
  }

  EXPECT_EQ(*pair.privkey, privkey);
  EXPECT_TRUE(pair.pubkey);
  EXPECT_TRUE(*pair.pubkey);
  EXPECT_TRUE(pair.pubkey->is_public());
  EXPECT_FALSE(pair.pubkey->is_private());
  EXPECT_EQ(expected, pair.pubkey->type());

  return pair;
}


inline void
test_privkey(std::string const & privinput, std::string const & pubinput,
    s3kr1t::key_type expected, std::string const & password = {})
{
  auto privkey = s3kr1t::key::read_privkey(privinput, password);
  auto pair = test_privkey_ops(privkey, expected);

  auto pubkey = s3kr1t::key::read_pubkey(pubinput);
  ASSERT_TRUE(pubkey);
  ASSERT_EQ(pubkey, *pair.pubkey);
}


inline void
test_pair(std::string const & input,
    s3kr1t::key_type expected, std::string const & password = {})
{
  auto pair = s3kr1t::key::read_keys(input, password);
  ASSERT_TRUE(pair.privkey);
  ASSERT_TRUE(*pair.privkey);
  ASSERT_TRUE(pair.privkey->is_private());
  ASSERT_FALSE(pair.privkey->is_public());
  ASSERT_EQ(expected, pair.privkey->type());

  ASSERT_TRUE(pair.pubkey);
  ASSERT_TRUE(*pair.pubkey);
  ASSERT_FALSE(pair.pubkey->is_private());
  ASSERT_TRUE(pair.pubkey->is_public());
  ASSERT_EQ(expected, pair.pubkey->type());
}


inline void
test_write_key(s3kr1t::key const & key, s3kr1t::key_encoding encoding,
    bool is_public,
    std::string const & password = {})
{
  size_t written = 0;

  // Write without buffer
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      key.write(nullptr, 1000, encoding, written));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      key.write(reinterpret_cast<void *>(0xdeadbeef), 0, encoding, written));

  // Write into a small buffer; this *must* fail without memory corruption.
  char smallbuf[20] = {};
  auto err = key.write(smallbuf, sizeof(smallbuf), encoding, written, password);
  ASSERT_NE(s3kr1t::ERR_SUCCESS, err);
  ASSERT_EQ(0, written);

  // Write into a sufficently large buffer.
  char buf[8192] = {};
  err = key.write(buf, sizeof(buf), encoding, written, password);
  ASSERT_EQ(s3kr1t::ERR_SUCCESS, err);
  ASSERT_TRUE(written > 0);

#ifdef DEBUG
  std::cerr << "written: " << written << std::endl;
  liberate::string::canonical_hexdump hd;
  std::cerr << hd(buf, written) << std::endl;
#endif

  // Read buffer back into a new key
  s3kr1t::key newkey;
  if (is_public) {
    newkey = s3kr1t::key::read_pubkey(buf, written);
  }
  else {
    newkey = s3kr1t::key::read_privkey(buf, written, password);
  }
  ASSERT_TRUE(newkey);
  ASSERT_EQ(key.type(), newkey.type());
  ASSERT_EQ(key, newkey);
}


inline void
test_write(std::string const & input, s3kr1t::key_type expected,
    std::string const & password = {})
{
  auto privkey = s3kr1t::key::read_privkey(input, password);
  ASSERT_TRUE(privkey);
  auto pair = test_privkey_ops(privkey, expected);
  ASSERT_TRUE(pair);

  // Without password
  test_write_key(*pair.privkey, s3kr1t::KE_PEM, false);
  test_write_key(*pair.privkey, s3kr1t::KE_DER, false);
  test_write_key(*pair.pubkey, s3kr1t::KE_PEM, true);
  test_write_key(*pair.pubkey, s3kr1t::KE_DER, true);

  if (s3kr1t::KT_ED == pair.privkey->type()) {
    test_write_key(*pair.privkey, s3kr1t::KE_RAW, false);
  }
  if (s3kr1t::KT_ED == pair.pubkey->type()) {
    test_write_key(*pair.pubkey, s3kr1t::KE_RAW, true);
  }

  // With password
  test_write_key(*pair.privkey, s3kr1t::KE_PEM, false, "s3kr1t");

  // TODO fails due to a bug in OpenSSL
  // test_write_key(*pair.privkey, s3kr1t::KE_DER, false, "s3kr1t");
}


} // anonymous namespace


TEST(Keys, pubkey_from_certs)
{
  // X509 cert, RSA key
  test_pubkey(test_keys::google_cert_root2, s3kr1t::KT_RSA, 4096);

  // DER cert, ECDSA key
  test_pubkey(test_keys::google_cert_root4, s3kr1t::KT_EC, 384);
}


TEST(Keys, bad_input)
{
  ASSERT_THROW(s3kr1t::key::read_pubkey("foobar"), s3kr1t::exception);
}


TEST(Keys, missing_password)
{
  ASSERT_THROW(s3kr1t::key::read_privkey(test_keys::rsa_encrypted_private_key), s3kr1t::exception);
}

// *** ECDSA *****************************************************************


TEST(Keys, ecdsa_pubkey)
{
  test_pubkey(test_keys::ecdsa_public_key_p256, s3kr1t::KT_EC, 256);
  test_pubkey(test_keys::ecdsa_public_key_p384, s3kr1t::KT_EC, 384);
  test_pubkey(test_keys::ecdsa_public_key_p521, s3kr1t::KT_EC, 521);
}


TEST(Keys, ecdsa_privkey)
{
  test_privkey(test_keys::ecdsa_private_key_p256,
      test_keys::ecdsa_public_key_p256,
      s3kr1t::KT_EC);

  test_privkey(test_keys::ecdsa_private_key_p384,
      test_keys::ecdsa_public_key_p384,
      s3kr1t::KT_EC);

  test_privkey(test_keys::ecdsa_private_key_p521,
      test_keys::ecdsa_public_key_p521,
      s3kr1t::KT_EC);
}



TEST(Keys, ecdsa_pair)
{
  test_pair(test_keys::ecdsa_private_key_p256,
      s3kr1t::KT_EC);

  test_pair(test_keys::ecdsa_private_key_p384,
      s3kr1t::KT_EC);

  test_pair(test_keys::ecdsa_private_key_p521,
      s3kr1t::KT_EC);
}


TEST(Keys, ecdsa_generate)
{
  auto privkey = s3kr1t::key::generate(s3kr1t::KT_EC, "secp256k1");
  test_privkey_ops(privkey, s3kr1t::KT_EC);

  auto badkey = s3kr1t::key::generate(s3kr1t::KT_EC, "nope");
  ASSERT_FALSE(badkey);
}


TEST(Keys, ecdsa_write)
{
  test_write(test_keys::ecdsa_private_key_p256, s3kr1t::KT_EC);
  test_write(test_keys::ecdsa_private_key_p384, s3kr1t::KT_EC);
  test_write(test_keys::ecdsa_private_key_p521, s3kr1t::KT_EC);
}

// *** RSA *******************************************************************


TEST(Keys, rsa_pubkey)
{
  test_pubkey(test_keys::rsa_public_key, s3kr1t::KT_RSA, 2048);
  test_pubkey(test_keys::rsa_other_public_key, s3kr1t::KT_RSA, 1024);
}



TEST(Keys, rsa_privkey)
{
  test_privkey(test_keys::rsa_encrypted_private_key,
      test_keys::rsa_public_key,
      s3kr1t::KT_RSA,
      test_keys::rsa_encrypted_key_password);

  test_privkey(test_keys::rsa_other_private_key,
      test_keys::rsa_other_public_key,
      s3kr1t::KT_RSA);
}


TEST(Keys, rsa_pair)
{
  test_pair(test_keys::rsa_encrypted_private_key,
      s3kr1t::KT_RSA,
      test_keys::rsa_encrypted_key_password);

  test_pair(test_keys::rsa_other_private_key,
      s3kr1t::KT_RSA);
}


TEST(Keys, rsa_generate)
{
  auto privkey = s3kr1t::key::generate(s3kr1t::KT_RSA, "1024");
  test_privkey_ops(privkey, s3kr1t::KT_RSA);

  auto badkey = s3kr1t::key::generate(s3kr1t::KT_RSA, "0");
  ASSERT_FALSE(badkey);
}


TEST(Keys, rsa_write)
{
  test_write(test_keys::rsa_encrypted_private_key,
      s3kr1t::KT_RSA,
      test_keys::rsa_encrypted_key_password);

  test_write(test_keys::rsa_other_private_key,
      s3kr1t::KT_RSA);
}

// *** DSA *******************************************************************

TEST(Keys, dsa_pubkey)
{
  test_pubkey(test_keys::dsa_public_key, s3kr1t::KT_DSA, 2048);
}


TEST(Keys, dsa_privkey)
{
  test_privkey(test_keys::dsa_private_key,
      test_keys::dsa_public_key,
      s3kr1t::KT_DSA);
}

TEST(Keys, dsa_pair)
{
  test_pair(test_keys::dsa_private_key,
      s3kr1t::KT_DSA);
}


TEST(Keys, dsa_generate)
{
  auto privkey = s3kr1t::key::generate(s3kr1t::KT_DSA, "1024");
  test_privkey_ops(privkey, s3kr1t::KT_DSA);

  auto badkey = s3kr1t::key::generate(s3kr1t::KT_DSA, "0");
  ASSERT_FALSE(badkey);
}


TEST(Keys, dsa_write)
{
  test_write(test_keys::dsa_private_key,
      s3kr1t::KT_DSA);
}


// *** Edwards ****************************************************************

TEST(Keys, ed_pubkey)
{
  test_pubkey(test_keys::ed25519_public_key, s3kr1t::KT_ED, 25519);
  test_pubkey(test_keys::ed448_public_key, s3kr1t::KT_ED, 448);
}


TEST(Keys, ed_privkey)
{
  test_privkey(test_keys::ed25519_private_key,
      test_keys::ed25519_public_key,
      s3kr1t::KT_ED);

  test_privkey(test_keys::ed448_private_key,
      test_keys::ed448_public_key,
      s3kr1t::KT_ED);

}

TEST(Keys, ed_pair)
{
  test_pair(test_keys::ed25519_private_key,
      s3kr1t::KT_ED);
  test_pair(test_keys::ed448_private_key,
      s3kr1t::KT_ED);
}


TEST(Keys, ed_generate)
{
  {
    auto privkey = s3kr1t::key::generate(s3kr1t::KT_ED, "ed25519");
    test_privkey_ops(privkey, s3kr1t::KT_ED);
  }

  {
    auto privkey = s3kr1t::key::generate(s3kr1t::KT_ED, "ed448");
    test_privkey_ops(privkey, s3kr1t::KT_ED);
  }

  auto badkey = s3kr1t::key::generate(s3kr1t::KT_ED, "0");
  ASSERT_FALSE(badkey);
}


TEST(Keys, ed_write)
{
  test_write(test_keys::ed25519_private_key,
      s3kr1t::KT_ED);

  test_write(test_keys::ed448_private_key,
      s3kr1t::KT_ED);
}
