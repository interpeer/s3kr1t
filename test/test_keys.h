/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_TEST_KEYS_H
#define S3KR1T_TEST_KEYS_H

#include <string>

namespace test_keys
{
  // RSA key, PEM encoded
  extern std::string google_cert_root2; // RSA
  // ECDSA key, DER encoded
  extern std::string google_cert_root4; // ECDSA

  // ECDSA

  // openssl ecparam -out ec_key.pem -name secp256k1 -genkey
  // openssl ec -in myprivatekey.pem -pubout -out mypublickey.pem
  extern std::string ecdsa_private_key_p256;
  extern std::string ecdsa_public_key_p256;

  extern std::string ecdsa_private_key_p384;
  extern std::string ecdsa_public_key_p384;

  extern std::string ecdsa_private_key_p521;
  extern std::string ecdsa_public_key_p521;

  // RSA

  // openssl genrsa [-aes256] -out private-key.pem 3072
  // openssl rsa -in private-key.pem -pubout -out public-key.pem
  extern std::string rsa_encrypted_private_key;
  extern std::string rsa_encrypted_key_password;
  extern std::string rsa_public_key;

  // RSA - 1024 bits is small for some operations
  extern std::string rsa_other_private_key;
  extern std::string rsa_other_public_key;

  // DSA

  // openssl dsaparam -out dsaparam.pem 2048
  // openssl gendsa -out dsaprivkey.pem dsaparam.pem
  // openssl dsa -in dsaprivkey.pem -pubout -out dsapubkey.pem
  extern std::string dsa_private_key;
  extern std::string dsa_public_key;

  // Edwards

  // openssl genpkey -algorithm ed25519 -out private.pem
  // openssl pkey -in private.pem -pubout -out public.pem
  extern std::string ed25519_private_key;
  extern std::string ed25519_public_key;

  extern std::string ed448_private_key;
  extern std::string ed448_public_key;

} // namespace test_keys

#endif // guard
