/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <gtest/gtest.h>

#include <s3kr1t/digests.h>

namespace {

s3kr1t::digest_type test_digests[] = {
  s3kr1t::DT_SHA2_224,
  s3kr1t::DT_SHA2_256,
  s3kr1t::DT_SHA2_384,
  s3kr1t::DT_SHA2_512,

  s3kr1t::DT_SHA3_224,
  s3kr1t::DT_SHA3_256,
  s3kr1t::DT_SHA3_384,
  s3kr1t::DT_SHA3_512,
};


std::string generate_name_value(testing::TestParamInfo<s3kr1t::digest_type> const & dtype)
{
  using namespace s3kr1t;

  switch (dtype.param) {
    case DT_SHA2_224:
      return "DT_SHA2_224";
    case DT_SHA2_256:
      return "DT_SHA2_256";
    case DT_SHA2_384:
      return "DT_SHA2_384";
    case DT_SHA2_512:
      return "DT_SHA2_512";

    case DT_SHA3_224:
      return "DT_SHA3_224";
    case DT_SHA3_256:
      return "DT_SHA3_256";
    case DT_SHA3_384:
      return "DT_SHA3_384";
    case DT_SHA3_512:
      return "DT_SHA3_512";

    default:
      break;
  }
  return "";
}



} // anonymous namespace


class Digests
  : public testing::TestWithParam<s3kr1t::digest_type>
{
};


TEST_P(Digests, bad_inputs)
{
  auto dtype = GetParam();

  std::string message = "Hello, world!";
  char dig[512] = {};
  size_t required = 0;

  // Bad inputs for digest
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::digest(
        nullptr, sizeof(dig),
        required,
        message.c_str(), message.size(),
        dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::digest(
        dig, 0,
        required,
        message.c_str(), message.size(),
        dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::digest(
        dig, sizeof(dig),
        required,
        nullptr, message.size(),
        dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::digest(
        dig, sizeof(dig),
        required,
        message.c_str(), 0,
        dtype));

  // Bad inputs for verify
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        nullptr, message.size(),
        dig, sizeof(dig),
        dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), 0,
        dig, sizeof(dig),
        dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), message.size(),
        nullptr, sizeof(dig),
        dtype));
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), message.size(),
        dig, 0,
        dtype));
}


TEST_P(Digests, small_buffer)
{
  auto dtype = GetParam();

  std::string message = "Hello, world!";
  char dig[3] = {};
  size_t required = 0;

  // Create digest
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::digest(
        dig, sizeof(dig),
        required,
        message.c_str(), message.size(),
        dtype));
  ASSERT_GT(required, 0);

  // Verify digest
  ASSERT_EQ(s3kr1t::ERR_INSUFFICIENT_BUFFER_SIZE,
      s3kr1t::check(
        message.c_str(), message.size(),
        dig, sizeof(dig),
        dtype));
}



TEST_P(Digests, match)
{
  auto dtype = GetParam();

  std::string message = "Hello, world!";
  char dig[512] = {};
  size_t required = 0;

  // Create digest
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::digest(
        dig, sizeof(dig),
        required,
        message.c_str(), message.size(),
        dtype));
  ASSERT_GT(required, 0);

  // Verify digest
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::check(
        message.c_str(), message.size(),
        dig, required,
        dtype));
}


TEST_P(Digests, no_match)
{
  auto dtype = GetParam();

  std::string message = "Hello, world!";
  char dig[512] = {};
  size_t required = 0;

  // Create digest
  ASSERT_EQ(s3kr1t::ERR_SUCCESS,
      s3kr1t::digest(
        dig, sizeof(dig),
        required,
        message.c_str(), message.size(),
        dtype));
  ASSERT_GT(required, 0);

  // Verify digest - but over part of the message only!
  ASSERT_EQ(s3kr1t::ERR_VERIFY,
      s3kr1t::check(
        message.c_str(), message.size() - 3,
        dig, required,
        dtype));
}


INSTANTIATE_TEST_SUITE_P(s3kr1t, Digests,
    testing::ValuesIn(test_digests),
    generate_name_value);
