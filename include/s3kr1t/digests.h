/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_DIGESTS_H
#define S3KR1T_DIGESTS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <s3kr1t.h>

#include <s3kr1t/error.h>

namespace s3kr1t {

/**
 * Digest types.
 */
enum S3KR1T_API digest_type : uint8_t
{
  DT_NONE = 0,

  // SHA-2
  DT_SHA2_224 = 1,
  DT_SHA2_256 = 2,
  DT_SHA2_384 = 3,
  DT_SHA2_512 = 4,

  // Common aliases for SHA-2
  DT_SHA224 = DT_SHA2_224,
  DT_SHA256 = DT_SHA2_256,
  DT_SHA384 = DT_SHA2_384,
  DT_SHA512 = DT_SHA2_512,

  // SHA-3
  DT_SHA3_224 = 5,
  DT_SHA3_256 = 6,
  DT_SHA3_384 = 7,
  DT_SHA3_512 = 8,

  // TODO
};


/**
 * Create or check a message digest. If no inputs are given,
 * ERR_INSUFFICIENT_BUFFER_SIZE is returned. If the output buffer is too
 * small, the same error is returned, but the required parameter is set to
 * the number of Bytes necessary. On success, the parameter returns the number
 * of Bytes consumed.
 */
S3KR1T_API error_t
digest(void * digbuf, size_t digbuf_size, size_t & required,
    void const * data, size_t data_size,
    digest_type dtype);


S3KR1T_API error_t
check(void const * data, size_t data_size,
    void const * digbuf, size_t digbuf_size,
    digest_type dtype);


} // namespace s3kr1t

#endif // guard
