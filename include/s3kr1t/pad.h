/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_PAD_H
#define S3KR1T_PAD_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <s3kr1t.h>

#include <s3kr1t/error.h>

namespace s3kr1t {

/**
 * PKCS7 padding is used e.g. with AES, and pads incomplete plain text blocks
 * prior to encryption. We want to use the same mechanism, but more flexibly.
 *
 * a) Since it's used with fixed, limited length blocks, PKCS7 is only defined
 *    for those block sizes. We want to use arbitrary length buffers.
 * b) Since it's used to pad incomplete blocks, padding is either omitted
 *    entirely or at least one Byte longh. Instead, we want to use arbitrary
 *    length input buffers, which may also be zero length. The result is much
 *    the same, but technically this is not the PKCS7 standard.
 * c) Since we pad arbitrary length buffers, the pad value is the size of buffer
 *    we need to pad mod 256. If the padded buffer is large enough, it won't be
 *    possible to read the size of the padded area from the pad value. Use
 *    some kind of length encoding in your buffers.
 */
S3KR1T_API error_t
pkcs7_pad_flexible(void * buffer, std::size_t const & size);


/**
 * Same as the above, but it calculates the size of the padded area for you.
 * You pass in the *entire* buffer, and an offset at which padding should
 * start. If it is within the specified buffer, padding is applied, and the
 * number of padded Bytes is returned in pad_size. The pad_size is ignored as
 * input.
 */
S3KR1T_API error_t
pkcs7_pad_easy(void const * buffer, std::size_t const & buffer_size,
    void * pad_offset, std::size_t & pad_size);


} // namespace s3kr1t

#endif // guard
