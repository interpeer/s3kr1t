/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_MACS_H
#define S3KR1T_MACS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <s3kr1t.h>

#include <s3kr1t/error.h>

namespace s3kr1t {

/**
 * MAC types.
 */
enum S3KR1T_API mac_type : uint8_t
{
  MT_NONE = 0,

  MT_POLY1305 = 1,

  MT_KMAC_128 = 2,
  MT_KMAC_256 = 3,

  // TODO
};


/**
 * Create or check a message authentication code. If no inputs are given,
 * ERR_INSUFFICIENT_BUFFER_SIZE is returned. If the output buffer is too
 * small, the same error is returned, but the required parameter is set to
 * the number of Bytes necessary. On success, the parameter returns the number
 * of Bytes consumed.
 *
 * Note that the "extra" may be optional, but highly recommended. For Poly1305,
 * this is the nonce - a required parameter. For KMAC functions, this is the
 * customization string, which helps avoid attacks where plaintext is crafted
 * to create collisions. Think of it as the specific purpose of this particular
 * MAC.
 */
S3KR1T_API error_t
mac(void * macbuf, size_t macbuf_size, size_t & required,
    void const * data, size_t data_size,
    void const * key, size_t key_size,
    void const * extra, size_t extra_size,
    mac_type mtype);


S3KR1T_API error_t
check(void const * data, size_t data_size,
    void const * macbuf, size_t macbuf_size,
    void const * key, size_t key_size,
    void const * extra, size_t extra_size,
    mac_type mtype);

} // namespace s3kr1t

#endif // guard
