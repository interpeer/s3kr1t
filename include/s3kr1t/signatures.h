/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_SIGNATURES_H
#define S3KR1T_SIGNATURES_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <s3kr1t.h>
#include <s3kr1t/error.h>
#include <s3kr1t/keys.h>
#include <s3kr1t/digests.h>

namespace s3kr1t {

/**
 * Sign data, using the given key. The signature will be written into the
 * sigbuf buffer. If the function returns ERR_INSUFFICIENT_BUFFER_SIZE, the
 * sigbuf buffer is too small and the required parameter will contain the
 * appropriate buffer size. Otherwise, the required parameter will contain
 * the amount of sigbuf buffer consumed. The exception to this is if no inputs
 * are provided.
 *
 * Note that for Edwards keys, the digest type is ignored. RFC 8032 makes that
 * selection.
 */
S3KR1T_API error_t
sign(void * sigbuf, size_t sigbuf_size, size_t & required,
    void const * data, size_t data_size,
    key const & key, digest_type dtype);


S3KR1T_API error_t
verify(void const * data, size_t data_size,
    void const * sigbuf, size_t sigbuf_size,
    key const & key, digest_type dtype);


} // namespace s3kr1t

#endif // guard
