/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_KDFS_H
#define S3KR1T_KDFS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <s3kr1t.h>

#include <s3kr1t/error.h>

namespace s3kr1t {

/**
 * KDF types.
 */
enum S3KR1T_API kdf_type : uint8_t
{
  KDFT_NONE = 0,

  KDFT_SCRYPT   = 1,
  KDFT_ARGON2ID = 2,

  // TODO
};


/**
 * Given a password and a salt, create a strong password for use with
 * cryptographic functions.
 *
 * This function exposes a simple scheme without pepper or associated
 * data.
 *
 * For ARGON2ID, this implementation uses the recommended option #2 from
 * RFC9106 Section 4.
 */
S3KR1T_API error_t
kdf(void * outbuf, size_t outbuf_size, size_t & required,
    void const * password, size_t password_size,
    void const * salt, size_t salt_size,
    kdf_type ktype);

} // namespace s3kr1t

#endif // guard
