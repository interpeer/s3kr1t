/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_KEYS_H
#define S3KR1T_KEYS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <s3kr1t.h>

#include <memory>
#include <vector>
#include <string>

#include <liberate/types/byte.h>

#include <s3kr1t/error.h>
#include <s3kr1t/digests.h>

namespace s3kr1t {

/**
 * Key types
 */
enum S3KR1T_API key_type : uint8_t
{
  KT_UNKNOWN = 0,

  KT_DSA = 1, // For DSA
  KT_RSA = 2, // For RSA
  KT_EC  = 3, // For ECDSA
  KT_ED  = 4, // For edwards curves
  // TODO
};


/**
 * Key encodings
 */
enum S3KR1T_API key_encoding : uint8_t
{
  KE_RAW = 1, // Not supported by all key types
  KE_PEM = 2,
  KE_DER = 3,
  // TODO
};


/**
 * Represents a cryptographic key.
 *
 * Keys have a key type (see key_type above), which may determine what
 * operations they can be used in.
 *
 * Keys either represent a public or a private key. If a key is a private key,
 * it's possible to extract the corresponding public key from it.
 *
 * Keys mostly behave like value types; they are comparable, assignable and
 * copyable. Copying is shallow.
 */
class S3KR1T_API key
{
public:
  /**
   * Construction and assignment has default behaviour
   */
  key() = default;
  key(key const &) = default;
  key(key &&) = default;
  key & operator=(key const &) = default;

  /**
   * Keys are either public or private; if is_public() returns true,
   * is_private() must not and vice versa.
   */
  bool is_public() const;
  bool is_private() const;

  /**
   * Return the key type.
   */
  key_type type() const;

  /**
   * Return the number of bits for the key. Note that means different things
   * for different key types. For RSA and DSA, it should be equivalent to the
   * value passed during construction. For EC keys, it's the NIST curve ID in
   * numeric form. Similarly, for ED keys it's the curve in numeric form.
   *
   * Returns 0 if type() is KT_UNKNOWN.
   */
  size_t bits() const;

  /**
   * Return the key hash. This is a digest over a DER-encoded key.
   */
  std::vector<::liberate::types::byte> hash(digest_type dtype = DT_SHA3_512) const;
  std::string hex_hash(digest_type dtype = DT_SHA3_512) const;

  /**
   * Keys evaluate to true if they hold key data, i.e. their type is not
   * KT_UNKNOWN.
   */
  operator bool () const;

  /**
   * Minimal comparison operations work. Note that "less than" is not well
   * defined; essentially anything that is not equal is less than.
   */
  bool operator==(key const & other) const;
  bool operator<(key const & other) const;

  /**
   * Key pairs consist of one public and one corresponding private key.
   */
  struct S3KR1T_API pair
  {
    std::shared_ptr<key> pubkey;
    std::shared_ptr<key> privkey;

    inline pair(pair const &) = default;
    inline pair(pair &&) = default;
    inline pair & operator=(pair const &) = default;

    inline bool is_valid() const
    {
      return pubkey && privkey;
    }

    inline operator bool() const
    {
      return is_valid();
    }

  private:
    friend class key;
    inline pair() = default;
  };

  /**
   * Create a key pair from a private key. if the current key is not a private
   * key, this will return an empty key pair.
   */
  pair create_pair() const;

  /**
   * Given a private key, return a corresponding public key. The create_pair()
   * function uses this to create a key pair. If the currrent key is not a
   * private key, this will return a key of type KT_UNKNOWN.
   */
  key extract_pubkey() const;

  /**
   * Read key or keypair from some serialized form. The serialized form can
   * be an X509 certificate in PEM or DER format (for public keys), or an
   * individual key in PEM format (for both types of key).
   */
  static key
  read_pubkey(
      void const * buffer, size_t bufsize,
      std::string const & password = {});

  static key
  read_privkey(
      void const * buffer, size_t bufsize,
      std::string const & password = {});

  static key::pair
  read_keys(
      void const * buffer, size_t bufsize,
      std::string const & password = {});

  inline static key
  read_pubkey(std::string const & input, std::string const & password = {})
  {
    return read_pubkey(input.c_str(), input.size(), password);
  }

  inline static key
  read_privkey(std::string const & input, std::string const & password = {})
  {
    return read_privkey(input.c_str(), input.size(), password);
  }

  inline static key::pair
  read_keys(std::string const & input, std::string const & password = {})
  {
    return read_keys(input.c_str(), input.size(), password);
  }


  /**
   * Generate a (private) key.
   *
   * This function takes a key type, and some parameters; these are deliberately
   * kept asa string. For e.g. RSA or DSA keys, you provide the size here. For
   * EC keys, the curve parameter.
   */
  static key
  generate(key_type type, std::string const & parameters);

  /**
   * Write the current key to a buffer. You can specify the output format.
   */
  error_t
  write(void * buffer, size_t bufsize, key_encoding encoding, size_t & written,
      std::string const & password = {}) const;


private:
  struct key_impl;

  key(std::shared_ptr<key_impl> impl);

  // Sign and verify need to be friends; see signatures.h
  friend S3KR1T_API_FRIEND error_t sign(void *, size_t, size_t &, void const *, size_t, key const &, digest_type);
  friend S3KR1T_API_FRIEND error_t verify(void const *, size_t, void const *, size_t, key const &, digest_type);

  std::shared_ptr<key_impl> m_impl = {};
};


} // namespace s3kr1t

#endif // guard
