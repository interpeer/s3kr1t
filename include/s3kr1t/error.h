/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_ERROR_H
#define S3KR1T_ERROR_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <s3kr1t.h>

#include <string>
#include <stdexcept>

/**
 * Macros for building a table of known error codes and associated messages.
 *
 * XXX Note that the macros contain namespace definitions, but are not expanded
 *     in a namespace scope. This is to allow files that include this header to
 *     decide what namespace/scope macros are to be expanded in.
 **/
#if !defined(S3KR1T_START_ERRORS)
#define S3KR1T_START_ERRORS \
  namespace s3kr1t { \
  using error_t = uint32_t; \
  enum S3KR1T_API : error_t {
#define S3KR1T_ERRDEF(name, code, desc) name = code,
#define S3KR1T_END_ERRORS \
    S3KR1T_ERROR_LAST, \
    S3KR1T_START_USER_RANGE = 1000 }; }
#define S3KR1T_ERROR_FUNCTIONS
#endif


/*****************************************************************************
 * Error definitions
 **/
S3KR1T_START_ERRORS

S3KR1T_ERRDEF(ERR_SUCCESS,
    0,
    "No error")

S3KR1T_ERRDEF(ERR_UNEXPECTED,
    1,
    "Nobody expects the Spanish Inquisition!")

S3KR1T_ERRDEF(ERR_INVALID_VALUE,
    2,
    "Invalid parameter value provided.")

S3KR1T_ERRDEF(ERR_INITIALIZATION,
    3,
    "An initialization error occurred.")

S3KR1T_ERRDEF(ERR_UNSUPPORTED,
    4,
    "The requested operation is not supported.")

S3KR1T_ERRDEF(ERR_AGAIN,
    5,
    "The requested operation would block, try again.")

S3KR1T_ERRDEF(ERR_INSUFFICIENT_BUFFER_SIZE,
    6,
    "The provided buffer is too small for the data type!")

S3KR1T_ERRDEF(ERR_DECODE,
    7,
    "Could not decode data buffer.")

S3KR1T_ERRDEF(ERR_ENCODE,
    8,
    "Could not encode data buffer.")

S3KR1T_ERRDEF(ERR_SIGN,
    9,
    "Could not sign data with the given key.")

S3KR1T_ERRDEF(ERR_DIGEST,
    10,
    "Could not create digest.")

S3KR1T_ERRDEF(ERR_VERIFY,
    11,
    "Could not verify a digest, or signature with the given key.")

S3KR1T_ERRDEF(ERR_ENCRYPT,
    12,
    "Could not encrypt data.")

S3KR1T_ERRDEF(ERR_DECRYPT,
    13,
    "Could not decrypt data.")

S3KR1T_ERRDEF(ERR_MAC,
    14,
    "Could not create MAC.")

S3KR1T_ERRDEF(ERR_KDF,
    15,
    "Could not create derived key.")


S3KR1T_END_ERRORS


#if defined(S3KR1T_ERROR_FUNCTIONS)

namespace s3kr1t {

/*****************************************************************************
 * Functions
 **/

/**
 * Return the error message associated with the given error code. Never returns
 * nullptr; if an unknown error code is given, an "unidentified error" string is
 * returned. Not that this should happen, given that error_t is an enum...
 **/
S3KR1T_API char const * error_message(error_t code);

/**
 * Return a string representation of the given error code. Also never returns
 * nullptr, see error_message() above.
 **/
S3KR1T_API char const * error_name(error_t code);



/*****************************************************************************
 * Exception
 **/

/**
 * Exception class. Constructed with an error code and optional message;
 * wraps error_message() and error_name() above.
 **/
class S3KR1T_API exception : public std::runtime_error
{
public:
  /**
   * Constructor/destructor
   **/
  explicit exception(error_t code, std::string const & details = std::string()) throw();
  virtual ~exception() throw();

  /**
   * std::exception interface
   **/
  virtual char const * what() const throw();

  /**
   * Additional functions
   **/
  char const * name() const throw();
  error_t code() const throw();

private:
  error_t     m_code;
  std::string m_message;
};


} // namespace s3kr1t

#endif // S3KR1T_ERROR_FUNCTIONS


/**
 * Undefine macros again
 **/
#undef S3KR1T_START_ERRORS
#undef S3KR1T_ERRDEF
#undef S3KR1T_END_ERRORS

#undef S3KR1T_ERROR_FUNCTIONS

#endif // guard
