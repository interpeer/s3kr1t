/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_CIPHERS_H
#define S3KR1T_CIPHERS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <s3kr1t.h>

#include <memory>

#include <s3kr1t/error.h>

namespace s3kr1t {

/**
 * Cipher types.
 */
enum S3KR1T_API cipher_type : uint8_t
{
  CT_NONE = 0,

  // AES in different modes
  CT_AES_128_ECB        = 10,
  CT_AES_128_CBC        = 11,
  CT_AES_128_CFB1       = 12,
  CT_AES_128_CFB8       = 13,
  CT_AES_128_CFB128     = 14,
  CT_AES_128_OFB        = 15,
  CT_AES_128_CTR        = 16,
  CT_AES_128_GCM        = 17,
  CT_AES_128_OCB        = 18,

  CT_AES_192_ECB        = 30,
  CT_AES_192_CBC        = 31,
  CT_AES_192_CFB1       = 32,
  CT_AES_192_CFB8       = 33,
  CT_AES_192_CFB128     = 34,
  CT_AES_192_OFB        = 35,
  CT_AES_192_CTR        = 36,
  CT_AES_192_GCM        = 37,
  CT_AES_192_OCB        = 38,

  CT_AES_256_ECB        = 50,
  CT_AES_256_CBC        = 51,
  CT_AES_256_CFB1       = 52,
  CT_AES_256_CFB8       = 53,
  CT_AES_256_CFB128     = 54,
  CT_AES_256_OFB        = 55,
  CT_AES_256_CTR        = 56,
  CT_AES_256_GCM        = 57,
  CT_AES_256_OCB        = 58,

  // ChaCha20 and related
  CT_CHACHA20           = 70,
  CT_CHACHA20_POLY1305  = 71,

  // TODO add other ciphers
};


/**
 * You can use the cipher class for encryption or decryption operations, not
 * both.
 */
enum S3KR1T_API cipher_operation : uint8_t
{
  CO_ENCRYPT = 0,
  CO_DECRYPT = 1,
};



/**
 * Create a cipher with the given cipher type and block mode.
 */
class S3KR1T_API cipher
{
public:
  /**
   * Construct a cipher instance with a cipher type and operation. Each instance
   * can be used either for decryption or encryption. You can use the instance
   * for the selected operation until a call to encrypt_final() or
   * decrypt_final() is invoked; at that point, the instance resets and can be
   * re-used. Note, however, that you should not do so without also calling
   * initialize() with a fresh key and/or IV.
   */
  explicit cipher(cipher_type type, cipher_operation cop);
  ~cipher();

  /**
   * Return this cipher's type
   */
  cipher_type type() const;

  /**
   * The IV/nonce size required by the cipher.
   */
  std::size_t iv_size() const;
  inline std::size_t nonce_size() const
  {
    return iv_size();
  }

  /**
   * The key size required by the cipher.
   */
  std::size_t key_size() const;


  /**
   * The block size of the cipher. Stream ciphers may return a size of 1 here.
   */
  std::size_t block_size() const;


  /**
   * The tag size of the cipher; this is for AEAD operations. That includes
   * GCM modes, OCB modes, and POLY1305 modes.
   */
  std::size_t tag_size() const;


  /**
   * Initialize. The IV and key parameters must be exactly iv_size() and
   * key_size() Bytes, respectively.
   *
   * You can optionally set a tag for AEAD here. This will be ignored for
   * encryption, but used in AEAD modes for decryption.
   *
   * Note that the function *will* read the amount the cipher requires, so
   * it is up to the caller to ensure there is enough data.
   *
   * You can call this function to re-initialize the cipher. This will reset
   * the instance for fresh operations.
   */
  error_t initialize(void const * key, void const * iv,
      void const * tag = nullptr);

  /**
   * Add data to encrypt.
   *
   * Note that the plain text does not have to be aligned to the cipher's block
   * size, but the cipher text should ideally be. Stream ciphers provide a block
   * size of 1 here, and are more flexible.
   *
   * The actual amount of ciphertext is returned in the ciphersize parameter. On
   * any error, this is set to zero, however.
   *
   * You can call this function multiple times. However, if you use an
   * authenticated mode and want to add authentication data, it's best to
   * provide this before calls to encrypt(), because GCM modes may otherwise fail.
   *
   * On ERR_SUCCESS, size contains the number of Bytes written to the
   * ciphertext. This may be 0, in which case encrypt_final() is likely to
   * write ciphertext (see below).
   */
  error_t encrypt(void * ciphertext, std::size_t & ciphersize,
      void const * plaintext, std::size_t const & plainsize);

  /**
   * Call this function to finalize encryption. It will write any lingering
   * ciphertext, and return the number of bytes written. The buffer passed here
   * should be at least block_size() in Bytes. However, if encrypt_final() has
   * nothing to write, a zero size may work just fine.
   *
   * For authenticated mode, pass a buffer to the tag parameter that is
   * tag_size() in Bytes. The function will write the tag to this buffer.
   */
  error_t encrypt_final(void * ciphertext, std::size_t & ciphersize,
      void * tag = nullptr);


  /**
   * Decryption works analogous to encryption, except of course that the
   * cipher text is input, and the plain text is output.
   */
  error_t decrypt(void * plaintext, std::size_t & plainsize,
      void const * ciphertext, std::size_t const & ciphersize);

  error_t decrypt_final(void * plaintext, std::size_t & plainsize);

  /**
   * For AEAD capable ciphers, add data to be verified (but not encrypted).
   * This function can be called multiple times.
   *
   * Note that for GCM cipher modes, it is vital that this function is called
   * before encrypt() or decrypt() are called. For other authenticated modes,
   * that is not important. It's best to add authenticated data before
   * encryption or decryption either way.
   */
  error_t add_authenticated(void const * data, std::size_t const & size);

private:
  struct impl;
  std::shared_ptr<impl> m_impl;
};


} // namespace s3kr1t

#endif // guard
