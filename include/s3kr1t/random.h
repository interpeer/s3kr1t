/**
 * This file is part of s3kr1t.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef S3KR1T_RANDOM_H
#define S3KR1T_RANDOM_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <s3kr1t.h>

#include <memory>
#include <functional>

#include <s3kr1t/error.h>

namespace s3kr1t {

/**
 * Return some high quality random bytes from the OS's CSRNG source. The exact
 * source varies per OS. Do not call this function too often, but do use it to
 * regularly seed RNGs.
 *
 * Place the requested amount of random Bytes into the buffer. Return the
 * number of Bytes actually placed.
 */
S3KR1T_API error_t
get_random_bytes(void * buffer, std::size_t & amount);


/**
 * The PRNG class provides a pseudo-random number generator based on the
 * ChaCha20 symmetric cipher. This particular implementation is using a
 * comparable logic to GnuTLS, but is not designed to yield identical
 * results.
 *
 * The basic operation is to rely on the symmetric cipher's internal state
 * to create sufficient entropy. At intervals, the entropy is refreshed by
 * reseeding the cipher from the seed function.
 *
 * For a PRNG to produce deterministic results, the seed function should
 * return the same seeds. This mode is automatically chosen if no seed
 * function is supplied, by using the constructor that only takes an initial
 * seed value.
 */
class S3KR1T_API prng
{
public:
  /**
   * Seed functions should place the number of Bytes requested into the first
   * buffer parameter. The actual number placed will be returned in the size
   * parameter.
   * It's no accident that this is the prototype of get_random_bytes() above;
   * you can use it for a seed function.
   */
  using seed_function = std::function<error_t (void *, std::size_t &)>;

  /**
   * Construct a PRNG from a static seed. Multiple PRNG instances with the same
   * seed will yield the same results.
   */
  prng(void const * seed, std::size_t seed_size);

  /**
   * Construct a PRNG with a seed function. At every reseed interval, this
   * function is used to refresh the PRNG's entropy.
   */
  prng(seed_function seed_func);

  ~prng();

  /**
   * Generate random Bytes. If reseeding needs to occur, it will be done in
   * time.
   */
  error_t generate(void * random_bytes, std::size_t const & size);

  /**
   * Return the number of times the PRNG has been reseeded. This is not a
   * piece of information relevant to most use cases, but it permits for
   * some validation of the PRNGs internals.
   */
  std::size_t reseed_count() const;

  /**
   * Return if the PRNG is deterministic. This is the case if a single static
   * seed is given in the constructor. Callers already know this, but we can
   * expose it here anyway.
   */
  bool is_deterministic() const;

private:
  struct impl;
  std::shared_ptr<impl> m_impl;
};

} // namespace s3kr1t

#endif // guard
